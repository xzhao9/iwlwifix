# Iwlwifix macOS Kernel extension

Hobby project of porting Linux iwlwifi kernel module to macOS X.

## Goal

Make Intel Wireless 9560 work on macOS 10.14.6.

Some hardware information here:
- PCI vendor id:   0x8086
- PCI device id:   0xa370
- PCI revision id: 0x0010

## Non-goals

- Support DVM cards or legacy cards.
- Support any MVM cards other than Intel 9560. 
- OS X coding style for device drivers.

## Dependencies

- com.apple.kpi.mach v18.7
- com.apple.kpi.bsd v18.7
- com.apple.kpi.libkern v18.7
- com.apple.kpi.iokit v18.7
- com.apple.iokit.IOPCIFamily v2.9
- com.apple.iokit.IONetworkingFamily v3.4
