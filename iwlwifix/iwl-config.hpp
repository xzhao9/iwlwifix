//
//  iwlConfig.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-02.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef iwlConfig_h
#define iwlConfig_h

#include <IOKit/IOLib.h>
#include "iwl-csr.hpp"
#include "linux/netdev_features.h"

enum IwlDeviceFamily {
    IWL_DEVICE_FAMILY_9000,
};

// LED mode
// @IWL_LED_DEFAULT:  use device default
// @IWL_LED_RF_STATE: turn LED on/off based on RF state
//                    LED ON = RF ON
//                    LED OFF = RF OFF
// @IWL_LED_BLINK:    adjust led blink rate based on blink table
// @IWL_LED_DISABLE:  led disabled
enum IwlLedMode {
    IWL_LED_DEFAULT,
    IWL_LED_RF_STATE,
    IWL_LED_BLINK,
    IWL_LED_DISABLE,
};

// NVM formats
// @IWL_NVM: the regular format
// @IWL_NVM_EXT: extended NVM format
// @IWL_NVM_SDP: NVM format used by 3168 series
enum IwlNvmType {
    IWL_NVM,
    IWL_NVM_EXT,
    IWL_NVM_SDP,
};

// This is the threshold value of plcp error rate per 100mSecs.
// It is used to set and check for the validity of plcp_delta.
#define IWL_MAX_PLCP_ERR_THRESHOLD_MIN                  1
#define IWL_MAX_PLCP_ERR_THRESHOLD_DEF                  50
#define IWL_MAX_PLCP_ERR_LONG_THRESHOLD_DEF             100
#define IWL_MAX_PLCP_ERR_EXT_LONG_THRESHOLD_DEF         200
#define IWL_MAX_PLCP_ERR_THRESHOLD_MAX                  255
#define IWL_MAX_PLCP_ERR_THRESHOLD_DISABLE              0

// TX queue watchdog timeouts in mSecs
#define IWL_WATCHDOG_DISABLED    0
#define IWL_DEF_WD_TIMEOUT    2500
#define IWL_LONG_WD_TIMEOUT    10000
#define IWL_MAX_WD_TIMEOUT    120000

#define IWL_DEFAULT_MAX_TX_POWER 22
#define IWL_TX_CSUM_NETIF_FLAGS (NETIF_F_IPV6_CSUM | NETIF_F_IP_CSUM |\
NETIF_F_TSO | NETIF_F_TSO6)

// Antenna presence definitions
#define    ANT_NONE    0x0
#define    ANT_INVALID    0xff
#define    ANT_A        BIT(0)
#define    ANT_B        BIT(1)
#define ANT_C        BIT(2)
#define    ANT_AB        (ANT_A | ANT_B)
#define    ANT_AC        (ANT_A | ANT_C)
#define ANT_BC        (ANT_B | ANT_C)
#define ANT_ABC        (ANT_A | ANT_B | ANT_C)
#define MAX_ANT_NUM 3

static inline UInt8 num_of_ant(UInt8 mask)
{
    return  !!((mask) & ANT_A) +
    !!((mask) & ANT_B) +
    !!((mask) & ANT_C);
}

struct IwlBaseParams {
    UInt32 wd_timeout;
    UInt16 eeprom_size;
    UInt16 max_event_log_size;
    UInt8  pll_cfg:1,
           shadow_ram_support:1,
           shadow_reg_enable:1,
           pcieL1_allowed:1,
           ampgWake_up_wa:1,
           scd_chain_ext_wa: 1;
    UInt16 num_of_queues;
    UInt16 max_tfd_queue_size;
    UInt8  max_ll_items;
    UInt8  led_compensation;
};

// @stbc: Support Tx STBC and 1*SS Tx STBC
// @ldpc: Support Tx/Rx with LDPC
// @useRtxForAggregation: use rts/cts protection for HT traffic
// @ht40Bands: bitmap of bands (using %NL80211_BAND_*) that support HT40
struct IwlHtParams {
    UInt8 ht_greenfield_support:1,
        stbc:1,
        ldpc:1,
        use_rts_for_aggregation: 1;
    UInt8 ht40_bands;
};

// Tx-backoff threshold
// @temperature: The threshold in Celsius
// @backoff:     The tx-backoff in uSec
struct IwlTtTxBackoff {
    SInt32 temperature;
    UInt32 backoff;
};

#define TT_TX_BACKOFF_SIZE 6

// IwlTTParams: Thermal throttling parameters
// @ctKillEntry: CT Kill entry threshold
// @ctKillExit: CT Kill exit threshold
// @ctKillDuration: The time intervals (in uSec) in which the driver needs to check whether to exit CT Kill.
// @dynamicSMPSEntry: Dynamic SMPS entry threshold
// @dynamicSMPSExit: Dynamic SMPS exit threshold
// @txProtectionEntry: TX protection entry threshold
// @txProtectionExit:  TX protection exit threshold
// @txBackoff: Array of thresholds for TxBackoff, in ascending order.
// @suppportCTKill: Support CT Kill?
// @supportDynamicSMPS: Support dynamic SMPS?
// @supportTxProtection: Support TX Protection?
// @supportTxBackoff: Support TX Backoff?
struct IwlTTParams {
    UInt32 ct_kill_entry;
    UInt32 ct_kill_exit;
    UInt32 ct_kill_duration;
    UInt32 dynamic_smps_entry;
    UInt32 dynamic_smps_exit;
    UInt32 tx_protection_entry;
    UInt32 tx_protection_exit;
    struct IwlTtTxBackoff tx_backoff[TT_TX_BACKOFF_SIZE];
    UInt8  support_ct_kill: 1,
        support_dynamic_smps: 1,
        support_tx_protection: 1,
        support_tx_backoff: 1;
};



/*
 * information on how to parse the EEPROM
 */
#define EEPROM_REG_BAND_1_CHANNELS		0x08
#define EEPROM_REG_BAND_2_CHANNELS		0x26
#define EEPROM_REG_BAND_3_CHANNELS		0x42
#define EEPROM_REG_BAND_4_CHANNELS		0x5C
#define EEPROM_REG_BAND_5_CHANNELS		0x74
#define EEPROM_REG_BAND_24_HT40_CHANNELS	0x82
#define EEPROM_REG_BAND_52_HT40_CHANNELS	0x92
#define EEPROM_6000_REG_BAND_24_HT40_CHANNELS	0x80
#define EEPROM_REGULATORY_BAND_NO_HT40		0

/* lower blocks contain EEPROM image and calibration data */
#define OTP_LOW_IMAGE_SIZE_2K		(2 * 512 * sizeof(UInt16))  /*  2 KB */
#define OTP_LOW_IMAGE_SIZE_16K		(16 * 512 * sizeof(UInt16)) /* 16 KB */
#define OTP_LOW_IMAGE_SIZE_32K		(32 * 512 * sizeof(UInt16)) /* 32 KB */


struct IwlEepromParams {
	const UInt8 regulatory_bands[7];
	bool enhanced_txpower;
};

/* Tx-backoff power threshold
 * @pwr: The power limit in mw
 * @backoff: The tx-backoff in uSec
 */
struct IwlPwrTxBackoff {
	UInt32 pwr;
	UInt32 backoff;
};

struct IwlCsrParams {
	UInt8 flag_sw_reset;                // reset the device
	UInt8 flag_mac_clock_ready;         // Indicates MAC (ucode processor, etc.) is powered up and can run
	UInt8 flag_init_done;               // Host sets this to put device fully operational.
                                        // Needs to reset this after SW_RESET to put device into low power mode.
	UInt8 flag_mac_access_req;          // Host sets this to request and maintain MAC wakeup
	UInt8 flag_val_mac_access_en;       // MAC access is enabled
	UInt8 flag_master_dis;              // Disable master
	UInt8 flag_stop_master;             // Address for resetting the device
	UInt8 addr_sw_reset;                // Address for resetting the device
	UInt32 mac_addr0_otp;               // First part of MAC address from OTP
	UInt32 mac_addr1_otp;               // Second part of MAC address from OTP
	UInt32 mac_addr0_strap;             // First part of MAC address from strap
	UInt32 mac_addr1_strap;             // Second part of MAC address from strap
};

struct iwl_cfg {
    const char *name;
    const char *fw_name_pre;
    const struct IwlBaseParams *base_params;
    const struct IwlHtParams *ht_params;
    const struct IwlEepromParams *eeprom_params;
    const struct IwlPwrTxBackoff *pwr_tx_backoffs;
    const char *default_nvm_file_C_step;
    const struct IwlTTParams *thermal_params;
    const struct IwlCsrParams *csr;
    enum IwlDeviceFamily device_family;
    enum IwlLedMode led_mode;
    enum IwlNvmType nvm_type;
    UInt32 max_data_size;
    UInt32 max_inst_size;
    NetDevFeaturesT features;
    UInt32 dccm_offset;
    UInt32 dccm_len;
    UInt32 dccm2_offset;
    UInt32 dccm2_len;
    UInt32 smem_offset;
    UInt32 smem_len;
    UInt32 soc_latency;
    UInt16 nvm_ver;
    UInt16 nvm_calib_ver;
    UInt32 rx_with_siso_diversity:1,
           bt_shared_single_ant:1,
           internal_wimax_coex:1,
           host_interrupt_operation_mode:1,
           high_temp:1,
           mac_addr_from_csr:1,
           lp_xtal_workaround:1,
           disable_dummy_notification:1,
           apmg_not_supported:1,
           mq_rx_supported:1,
           vht_mu_mimo_supported:1,
           rf_id:1,
           integrated:1,
           use_tfh:1,
           gen2:1,
           cdb:1,
           dbgc_supported:1,
           bisr_workaround:1,
           uhb_supported:1;
    UInt8 valid_tx_ant;
    UInt8 valid_rx_ant;
    UInt8 non_shared_ant;
    UInt8 nvm_hw_section_num;
    UInt8 max_rx_agg_size;
    UInt8 max_tx_aggSize;
    UInt8 max_ht_ampdu_exponent;
    UInt8 max_vht_ampdu_exponent;
    UInt8 ucode_api_max;
    UInt8 ucode_api_min;
    UInt32 min_umac_error_event_table;
    UInt32 extra_phy_cfg_flags;
    UInt32 d3_debug_data_base_addr;
    UInt32 d3_debug_data_length;
    UInt32 min_txq_size;
    UInt32 umac_prph_offset;
    UInt32 fw_mon_smem_write_ptr_addr;
    UInt32 fw_mon_smem_write_ptr_msk;
    UInt32 fw_mon_smem_cycle_cnt_ptr_addr;
    UInt32 fw_mon_smem_cycle_cnt_ptr_msk;
    UInt32 gp2_reg_addr;
    UInt32 min_256_ba_txq_size;
};

extern const struct IwlCsrParams iwl_csr_v1;
// We are not using iwlCsrV2
// extern const struct IwlCsrParams iwlCsrV2;

extern const struct iwl_cfg iwl9560_2ac_160_cfg_soc;

#endif /* iwlConfig_h */
