//
//  memory.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-06.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef memory_h
#define memory_h

#include <IOKit/IOLib.h>

inline void *kcalloc(int count, size_t size) {
    size_t total_size = size * count;
    void* ret = IOMalloc(total_size);
    return ret;
}

inline void *kmemdup(const void* origin, size_t size) {
    void* ret = IOMalloc(size);
    memcpy(ret, origin, size);
    return ret;
}

inline void *krealloc(void* origin, size_t oldsize, size_t newsize) {
    if(origin) {
        IOFree(origin, oldsize);
    }
    return IOMalloc(newsize);
}

inline void *IOMallocAtomic(vm_size_t size) {
    return IOMalloc(size);
}

inline void IOFreeAtomic(void* addr, vm_size_t size) {
    IOFree(addr, size);
}

inline void *IOCMallocAtomic(size_t n, size_t size) {
    size_t bytes = n * size;
    return IOMallocAtomic(bytes);
}

inline void IOCFreeAtomic(void* addr, size_t n, size_t size) {
    size_t bytes = n * size;
    IOFreeAtomic(addr, bytes);
}

inline void *IOMemDupAtomic(void* addr, size_t size) {
    return NULL;
}

#endif /* memory_h */
