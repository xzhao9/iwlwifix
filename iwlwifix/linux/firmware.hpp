//
//  firmware.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-17.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef firmware_h
#define firmware_h

// TODO

#include "../iwlwifix.hpp"
#include "../iwl-trans.hpp"

int inline request_firmware(const struct firmware ** fw_entry, const char* fw_name, IOPCIDevice *dev) {
    return 0;
}

void inline release_firmware(const struct firmware *fw_entry) {
}

int inline request_firmware_nowait(const char* firmware_name, IOPCIDevice *device, void* callback) {
    return 0;
}

#endif /* firmware_h */
