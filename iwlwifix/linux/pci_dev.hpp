//
//  pci_dev.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-11.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef pci_dev_h
#define pci_dev_h
/****************** PCI Power Capabilities ********************/
/* These definitions should have been in IOPCIDevice.h. */
enum
{
    kIOPCIPMCapability = 2,
    kIOPCIPMControl = 4,
};

enum {
    kIOPCIELinkControl = 16,
};

/********************* PCI-E Capabilities Start ********************/
#define  PCI_EXP_LNKCTL_ASPMC    0x0003    /* ASPM Control */
#define  PCI_EXP_LNKCTL_ASPM_L0S 0x0001    /* L0s Enable */
#define  PCI_EXP_LNKCTL_ASPM_L1  0x0002    /* L1 Enable */
#define  PCI_EXP_LNKCTL_RCB    0x0008    /* Read Completion Boundary */
#define  PCI_EXP_LNKCTL_LD    0x0010    /* Link Disable */
#define  PCI_EXP_LNKCTL_RL    0x0020    /* Retrain Link */
#define  PCI_EXP_LNKCTL_CCC    0x0040    /* Common Clock Configuration */
#define  PCI_EXP_LNKCTL_ES    0x0080    /* Extended Synch */
#define  PCI_EXP_LNKCTL_CLKREQ_EN 0x0100 /* Enable clkreq */
#define  PCI_EXP_LNKCTL_HAWD    0x0200    /* Hardware Autonomous Width Disable */
#define  PCI_EXP_LNKCTL_LBMIE    0x0400    /* Link Bandwidth Management Interrupt Enable */
#define  PCI_EXP_LNKCTL_LABIE    0x0800    /* Link Autonomous Bandwidth Interrupt Enable */

#define PCI_EXP_DEVCTL2        40    /* Device Control 2 */
#define  PCI_EXP_DEVCTL2_COMP_TIMEOUT    0x000f    /* Completion Timeout Value */
#define  PCI_EXP_DEVCTL2_COMP_TMOUT_DIS    0x0010    /* Completion Timeout Disable */
#define  PCI_EXP_DEVCTL2_ARI        0x0020    /* Alternative Routing-ID */
#define  PCI_EXP_DEVCTL2_ATOMIC_REQ    0x0040    /* Set Atomic requests */
#define  PCI_EXP_DEVCTL2_ATOMIC_EGRESS_BLOCK 0x0080 /* Block atomic egress */
#define  PCI_EXP_DEVCTL2_IDO_REQ_EN    0x0100    /* Allow IDO for requests */
#define  PCI_EXP_DEVCTL2_IDO_CMP_EN    0x0200    /* Allow IDO for completions */
#define  PCI_EXP_DEVCTL2_LTR_EN        0x0400    /* Enable LTR mechanism */
#define  PCI_EXP_DEVCTL2_OBFF_MSGA_EN    0x2000    /* Enable OBFF Message type A */
#define  PCI_EXP_DEVCTL2_OBFF_MSGB_EN    0x4000    /* Enable OBFF Message type B */
#define  PCI_EXP_DEVCTL2_OBFF_WAKE_EN    0x6000    /* OBFF using WAKE# signaling */
/********************* PCI-E Capabilities End ********************/

struct pci_dev {
    UInt16 vendor;
    UInt16 device;
    UInt16 subsystem_vendor;
    UInt16 subsystem_device;
    UInt8 revision;
};

// @state: kPCIPMCPMESupportFromD3Cold == 0x8000 == PCI_PM_CAP_PME_D3cold on Linux
bool inline pci_pme_capable(IOPCIDevice *pciDevice, UInt16 state) {
    UInt8 pmCapOffset;
    // kIOPCIPowerManagementCapability == 0x01 == PCI_CAP_ID_PM on Linux
    if(pciDevice->findPCICapability(kIOPCIPowerManagementCapability, &pmCapOffset)) {
        UInt16 pmCap;
        // kIOPCIPMCapability == 0x02 == PCI_PM_PMC on Linux
        pmCap = pciDevice->extendedConfigRead16(pmCapOffset + kIOPCIPMCapability);
        return !!(pmCap & state);
    } else { // No Power Management
        return false;
    }
}

bool inline pcie_capability_read_word(IOPCIDevice *pciDevice, int pos, UInt16 *word) {
    IOByteCount capa;
    // kIOPCIPCIExpressCapability == 0x10 == PCI_CAP_ID_EXP on Linux
    if(pciDevice->extendedFindPCICapability(kIOPCIPCIExpressCapability, &capa)) {
        // example pos:
        // PCI_EXP_LNKCTL == 16
        UInt16 linkCaps = pciDevice->extendedConfigRead16(capa + pos);
        *word = linkCaps;
        return true;
    } else {
        return false;
    }
}

#endif /* pci_dev_h */
