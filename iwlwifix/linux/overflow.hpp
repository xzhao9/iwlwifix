//
//  overflow.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-07.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef overflow_h
#define overflow_h

#include <IOKit/IOLib.h>

#define check_add_overflow(a, b, d) ({        \
typeof(a) __a = (a);            \
typeof(b) __b = (b);            \
typeof(d) __d = (d);            \
(void) (&__a == &__b);            \
(void) (&__a == __d);            \
__builtin_add_overflow(__a, __b, __d);    \
})

#define check_sub_overflow(a, b, d) ({        \
typeof(a) __a = (a);            \
typeof(b) __b = (b);            \
typeof(d) __d = (d);            \
(void) (&__a == &__b);            \
(void) (&__a == __d);            \
__builtin_sub_overflow(__a, __b, __d);    \
})

#define check_mul_overflow(a, b, d) ({        \
typeof(a) __a = (a);            \
typeof(b) __b = (b);            \
typeof(d) __d = (d);            \
(void) (&__a == &__b);            \
(void) (&__a == __d);            \
__builtin_mul_overflow(__a, __b, __d);    \
})

/*
 * Compute a*b+c, returning SIZE_MAX on overflow. Internal helper for
 * struct_size() below.
 */
static inline size_t __ab_c_size(size_t a, size_t b, size_t c)
{
    size_t bytes;
    
    if (check_mul_overflow(a, b, &bytes))
        return SIZE_MAX;
    if (check_add_overflow(bytes, c, &bytes))
        return SIZE_MAX;
    
    return bytes;
}


/**
 * struct_size() - Calculate size of structure with trailing array.
 * @p: Pointer to the structure.
 * @member: Name of the array member.
 * @n: Number of elements in the array.
 *
 * Calculates size of memory needed for structure @p followed by an
 * array of @n @member elements.
 *
 * Return: number of bytes needed or SIZE_MAX on overflow.
 */
#define struct_size(p, member, n)                    \
__ab_c_size((size_t)n,  (size_t)(sizeof(*(p)->member)), sizeof(*(p)))


#endif /* overflow_h */
