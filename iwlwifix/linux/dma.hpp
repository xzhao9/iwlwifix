//
//  dma.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-12.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef linux_dma_h
#define linux_dma_h

// Use the lower N bits
#define DMA_BIT_MASK(n)    (((n) == 64) ? ~0ULL : ((1ULL<<(n))-1))


#endif /* dma_h */
