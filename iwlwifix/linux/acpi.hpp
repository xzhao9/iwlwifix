//
//  acpi.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef acpi_h
#define acpi_h

/*
 * Miscellaneous types
 */
typedef UInt32 acpi_status;    /* All ACPI Exceptions */
typedef UInt32 acpi_name;        /* 4-byte ACPI name */
typedef char *acpi_string;    /* Null terminated ASCII string */
typedef void *acpi_handle;    /* Actually a ptr to a NS Node */


/*
 * Types associated with ACPI names and objects. The first group of
 * values (up to ACPI_TYPE_EXTERNAL_MAX) correspond to the definition
 * of the ACPI object_type() operator (See the ACPI Spec). Therefore,
 * only add to the first group if the spec changes.
 *
 * NOTE: Types must be kept in sync with the global acpi_ns_properties
 * and acpi_ns_type_names arrays.
 */
typedef UInt32 acpi_object_type;
/*
 * Event Types: Fixed & General Purpose
 */
typedef UInt32 acpi_event_type;
typedef SInt64 acpi_native_int;
typedef UInt64 acpi_size;
typedef UInt64 acpi_io_address;
typedef UInt64 acpi_physical_address;


/*
 * Note: Type == ACPI_TYPE_ANY (0) is used to indicate a NULL package
 * element or an unresolved named reference.
 */
union acpi_object {
    acpi_object_type type;    /* See definition of acpi_ns_type for values */
    struct {
        acpi_object_type type;    /* ACPI_TYPE_INTEGER */
        UInt64 value;    /* The actual number */
    } integer;
    
    struct {
        acpi_object_type type;    /* ACPI_TYPE_STRING */
        UInt32 length;    /* # of bytes in string, excluding trailing null */
        char *pointer;    /* points to the string value */
    } string;
    
    struct {
        acpi_object_type type;    /* ACPI_TYPE_BUFFER */
        UInt32 length;    /* # of bytes in buffer */
        UInt8 *pointer;    /* points to the buffer */
    } buffer;
    
    struct {
        acpi_object_type type;    /* ACPI_TYPE_PACKAGE */
        UInt32 count;    /* # of elements in package */
        union acpi_object *elements;    /* Pointer to an array of ACPI_OBJECTs */
    } package;
    
    struct {
        acpi_object_type type;    /* ACPI_TYPE_LOCAL_REFERENCE */
        acpi_object_type actual_type;    /* Type associated with the Handle */
        acpi_handle handle;    /* object reference */
    } reference;
    
    struct {
        acpi_object_type type;    /* ACPI_TYPE_PROCESSOR */
        UInt32 proc_id;
        acpi_io_address pblk_address;
        UInt32 pblk_length;
    } processor;
    
    struct {
        acpi_object_type type;    /* ACPI_TYPE_POWER */
        UInt32 system_level;
        UInt32 resource_order;
    } power_resource;
};


#endif /* acpi_h */
