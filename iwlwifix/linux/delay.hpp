//
//  delay.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-06.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef delay_hpp
#define delay_hpp

#define udelay(x) IODelay(x)
#define yield()  IOSleep(0)

#define USEC_PER_MSEC 1000
#define MAX_UDELAY_MS    5

#define mdelay(n) (\
(__builtin_constant_p(n) && (n)<=MAX_UDELAY_MS) ? udelay((n)*1000) : \
({unsigned long __ms=(n); while (__ms--) udelay(1000);}))

#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))

// by default we sleep for the max range
void inline usleep_range(unsigned long min, unsigned long max) {
    // IOSleep takes ms(milliseconds), passes in us(microseconds)
    IOSleep(DIV_ROUND_UP(min, USEC_PER_MSEC));
}

void inline msleep(unsigned long ms) {
    IOSleep(ms);
}

#endif /* delay_hpp */
