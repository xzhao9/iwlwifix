//
//  sched.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef sched_h
#define sched_h

// TODO: Assume we never give in control to other processes
static inline void might_sleep(void) {
    do {} while(0);
}

#endif /* sched_h */
