//
//  bitops.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef bitops_h
#define bitops_h

#include <IOKit/IOTypes.h>

#define BIT(nr)            (1UL << (nr))
#define BITS_PER_BYTE        8
// We only support x86_64 arch
#define BITS_PER_LONG 64
#define _BITOPS_LONG_SHIFT 6

#define DIV_ROUND_UP(n, d) (((n) + (d) - 1) / (d))

#define BITS_PER_TYPE(type) (sizeof(type) * BITS_PER_BYTE)
#define BITS_TO_LONGS(nr)    DIV_ROUND_UP(nr, BITS_PER_TYPE(long))

#define __ASM_FORM(x)    " " #x " "
#define __ASM_FORM_RAW(x)     #x
#define __ASM_FORM_COMMA(x) " " #x ","
#define __ASM_SEL(a,b) __ASM_FORM(b)
#define __ASM_SEL_RAW(a,b) __ASM_FORM_RAW(b)
#define CC_SET(c) "\n\tset" #c " %[_cc_" #c "]\n"
#define CC_OUT(c) [_cc_ ## c] "=qm"

#define __ASM_SIZE(inst, ...)    __ASM_SEL(inst##l##__VA_ARGS__, \
inst##q##__VA_ARGS__)

static inline bool constant_test_bit(long nr, const volatile unsigned long *addr)
{
    return ((1UL << (nr & (BITS_PER_LONG-1))) &
            (addr[nr >> _BITOPS_LONG_SHIFT])) != 0;
}

static inline bool variable_test_bit(long nr, volatile const unsigned long *addr)
{
    bool oldbit;
    
    asm volatile(__ASM_SIZE(bt) " %2,%1"
                 CC_SET(c)
                 : CC_OUT(c) (oldbit)
                 : "m" (*(unsigned long *)addr), "Ir" (nr) : "memory");
    
    return oldbit;
}

#define test_bit(nr, addr)            \
(__builtin_constant_p((nr))        \
? constant_test_bit((nr), (addr))    \
: variable_test_bit((nr), (addr)))

// Intel CPU is little-endian so no conversion
#define cpu_to_le16(x) ((UInt16)(x))
#define le16_to_cpu(x) ((UInt16)(x))
#define le32_to_cpu(x) ((UInt32)(x))
#define cpu_to_le32(x) ((UInt32)(x))

static inline UInt32 le32_to_cpup(const UInt32 *p)
{
    return (UInt32)*p;
}


static inline UInt16 le16_to_cpup(const UInt16 *p)
{
    return (UInt16)*p;
}

static inline UInt32 be32_to_cpu(const UInt32 be) {
    
#define ___constant_swab32(x) ((UInt32)(                \
(((UInt32)(x) & (UInt32)0x000000ffUL) << 24) |        \
(((UInt32)(x) & (UInt32)0x0000ff00UL) <<  8) |        \
(((UInt32)(x) & (UInt32)0x00ff0000UL) >>  8) |        \
(((UInt32)(x) & (UInt32)0xff000000UL) >> 24)))
    
    return ___constant_swab32(be);
}

static inline UInt16 be16_to_cpu(const UInt16 be) {
    
    /*
     * casts are necessary for constants, because we never know how for sure
     * how U/UL/ULL map to __u16, __u32, __u64. At least not in a portable way.
     */
#define ___constant_swab16(x) ((UInt16)(                \
(((UInt16)(x) & (UInt16)0x00ffU) << 8) |            \
(((UInt16)(x) & (UInt16)0xff00U) >> 8)))
    
    return ___constant_swab16(be);
}

static __attribute__((always_inline)) int fls64(UInt64 x)
{
    int bitpos = -1;
    /*
     * AMD64 says BSRQ won't clobber the dest reg if x==0; Intel64 says the
     * dest reg is undefined if x==0, but their CPU architect says its
     * value is written to set it to the same as before.
     */
    asm("bsrq %1,%q0"
        : "+r" (bitpos)
        : "rm" (x));
    return bitpos + 1;
}

/*
 * Runtime evaluation of get_order()
 */
static inline __attribute__((__const__))
int get_order(unsigned long size)
{
    int order;
    size--;
    // 12 == PAGE_SHIFT
    size >>= 12;
    order = fls64(size);
    return order;
}

#define RLONG_ADDR(x)             "m" (*(volatile long *) (x))
#define WBYTE_ADDR(x)            "+m" (*(volatile char *) (x))

#define IS_IMMEDIATE(nr)             (__builtin_constant_p(nr))
#define CONST_MASK_ADDR(nr, addr)    WBYTE_ADDR((char *)(addr) + ((nr)>>3))
#define CONST_MASK(nr)               (1 << ((nr) & 7))

// OS X desktop don't have SMP so LOCK_PREFIX is empty
#define LOCK_PREFIX_HERE ""
#define LOCK_PREFIX ""

static inline void
clear_bit(long nr, volatile unsigned long *addr)
{
    if (IS_IMMEDIATE(nr)) {
        asm volatile(LOCK_PREFIX "andb %1,%0"
                     : CONST_MASK_ADDR(nr, addr)
                     : "iq" ((UInt8)~CONST_MASK(nr)));
    } else {
        asm volatile(LOCK_PREFIX __ASM_SIZE(btr) " %1,%0"
                     : : RLONG_ADDR(addr), "Ir" (nr) : "memory");
    }
}

/**
 * upper_32_bits - return bits 32-63 of a number
 * @n: the number we're accessing
 *
 * A basic shift-right of a 64- or 32-bit quantity.  Use this to suppress
 * the "right shift count >= width of type" warning when that quantity is
 * 32-bits.
 */
#define upper_32_bits(n) ((UInt32)(((n) >> 16) >> 16))
/**
 * lower_32_bits - return bits 0-31 of a number
 * @n: the number we're accessing
 */
#define lower_32_bits(n) ((UInt32)(n))

// ================== test_and_set_bit =====================
#include "asm.hpp"

/**
 * test_and_set_bit - Set a bit and return its old value
 * @nr: Bit to set
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline bool test_and_set_bit(long nr, volatile unsigned long *addr)
{
    return GEN_BINARY_RMWcc(LOCK_PREFIX __ASM_SIZE(bts), *addr, c, "Ir", nr);
}
// ====================== hweight ===============

#define REG_IN "D"
#define REG_OUT "a"
#define X86_FEATURE_POPCNT        ( 4*32+23) /* POPCNT instruction */

static inline unsigned int __arch_hweight32(unsigned int w)
{
    unsigned int res;
    
    asm ("popcntl %1, %0"
         : "=" REG_OUT (res)
         : REG_IN (w));
    
    return res;
}

static inline unsigned int __arch_hweight8(unsigned int w)
{
    return __arch_hweight32(w & 0xff);
}
#define __const_hweight8(w)        \
((unsigned int)            \
((!!((w) & (1ULL << 0))) +    \
(!!((w) & (1ULL << 1))) +    \
(!!((w) & (1ULL << 2))) +    \
(!!((w) & (1ULL << 3))) +    \
(!!((w) & (1ULL << 4))) +    \
(!!((w) & (1ULL << 5))) +    \
(!!((w) & (1ULL << 6))) +    \
(!!((w) & (1ULL << 7)))))

#define hweight8(w)  (__builtin_constant_p(w) ? __const_hweight8(w)  : __arch_hweight8(w))


// ================ set_bit ====================

/**
 * set_bit - Atomically set a bit in memory
 * @nr: the bit to set
 * @addr: the address to start counting from
 *
 * This function is atomic and may not be reordered.  See __set_bit()
 * if you do not require the atomic guarantees.
 *
 * Note: there are no guarantees that this function will not be reordered
 * on non x86 architectures, so if you are writing portable code,
 * make sure not to rely on its reordering guarantees.
 *
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 */
static inline void
set_bit(long nr, volatile unsigned long *addr)
{
    if (IS_IMMEDIATE(nr)) {
        asm volatile(LOCK_PREFIX "orb %1,%0"
                     : CONST_MASK_ADDR(nr, addr)
                     : "iq" ((UInt8)CONST_MASK(nr))
                     : "memory");
    } else {
        asm volatile(LOCK_PREFIX __ASM_SIZE(bts) " %1,%0"
                     : : RLONG_ADDR(addr), "Ir" (nr) : "memory");
    }
}

#define BITS_PER_LONG 64
#define BIT_MASK(nr)        (1UL << ((nr) % BITS_PER_LONG))
#define BIT_WORD(nr)        ((nr) / BITS_PER_LONG)
/**
 * test_and_clear_bit - Clear a bit and return its old value
 * @nr: Bit to clear
 * @addr: Address to count from
 */
static inline int test_and_clear_bit(int nr, unsigned long *addr)
{
    unsigned long mask = BIT_MASK(nr);
    unsigned long *p = ((unsigned long *)addr) + BIT_WORD(nr);
    unsigned long old;
    
    old = *p;
    *p = old & ~mask;
    
    return (old & mask) != 0;
}


#endif /* bitops_h */
