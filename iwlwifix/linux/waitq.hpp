//
//  waitq.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-09.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

// Implementing the wait_queue on OS X
// https://blog.xuzhao.net/2019-08-14/

#ifndef waitq_h
#define waitq_h

#include <IOKit/IOLocks.h>

AbsoluteTime inline getCurrentTime(void) {
    return mach_absolute_time();
}

AbsoluteTime inline getTimeDeley(UInt64 absInterval) {
    AbsoluteTime deadline = mach_absolute_time() + absInterval;
    return deadline;
}

UInt64 inline getRemainingTime(AbsoluteTime first) {
    AbsoluteTime current = mach_absolute_time();
    if(current > first) {
        return (current - first);
    } else {
        return 0;
    }
}

#define ___wait_cond_timeout(condition)                                               \
({                                                                                    \
bool __cond = (condition);                                                        \
if (__cond && !__ret)                                                             \
__ret = 1;                                                                    \
__cond || !__ret;                                                                 \
})                                                                                    \

// Return 0 if the condition is false until timeout
// Return remaining time if the condition is true, remaining time is at least 1
#define wait_event_timeout(wq_head, condition, abstime)                               \
({                                                                                    \
long __ret = getRemainingTime(abstime);                                           \
if(__wait_cond_timeout(condition)) {                                                \
   return __ret;                                                                   \
}                                                                                   \
for(;;) {                                                                           \
  __ret = IOLockSleepDeadline(wq_head->lock, wq_head, abstime, THREAD_UNINT);     \
  bool __cond = (condition);                                                      \
  if(!__cond && (__ret == THREAD_TIMED_OUT)) {                                    \
    __ret = 0;                                                                  \
    break;                                                                      \
  }                                                                               \
  if(__cond) {                                                                    \
    __ret = getRemainingTime(abstime);                                        \
    __ret = __ret == 0 ? 1 : __ret;                                             \
    break;                                                                      \
  }                                                                               \
}                                                                                   \
return __ret;                                                                       \
})

#endif /* waitq_h */
