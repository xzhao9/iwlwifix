//
//  stddef.h
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-07.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef stddef_h
#define stddef_h

// #define offsetof(TYPE, MEMBER)    __compiler_offsetof(TYPE, MEMBER)

/**
 * sizeof_field(TYPE, MEMBER)
 *
 * @TYPE: The structure containing the field of interest
 * @MEMBER: The field to return the size of
 */
#define sizeof_field(TYPE, MEMBER) sizeof((((TYPE *)0)->MEMBER))

/**
 * offsetofend(TYPE, MEMBER)
 *
 * @TYPE: The type of the structure
 * @MEMBER: The member within the structure to get the end offset of
 */
#define offsetofend(TYPE, MEMBER) (offsetof(TYPE, MEMBER) + sizeof_field(TYPE, MEMBER))

#endif /* stddef_h */
