//
//  types.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef types_h
#define types_h

#include <IOKit/IOLocks.h>
#include <libkern/OSTypes.h>
#include "stddef.h"

typedef UInt16 __le16;
typedef UInt16 __be16;
typedef UInt32 __le32;

typedef UInt32 __be32;
typedef UInt64 __le64;
typedef UInt64 dma_addr_t;
typedef unsigned int gfp_t;

typedef IOLock* wait_queue_t; // mutex
#define HZ 100

#define __acquire(x) (void)0
#define __release(x) (void)0

# define __cond_lock(x,c)    ((c) ? ({ __acquire(x); 1; }) : 0)

/**
 * container_of - cast a member of a structure out to the containing structure
 * @ptr:    the pointer to the member.
 * @type:    the type of the container struct this is embedded in.
 * @member:    the name of the member within the struct.
 *
 */
#define container_of(ptr, type, member) ({                \
char *__mptr = (char *)(ptr);                    \
((type *)(__mptr - offsetof(type, member))); })

#endif /* types_h */
