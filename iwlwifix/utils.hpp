//
//  utils.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-03.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef utils_h
#define utils_h

#include <IOKit/IOLib.h>
#include "linux/overflow.hpp"

#define DEBUGLOG(args...) IOLog(args)

#define WARN_ON(condition) ( { \
int __ret_warn_on = !!(condition); \
if((__ret_warn_on)) { \
DEBUGLOG("[iwlwifix] Warning! %s:%d occurred in %s \n", __FILE__, __LINE__, __func__); \
} \
__ret_warn_on; \
})

#define WARN_ON_ONCE(condition) WARN_ON(condition)

static inline void* IWLIOMallocZero(vm_size_t sz) {
    char *start = (char*)IOMalloc(sz);
    if(!start) {
        return NULL;
    } else {
        memset(start, 0, sz);
        return start;
    }
}

static inline void* IWLIOCalloc(vm_size_t n, vm_size_t size) {
    vm_size_t bytes = 0;
    if(check_mul_overflow(n, size, &bytes)) {
        return NULL;
    }
    void *start = IOMalloc(bytes);
    return start;
}

static inline void* IWLIOMemdup(const void* src, size_t len) {
    void *p;
    p = IOMalloc(len);
    if(p) {
        memcpy(p, src, len);
    }
    return p;
}

#endif /* utils_h */
