//
//  drv.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-03.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef drv_hpp
#define drv_hpp

#include <IOKit/IOLib.h>
struct iwl_cfg;

struct PCIDeviceID {
    UInt32 vendor;
    UInt32 device;
    UInt32 subvendor;
    UInt32 subdevice;
    const struct iwl_cfg *driver_data;
};

extern const struct PCIDeviceID iwlHwCardIds[1];

#endif /* drv_hpp */
