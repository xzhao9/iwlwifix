//
//  drv.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-03.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//
#include "drv.hpp"
#include "../iwl-config.hpp"

#define PCI_VENDOR_ID_INTEL        0x8086
#define PCI_ANY_ID      0x8086

#define IWL_PCI_DEVICE(dev, subdev, cfg) \
.vendor = PCI_VENDOR_ID_INTEL,  .device = (dev), \
.subvendor = PCI_ANY_ID, .subdevice = (subdev), \
.driver_data = &cfg

const struct PCIDeviceID iwlHwCardIds[1] = {
    {IWL_PCI_DEVICE(0xA370, 0x0030, iwl9560_2ac_160_cfg_soc)}
};

// PCI registers
#define PCI_CFG_RETRY_TIMEOUT 0x041

