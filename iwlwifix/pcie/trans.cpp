//
//  trans.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-05.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//
#include "../iwlwifix.hpp"
#include "../iwl-trans.hpp"
#include "../iwl-prph.hpp"
#include "internal.hpp"
#include "../linux/delay.hpp"
#include "../linux/dma.hpp"
#include <libkern/OSByteOrder.h>

/* extended range in FW SRAM */
#define IWL_FW_MEM_EXTENDED_START    0x40000
#define IWL_FW_MEM_EXTENDED_END        0x57FFF

static void iwl_trans_pcie_sw_reset(struct iwl_trans *trans) {
    iwl_set_bit(trans, trans->cfg->csr->addr_sw_reset,
                BIT(trans->cfg->csr->flag_sw_reset));
    usleep_range(5000, 6000);
}

static UInt32 iwl_trans_pcie_read_shr(struct iwl_trans *trans, UInt32 reg)
{
    iwl_write32(trans, HEEP_CTRL_WRD_PCIEX_CTRL_REG,
                ((reg & 0x0000ffff) | (2 << 28)));
    return iwl_read32(trans, HEEP_CTRL_WRD_PCIEX_DATA_REG);
}

static void iwl_trans_pcie_write_shr(struct iwl_trans *trans, UInt32 reg, UInt32 val)
{
    iwl_write32(trans, HEEP_CTRL_WRD_PCIEX_DATA_REG, val);
    iwl_write32(trans, HEEP_CTRL_WRD_PCIEX_CTRL_REG,
                ((reg & 0x0000ffff) | (3 << 28)));
}

static void iwl_pcie_set_pwr(struct iwl_trans *trans, bool vaux)
{
    if (trans->cfg->apmg_not_supported)
        return;
    
    if (vaux && pci_pme_capable(trans->dev, kPCIPMCPMESupportFromD3Cold))
        iwl_set_bits_mask_prph(trans, APMG_PS_CTRL_REG,
                               APMG_PS_CTRL_VAL_PWR_SRC_VAUX,
                               ~APMG_PS_CTRL_MSK_PWR_SRC);
    else
        iwl_set_bits_mask_prph(trans, APMG_PS_CTRL_REG,
                               APMG_PS_CTRL_VAL_PWR_SRC_VMAIN,
                               ~APMG_PS_CTRL_MSK_PWR_SRC);
}

/* PCI registers */
#define PCI_CFG_RETRY_TIMEOUT    0x041

void iwl_pcie_apm_config(struct iwl_trans *trans)
{
    struct iwl_trans_pcie *trans_pcie = IWL_TRANS_GET_PCIE_TRANS(trans);
    UInt16 lctl;
    UInt16 cap;
    
    /*
     * HW bug W/A for instability in PCIe bus L0S->L1 transition.
     * Check if BIOS (or OS) enabled L1-ASPM on this device.
     * If so (likely), disable L0S, so device moves directly L0->L1;
     *    costs negligible amount of power savings.
     * If not (unlikely), enable L0S, so there is at least some
     *    power savings, even without L1.
     */
    pcie_capability_read_word(trans_pcie->pci_dev, kIOPCIELinkControl, &lctl);
    if (lctl & PCI_EXP_LNKCTL_ASPM_L1)
        iwl_set_bit(trans, CSR_GIO_REG, CSR_GIO_REG_VAL_L0S_ENABLED);
    else
        iwl_clear_bit(trans, CSR_GIO_REG, CSR_GIO_REG_VAL_L0S_ENABLED);
    trans->pm_support = !(lctl & PCI_EXP_LNKCTL_ASPM_L0S);
    
    pcie_capability_read_word(trans_pcie->pci_dev, PCI_EXP_DEVCTL2, &cap);
    trans->ltr_enabled = cap & PCI_EXP_DEVCTL2_LTR_EN;
    DEBUGLOG("[iwlwifix] L1 %sabled - LTR %sabled\n",
                        (lctl & PCI_EXP_LNKCTL_ASPM_L1) ? "En" : "Dis",
                        trans->ltr_enabled ? "En" : "Dis");
}

/*
 * Start up NIC's basic functionality after it has been reset
 * (e.g. after platform boot, or shutdown via iwl_pcie_apm_stop())
 * NOTE:  This does not load uCode nor start the embedded processor
 */
static int iwl_pcie_apm_init(struct iwl_trans *trans) {
    int ret;
    /*
     * Use "set_bit" below rather than "write", to preserve any hardware
     * bits already set by default after reset.
     */
    /* Disable L0S exit timer (platform NMI Work/Around) */
    // if (trans->cfg->device_family < IWL_DEVICE_FAMILY_8000)
    //     iwl_set_bit(trans, CSR_GIO_CHICKEN_BITS,
    //                 CSR_GIO_CHICKEN_BITS_REG_BIT_DIS_L0S_EXIT_TIMER);
    
    /*
     * Disable L0s without affecting L1;
     *  don't wait for ICH L0s (ICH bug W/A)
     */
    iwl_set_bit(trans, CSR_GIO_CHICKEN_BITS,
                CSR_GIO_CHICKEN_BITS_REG_BIT_L1A_NO_L0S_RX);
    
    /* Set FH wait threshold to maximum (HW error during stress W/A) */
    iwl_set_bit(trans, CSR_DBG_HPET_MEM_REG, CSR_DBG_HPET_MEM_REG_VAL);
    
    /*
     * Enable HAP INTA (interrupt from management bus) to
     * wake device's PCI Express link L1a -> L0s
     */
    iwl_set_bit(trans, CSR_HW_IF_CONFIG_REG,
                CSR_HW_IF_CONFIG_REG_BIT_HAP_WAKE_L1A);
    
    iwl_pcie_apm_config(trans);
    
    /* Configure analog phase-lock-loop before activating to D0A */
    if (trans->cfg->base_params->pll_cfg)
        iwl_set_bit(trans, CSR_ANA_PLL_CFG, CSR50_ANA_PLL_CFG_VAL);
    
    ret = iwl_finish_nic_init(trans);
    if (ret)
        return ret;

    if (trans->cfg->host_interrupt_operation_mode) {
        /*
         * This is a bit of an abuse - This is needed for 7260 / 3160
         * only check host_interrupt_operation_mode even if this is
         * not related to host_interrupt_operation_mode.
         *
         * Enable the oscillator to count wake up time for L1 exit. This
         * consumes slightly more power (100uA) - but allows to be sure
         * that we wake up from L1 on time.
         *
         * This looks weird: read twice the same register, discard the
         * value, set a bit, and yet again, read that same register
         * just to discard the value. But that's the way the hardware
         * seems to like it.
         */
        iwl_read_prph(trans, OSC_CLK);
        iwl_read_prph(trans, OSC_CLK);
        iwl_set_bits_prph(trans, OSC_CLK, OSC_CLK_FORCE_CONTROL);
        iwl_read_prph(trans, OSC_CLK);
        iwl_read_prph(trans, OSC_CLK);
    }
    
    /*
     * Enable DMA clock and wait for it to stabilize.
     *
     * Write to "CLK_EN_REG"; "1" bits enable clocks, while "0"
     * bits do not disable clocks.  This preserves any hardware
     * bits already set by default in "CLK_CTRL_REG" after reset.
     */
    if (!trans->cfg->apmg_not_supported) {
        iwl_write_prph(trans, APMG_CLK_EN_REG,
                       APMG_CLK_VAL_DMA_CLK_RQT);
        udelay(20);
        
        /* Disable L1-Active */
        iwl_set_bits_prph(trans, APMG_PCIDEV_STT_REG,
                          APMG_PCIDEV_STT_VAL_L1_ACT_DIS);
        
        /* Clear the interrupt in APMG if the NIC is in RFKILL */
        iwl_write_prph(trans, APMG_RTC_INT_STT_REG,
                       APMG_RTC_INT_STT_RFKILL);
    }
    
    set_bit(STATUS_DEVICE_ENABLED, &trans->status);
    
    return 0;
}

/*
 * Enable LP XTAL to avoid HW bug where device may consume much power if
 * FW is not loaded after device reset. LP XTAL is disabled by default
 * after device HW reset. Do it only if XTAL is fed by internal source.
 * Configure device's "persistence" mode to avoid resetting XTAL again when
 * SHRD_HW_RST occurs in S3.
 */
static void iwl_pcie_apm_lp_xtal_enable(struct iwl_trans *trans)
{
    int ret;
    UInt32 apmg_gp1_reg;
    UInt32 apmg_xtal_cfg_reg;
    UInt32 dl_cfg_reg;
    
    /* Force XTAL ON */
    __iwl_trans_pcie_set_bit(trans, CSR_GP_CNTRL,
                             CSR_GP_CNTRL_REG_FLAG_XTAL_ON);
    
    iwl_trans_pcie_sw_reset(trans);
    
    ret = iwl_finish_nic_init(trans);
    if (WARN_ON(ret)) {
        /* Release XTAL ON request */
        __iwl_trans_pcie_clear_bit(trans, CSR_GP_CNTRL,
                                   CSR_GP_CNTRL_REG_FLAG_XTAL_ON);
        return;
    }
    
    /*
     * Clear "disable persistence" to avoid LP XTAL resetting when
     * SHRD_HW_RST is applied in S3.
     */
    iwl_clear_bits_prph(trans, APMG_PCIDEV_STT_REG,
                        APMG_PCIDEV_STT_VAL_PERSIST_DIS);
    
    /*
     * Force APMG XTAL to be active to prevent its disabling by HW
     * caused by APMG idle state.
     */
    apmg_xtal_cfg_reg = iwl_trans_pcie_read_shr(trans,
                                                SHR_APMG_XTAL_CFG_REG);
    iwl_trans_pcie_write_shr(trans, SHR_APMG_XTAL_CFG_REG,
                             apmg_xtal_cfg_reg |
                             SHR_APMG_XTAL_CFG_XTAL_ON_REQ);
    
    iwl_trans_pcie_sw_reset(trans);
    
    /* Enable LP XTAL by indirect access through CSR */
    apmg_gp1_reg = iwl_trans_pcie_read_shr(trans, SHR_APMG_GP1_REG);
    iwl_trans_pcie_write_shr(trans, SHR_APMG_GP1_REG, apmg_gp1_reg |
                             SHR_APMG_GP1_WF_XTAL_LP_EN |
                             SHR_APMG_GP1_CHICKEN_BIT_SELECT);
    
    /* Clear delay line clock power up */
    dl_cfg_reg = iwl_trans_pcie_read_shr(trans, SHR_APMG_DL_CFG_REG);
    iwl_trans_pcie_write_shr(trans, SHR_APMG_DL_CFG_REG, dl_cfg_reg &
                             ~SHR_APMG_DL_CFG_DL_CLOCK_POWER_UP);
    
    /*
     * Enable persistence mode to avoid LP XTAL resetting when
     * SHRD_HW_RST is applied in S3.
     */
    iwl_set_bit(trans, CSR_HW_IF_CONFIG_REG,
                CSR_HW_IF_CONFIG_REG_PERSIST_MODE);
    
    /*
     * Clear "initialization complete" bit to move adapter from
     * D0A* (powered-up Active) --> D0U* (Uninitialized) state.
     */
    iwl_clear_bit(trans, CSR_GP_CNTRL,
                  BIT(trans->cfg->csr->flag_init_done));
    
    /* Activates XTAL resources monitor */
    __iwl_trans_pcie_set_bit(trans, CSR_MONITOR_CFG_REG,
                             CSR_MONITOR_XTAL_RESOURCES);
    
    /* Release XTAL ON request */
    __iwl_trans_pcie_clear_bit(trans, CSR_GP_CNTRL,
                               CSR_GP_CNTRL_REG_FLAG_XTAL_ON);
    udelay(10);
    
    /* Release APMG XTAL */
    iwl_trans_pcie_write_shr(trans, SHR_APMG_XTAL_CFG_REG,
                             apmg_xtal_cfg_reg &
                             ~SHR_APMG_XTAL_CFG_XTAL_ON_REQ);
}

void iwl_pcie_apm_stop_master(struct iwl_trans *trans)
{
    int ret;
    
    /* stop device's busmaster DMA activity */
    iwl_set_bit(trans, trans->cfg->csr->addr_sw_reset,
                BIT(trans->cfg->csr->flag_stop_master));
    
    ret = iwl_poll_bit(trans, trans->cfg->csr->addr_sw_reset,
                       BIT(trans->cfg->csr->flag_master_dis),
                       BIT(trans->cfg->csr->flag_master_dis), 100);
    if (ret < 0)
        DEBUGLOG("[iwlwifix] WARN: Master Disable Timed Out, 100 usec\n");
    
    DEBUGLOG("[iwlwifix] stop master\n");
}

static void iwl_pcie_apm_stop(struct iwl_trans *trans, bool op_mode_leave)
{
    DEBUGLOG("[iwlwifix] INFO: Stop card, put in low power state\n");
    
    if (op_mode_leave) {
        if (!test_bit(STATUS_DEVICE_ENABLED, &trans->status))
            iwl_pcie_apm_init(trans);
        
        /* inform ME that we are leaving */
        // if (trans->cfg->device_family == IWL_DEVICE_FAMILY_7000)
        // iwl_set_bits_prph(trans, APMG_PCIDEV_STT_REG,
        //                       APMG_PCIDEV_STT_VAL_WAKE_ME);
        // else if (trans->cfg->device_family >= IWL_DEVICE_FAMILY_8000) {
        {
            iwl_set_bit(trans, CSR_DBG_LINK_PWR_MGMT_REG,
                        CSR_RESET_LINK_PWR_MGMT_DISABLED);
            iwl_set_bit(trans, CSR_HW_IF_CONFIG_REG,
                        CSR_HW_IF_CONFIG_REG_PREPARE |
                        CSR_HW_IF_CONFIG_REG_ENABLE_PME);
            mdelay(1);
            iwl_clear_bit(trans, CSR_DBG_LINK_PWR_MGMT_REG,
                          CSR_RESET_LINK_PWR_MGMT_DISABLED);
        }
        // }
        mdelay(5);
    }
    
    clear_bit(STATUS_DEVICE_ENABLED, &trans->status);
    
    /* Stop device's DMA activity */
    iwl_pcie_apm_stop_master(trans);
    
    if (trans->cfg->lp_xtal_workaround) {
        iwl_pcie_apm_lp_xtal_enable(trans);
        return;
    }
    
    iwl_trans_pcie_sw_reset(trans);
    
    /*
     * Clear "initialization complete" bit to move adapter from
     * D0A* (powered-up Active) --> D0U* (Uninitialized) state.
     */
    iwl_clear_bit(trans, CSR_GP_CNTRL,
                  BIT(trans->cfg->csr->flag_init_done));
}

static int iwl_pcie_nic_init(struct iwl_trans *trans)
{
    struct iwl_trans_pcie *trans_pcie = IWL_TRANS_GET_PCIE_TRANS(trans);
    int ret;
    
    /* nic_init */
    IOSimpleLockLock(trans_pcie->irq_lock);
    // spin_lock(&trans_pcie->irq_lock);
    ret = iwl_pcie_apm_init(trans);
    IOSimpleLockUnlock(trans_pcie->irq_lock);
    // spin_unlock(&trans_pcie->irq_lock);
    
    if (ret)
        return ret;
    
    iwl_pcie_set_pwr(trans, false);
    
    iwl_op_mode_nic_config(trans->op_mode);
    
    /* Allocate the RX queue, or reset if it is already allocated */
    iwl_pcie_rx_init(trans);
    
    /* Allocate or reset and init all Tx and Command queues */
    if (iwl_pcie_tx_init(trans))
        return -ENOMEM;
    
    if (trans->cfg->base_params->shadow_reg_enable) {
        /* enable shadow regs in HW */
        iwl_set_bit(trans, CSR_MAC_SHADOW_REG_CTRL, 0x800FFFFF);
        DEBUGLOG("[iwlwifi] DEBUG INFO: Enabling shadow registers in device\n");
    }
    
    return 0;
}

#define HW_READY_TIMEOUT (50)

/* Note: returns poll_bit return value, which is >= 0 if success */
static int iwl_pcie_set_hw_ready(struct iwl_trans *trans)
{
    int ret;
    
    iwl_set_bit(trans, CSR_HW_IF_CONFIG_REG,
                CSR_HW_IF_CONFIG_REG_BIT_NIC_READY);
    
    /* See if we got it */
    ret = iwl_poll_bit(trans, CSR_HW_IF_CONFIG_REG,
                       CSR_HW_IF_CONFIG_REG_BIT_NIC_READY,
                       CSR_HW_IF_CONFIG_REG_BIT_NIC_READY,
                       HW_READY_TIMEOUT);
    
    if (ret >= 0)
        iwl_set_bit(trans, CSR_MBOX_SET_REG, CSR_MBOX_SET_REG_OS_ALIVE);
    DEBUGLOG("[iwlwifix] INFO: hardware%s ready\n", ret < 0 ? " not" : "");
    return ret;
}

/* Note: returns standard 0/-ERROR code */
int iwl_pcie_prepare_card_hw(struct iwl_trans *trans)
{
    int ret;
    int t = 0;
    int iter;
    
    DEBUGLOG("[iwlwifix] iwl_trans_prepare_card_hw enter\n");
    
    ret = iwl_pcie_set_hw_ready(trans);
    /* If the card is ready, exit 0 */
    if (ret >= 0)
        return 0;
    
    iwl_set_bit(trans, CSR_DBG_LINK_PWR_MGMT_REG,
                CSR_RESET_LINK_PWR_MGMT_DISABLED);
    usleep_range(1000, 2000);
    
    for (iter = 0; iter < 10; iter++) {
        /* If HW is not ready, prepare the conditions to check again */
        iwl_set_bit(trans, CSR_HW_IF_CONFIG_REG,
                    CSR_HW_IF_CONFIG_REG_PREPARE);
        
        do {
            ret = iwl_pcie_set_hw_ready(trans);
            if (ret >= 0)
                return 0;
            
            usleep_range(200, 1000);
            t += 200;
        } while (t < 150000);
        msleep(25);
    }
    
    DEBUGLOG("[iwlwifix] ERROR Aborting... Couldn't prepare the card\n");
    
    return ret;
}


/*
 * ucode
 */
static void iwl_pcie_load_firmware_chunk_fh(struct iwl_trans *trans,
                                            UInt32 dst_addr, dma_addr_t phy_addr,
                                            UInt32 byte_cnt)
{
    iwl_write32(trans, FH_TCSR_CHNL_TX_CONFIG_REG(FH_SRVC_CHNL),
                FH_TCSR_TX_CONFIG_REG_VAL_DMA_CHNL_PAUSE);
    
    iwl_write32(trans, FH_SRVC_CHNL_SRAM_ADDR_REG(FH_SRVC_CHNL),
                dst_addr);
    
    iwl_write32(trans, FH_TFDIB_CTRL0_REG(FH_SRVC_CHNL),
                phy_addr & FH_MEM_TFDIB_DRAM_ADDR_LSB_MSK);
    
    iwl_write32(trans, FH_TFDIB_CTRL1_REG(FH_SRVC_CHNL),
                (iwl_get_dma_hi_addr(phy_addr)
                 << FH_MEM_TFDIB_REG1_ADDR_BITSHIFT) | byte_cnt);
    
    iwl_write32(trans, FH_TCSR_CHNL_TX_BUF_STS_REG(FH_SRVC_CHNL),
                BIT(FH_TCSR_CHNL_TX_BUF_STS_REG_POS_TB_NUM) |
                BIT(FH_TCSR_CHNL_TX_BUF_STS_REG_POS_TB_IDX) |
                FH_TCSR_CHNL_TX_BUF_STS_REG_VAL_TFDB_VALID);
    
    iwl_write32(trans, FH_TCSR_CHNL_TX_CONFIG_REG(FH_SRVC_CHNL),
                FH_TCSR_TX_CONFIG_REG_VAL_DMA_CHNL_ENABLE |
                FH_TCSR_TX_CONFIG_REG_VAL_DMA_CREDIT_DISABLE |
                FH_TCSR_TX_CONFIG_REG_VAL_CIRQ_HOST_ENDTFD);
}

static int iwl_pcie_load_firmware_chunk(struct iwl_trans *trans,
                                        UInt32 dst_addr, dma_addr_t phy_addr,
                                        UInt32 byte_cnt)
{
//    struct iwl_trans_pcie *trans_pcie = IWL_TRANS_GET_PCIE_TRANS(trans);
//    unsigned long flags;
//    int ret;
//    
//    trans_pcie->ucode_write_complete = false;
//    
//    if (!iwl_trans_grab_nic_access(trans, &flags))
//        return -EIO;
//    
//    iwl_pcie_load_firmware_chunk_fh(trans, dst_addr, phy_addr,
//                                    byte_cnt);
//    iwl_trans_release_nic_access(trans, &flags);
//    
//    ret = wait_event_timeout(trans_pcie->ucode_write_waitq,
//                             trans_pcie->ucode_write_complete, 5 * HZ);
//    if (!ret) {
//        DEBUGLOG("[iwlwifix] ERROR: Failed to load firmware chunk!\n");
//        iwl_trans_pcie_dump_regs(trans);
//        return -ETIMEDOUT;
//    }
    return 0;
}

// Note: no error handling in these function family
static UInt32 iwl_trans_pcie_read32(struct iwl_trans *trans, UInt32 ofs)
{
    IOVirtualAddress hw_base = IWL_TRANS_GET_PCIE_TRANS(trans)->base_map->getVirtualAddress();
    UInt32 x = OSReadLittleInt32((volatile void*)hw_base, ofs);
    return x;
}

static void iwl_trans_pcie_write32(struct iwl_trans *trans, UInt32 ofs, UInt32 val) {
    IOVirtualAddress hw_base = IWL_TRANS_GET_PCIE_TRANS(trans)->base_map->getVirtualAddress();
    OSWriteLittleInt32((volatile void*)hw_base, ofs, val);
}

static void iwl_trans_pcie_write8(struct iwl_trans *trans, UInt32 ofs, UInt8 val) {
    IOVirtualAddress hw_base = IWL_TRANS_GET_PCIE_TRANS(trans)->base_map->getVirtualAddress();
    *(volatile uint8_t *)((uintptr_t)hw_base + ofs) = val;
}

static void iwl_trans_pcie_set_bits_mask(struct iwl_trans *trans, UInt32 reg, UInt32 mask,
                                         UInt32 value) {
    struct iwl_trans_pcie *trans_pcie = IWL_TRANS_GET_PCIE_TRANS(trans);
    // unsigned long flags; // We don't save the IRQs
    IOSimpleLockLock(trans_pcie->reg_lock);
    UInt32 v;
    v = iwl_read32(trans, reg);
    v &= ~mask;
    v |= value;
    iwl_write32(trans, reg, v);
    IOSimpleLockUnlock(trans_pcie->reg_lock);
}


static UInt32 iwl_trans_pcie_prph_msk(struct iwl_trans *trans)
{
//    if (trans->cfg->device_family >= IWL_DEVICE_FAMILY_22560)
//        return 0x00FFFFFF;
//    else
        return 0x000FFFFF;
}

static UInt32 iwl_trans_pcie_read_prph(struct iwl_trans *trans, UInt32 reg) {
    UInt32 mask = iwl_trans_pcie_prph_msk(trans);
    iwl_trans_pcie_write32(trans, HBUS_TARG_PRPH_RADDR, ((reg & mask) | (3 << 24)));
    return iwl_trans_pcie_read32(trans, HBUS_TARG_PRPH_RDAT);
}

static void iwl_trans_pcie_write_prph(struct iwl_trans *trans, UInt32 addr,
                                      UInt32 val)
{
    UInt32 mask = iwl_trans_pcie_prph_msk(trans);
    
    iwl_trans_pcie_write32(trans, HBUS_TARG_PRPH_WADDR,
                           ((addr & mask) | (3 << 24)));
    iwl_trans_pcie_write32(trans, HBUS_TARG_PRPH_WDAT, val);
}

static void iwl_trans_pcie_configure(struct iwl_trans *trans,
                                     const struct iwl_trans_config *trans_cfg)
{
    struct iwl_trans_pcie *trans_pcie = IWL_TRANS_GET_PCIE_TRANS(trans);
    
    trans_pcie->cmd_queue = trans_cfg->cmd_queue;
    trans_pcie->cmd_fifo = trans_cfg->cmd_fifo;
    trans_pcie->cmd_q_wdg_timeout = trans_cfg->cmd_q_wdg_timeout;
    if (WARN_ON(trans_cfg->n_no_reclaim_cmds > MAX_NO_RECLAIM_CMDS))
        trans_pcie->n_no_reclaim_cmds = 0;
    else
        trans_pcie->n_no_reclaim_cmds = trans_cfg->n_no_reclaim_cmds;
    if (trans_pcie->n_no_reclaim_cmds)
        memcpy(trans_pcie->no_reclaim_cmds, trans_cfg->no_reclaim_cmds,
               trans_pcie->n_no_reclaim_cmds * sizeof(UInt8));
    
    trans_pcie->rx_buf_size = trans_cfg->rx_buf_size;
    trans_pcie->rx_page_order =
    iwl_trans_get_rb_size_order(trans_pcie->rx_buf_size);
    
    trans_pcie->bc_table_dword = trans_cfg->bc_table_dword;
    trans_pcie->scd_set_active = trans_cfg->scd_set_active;
    trans_pcie->sw_csum_tx = trans_cfg->sw_csum_tx;
    
    trans_pcie->page_offs = trans_cfg->cb_data_offs;
    trans_pcie->dev_cmd_offs = trans_cfg->cb_data_offs + sizeof(void *);
    
    trans->command_groups = trans_cfg->command_groups;
    trans->command_groups_size = trans_cfg->command_groups_size;
    
    /* Initialize NAPI here - it should be before registering to mac80211
     * in the opmode but after the HW struct is allocated.
     * As this function may be called again in some corner cases don't
     * do anything if NAPI was already initialized.
     */
//    if (trans_pcie->napi_dev.reg_state != NETREG_DUMMY)
//        init_dummy_netdev(&trans_pcie->napi_dev);
}

static int iwl_trans_pcie_suspend(struct iwl_trans *trans)
{
    if (trans->runtime_pm_mode == IWL_PLAT_PM_MODE_D0I3 &&
        (trans->system_pm_mode == IWL_PLAT_PM_MODE_D0I3))
        return iwl_pci_fw_enter_d0i3(trans);
    
    return 0;
}

static void iwl_trans_pcie_resume(struct iwl_trans *trans)
{
    if (trans->runtime_pm_mode == IWL_PLAT_PM_MODE_D0I3 &&
        (trans->system_pm_mode == IWL_PLAT_PM_MODE_D0I3))
        iwl_pci_fw_exit_d0i3(trans);
}

#define IWL_TRANS_COMMON_OPS                        \
.read32 = iwl_trans_pcie_read32,                \
.write32 = iwl_trans_pcie_write32,                \
.write8 = iwl_trans_pcie_write8,                \
.set_bits_mask = iwl_trans_pcie_set_bits_mask,            \
.read_prph = iwl_trans_pcie_read_prph,                \
.write_prph = iwl_trans_pcie_write_prph,            \
//.op_mode_leave = iwl_trans_pcie_op_mode_leave,            \
//.read_mem = iwl_trans_pcie_read_mem,                \
//.write_mem = iwl_trans_pcie_write_mem,                \
//.configure = iwl_trans_pcie_configure,                \
//.set_pmi = iwl_trans_pcie_set_pmi,                \
//.sw_reset = iwl_trans_pcie_sw_reset,                \
//.grab_nic_access = iwl_trans_pcie_grab_nic_access,        \
//.release_nic_access = iwl_trans_pcie_release_nic_access,    \
//.ref = iwl_trans_pcie_ref,                    \
//.unref = iwl_trans_pcie_unref,                    \
//.dump_data = iwl_trans_pcie_dump_data,                \
//.d3_suspend = iwl_trans_pcie_d3_suspend,            \
//.d3_resume = iwl_trans_pcie_d3_resume,                \
//.sync_nmi = iwl_trans_pcie_sync_nmi

#define IWL_TRANS_PM_OPS                        \
.suspend = iwl_trans_pcie_suspend,                \
.resume = iwl_trans_pcie_resume,

static const iwl_trans_ops trans_ops_pcie = {
    IWL_TRANS_COMMON_OPS
    IWL_TRANS_PM_OPS
//    .start_hw = iwl_trans_pcie_start_hw,
//    .fw_alive = iwl_trans_pcie_fw_alive,
//    .start_fw = iwl_trans_pcie_start_fw,
//    .stop_device = iwl_trans_pcie_stop_device,
//
//    .send_cmd = iwl_trans_pcie_send_hcmd,
//
//    .tx = iwl_trans_pcie_tx,
//    .reclaim = iwl_trans_pcie_reclaim,
//    
//    .txq_disable = iwl_trans_pcie_txq_disable,
//    .txq_enable = iwl_trans_pcie_txq_enable,
//
//    .txq_set_shared_mode = iwl_trans_pcie_txq_set_shared_mode,
//
//    .wait_tx_queues_empty = iwl_trans_pcie_wait_txqs_empty,
//
//    .freeze_txq_timer = iwl_trans_pcie_freeze_txq_timer,
//    .block_txq_ptrs = iwl_trans_pcie_block_txq_ptrs,
};

struct iwl_trans *iwl_trans_pcie_alloc(Iwlwifix *dev,
                                       const struct iwl_cfg *cfg) {
    struct iwl_trans_pcie *trans_pcie;
    struct iwl_trans  *trans;
    int ret = NULL, addr_size;
    
    // I don't support gen2 NICs
    trans = iwl_trans_alloc(sizeof(struct iwl_trans_pcie), dev->pciDevice, cfg,
                            &trans_ops_pcie);
    if(!trans) {
         DEBUGLOG("[iwlwifix] %s: memory alloc failed.\n", __func__);
         return (struct iwl_trans *)ERR_PTR(-ENOMEM);
    }
    trans_pcie = IWL_TRANS_GET_PCIE_TRANS(trans);

    trans_pcie->trans = trans;
    trans_pcie->opmode_down = true;
    trans_pcie->irq_lock = IOSimpleLockAlloc();
    trans_pcie->reg_lock = IOSimpleLockAlloc();
    trans_pcie->mutex = IOLockAlloc();
    trans_pcie->ucode_write_waitq = IOLockAlloc();
    // TCP Segmentation Offload header
    // TODO trans_pcie->tso_hdr_page = alloc_percpu(struct iwl_tso_hdr_page);
    trans_pcie->debug_rfkill = 1;
    if(!cfg->base_params->pcieL1_allowed) {
        // Nothing, because pcieL1 is true for 9000 series
        DEBUGLOG("[iwlwifix] Panic! You should not be here!\n");
        ret = -EINVAL;
        iwl_trans_free(trans, sizeof(struct iwl_trans_pcie));
        return (struct iwl_trans*)ERR_PTR(ret);
    }
    trans_pcie->def_rx_queue = 0;
    if(cfg->use_tfh) {
        // do nothing, 9000 series don't use tfh
        addr_size = 32;
    } else {
        addr_size = 36;
        trans_pcie->max_tbs = IWL_NUM_OF_TBS;
        trans_pcie->tfd_size = sizeof(struct iwl_tfd);
    }
    
    trans->max_skb_frags = IWL_PCIE_MAX_FRAGS(trans_pcie);
    dev->setDMAMask(DMA_BIT_MASK(addr_size));
    dev->setConsistentDMAMask(DMA_BIT_MASK(addr_size));
    trans_pcie->base_map = dev->pciDevice->mapDeviceMemoryWithRegister(kIOPCIConfigBaseAddress0, kIOMapInhibitCache); // get BAR of region#0
    if(!trans_pcie->base_map) {
        DEBUGLOG("[iwlwifix] Region #0 is not an MMIO resource, aborting\n");
        ret = -ENODEV;
        iwl_trans_free(trans, sizeof(struct iwl_trans_pcie));
        return (struct iwl_trans*)ERR_PTR(ret);
    }
    
    /* We disable the RETRY_TIMEOUT register (0x41) to keep
     * PCI Tx retries from interfering with C3 CPU state */
    dev->pciDevice->configWrite8(PCI_CFG_RETRY_TIMEOUT, 0x0);
    trans_pcie->pci_dev = dev->pciDevice; // no need to retain because it is a part of trans
    iwl_disable_interrupts(trans);
    
    trans->hw_rev = iwl_read32(trans, CSR_HW_REV);
    if(trans->hw_rev == 0xffffffff) {
        DEBUGLOG("[iwlwifix] HW_REV=0xffffffff, PCI issues?\n");
        ret = -EIO;
        goto out_no_pci;
    }
    
    /*
     * In the 8000 HW family the format of the 4 bytes of CSR_HW_REV have
     * changed, and now the revision step also includes bit 0-1 (no more
     * "dash" value). To keep hw_rev backwards compatible - we'll store it
     * in the old format.
     */
    {
        unsigned long flags;
        trans->hw_rev = (trans->hw_rev & 0xfff0) | (CSR_HW_REV_STEP(trans->hw_rev << 2) << 2);
        ret = iwl_pcie_prepare_card_hw(trans);
        if(ret) {
            DEBUGLOG("[iwlwifix] Exit. HW is not ready");
            goto out_no_pci;
        }
        ret = iwl_finish_nic_init(trans);
        if(ret) goto out_no_pci;
        if(iwl_trans_grab_nic_access(trans, &flags)) {
            UInt32 hw_step;
            hw_step = iwl_read_umac_prph_no_grab(trans, WFPM_CTRL_REG);
            hw_step |= ENABLE_WFPM;
            iwl_write_umac_prph_no_grab(trans, WFPM_CTRL_REG, hw_step);
            hw_step = iwl_read_prph_no_grab(trans, CNVI_AUX_MISC_CHIP);
            hw_step = (hw_step >> HW_STEP_LOCATION_BITS) & 0xF;
            if (hw_step == 0x3) {
                trans->hw_rev = (trans->hw_rev & 0xFFFFFFF3) |
                (SILICON_C_STEP << 2);
            }
            iwl_trans_release_nic_access(trans, &flags);
        }
    }
    

    // trans->hw_rf_id = iwl_read32(trans, CSR_HW_RF_ID);
    // iwl_pcie_set_interrupt_capa(dev, trans);
    return trans;
out_no_pci:
    iwl_trans_free(trans, sizeof(struct iwl_trans_pcie));
    return (struct iwl_trans*)ERR_PTR(ret);
}
