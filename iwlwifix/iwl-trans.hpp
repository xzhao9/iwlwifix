//
//  iwlTrans.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-03.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef iwlTrans_hpp
#define iwlTrans_hpp

#include <IOKit/pci/IOPCIDevice.h>
#include <IOKit/IODMAController.h>
#include "iwl-config.hpp"
#include "linux/types.hpp"
#include "linux/bitops.hpp"
#include "linux/sched.hpp"
#include "linux/errno.hpp"

#include "fw/api/cmdhdr.hpp"
#include "fw/api/dbg-tlv.hpp"
#include "fw/file.hpp"

#include "iwl-modparams.hpp"
#include "iwl-dbg-tlv.hpp"
#include "iwl-op-mode.hpp"
#include "utils.hpp"

// Iwlwifix Transport Layer
// The transport layer deals with HW directly.
// It provides an abstraction of the underlying HW to the upper layer.
// The transport layer doesn't provide any policy, algorithm, etc.
// It only provides mechanisms to make the HW do something.
// It is not completely stateless but close to it.

#define FH_RSCSR_FRAME_SIZE_MSK        0x00003FFF    /* bits 0-13 */
#define FH_RSCSR_FRAME_INVALID        0x55550000
#define FH_RSCSR_FRAME_ALIGN        0x40
#define FH_RSCSR_RPA_EN            BIT(25)
#define FH_RSCSR_RADA_EN        BIT(26)
#define FH_RSCSR_RXQ_POS        16
#define FH_RSCSR_RXQ_MASK        0x3F0000

struct __attribute__((packed)) iwl_rx_packet  {
    __le32 len_n_flags;
    struct iwl_cmd_header hdr;
    UInt8 data[];
};

static inline UInt32 iwl_rx_packet_len(const struct iwl_rx_packet *pkt)
{
    return le32_to_cpu(pkt->len_n_flags) & FH_RSCSR_FRAME_SIZE_MSK;
}

static inline UInt32 iwl_rx_packet_payload_len(const struct iwl_rx_packet *pkt)
{
    return iwl_rx_packet_len(pkt) - sizeof(pkt->hdr);
}

/**
 * enum CMD_MODE - how to send the host commands ?
 *
 * @CMD_ASYNC: Return right away and don't wait for the response
 * @CMD_WANT_SKB: Not valid with CMD_ASYNC. The caller needs the buffer of
 *    the response. The caller needs to call iwl_free_resp when done.
 * @CMD_HIGH_PRIO: The command is high priority - it goes to the front of the
 *    command queue, but after other high priority commands. Valid only
 *    with CMD_ASYNC.
 * @CMD_SEND_IN_IDLE: The command should be sent even when the trans is idle.
 * @CMD_MAKE_TRANS_IDLE: The command response should mark the trans as idle.
 * @CMD_WAKE_UP_TRANS: The command response should wake up the trans
 *    (i.e. mark it as non-idle).
 * @CMD_WANT_ASYNC_CALLBACK: the op_mode's async callback function must be
 *    called after this command completes. Valid only with CMD_ASYNC.
 */
enum CMD_MODE {
    CMD_ASYNC        = BIT(0),
    CMD_WANT_SKB        = BIT(1),
    CMD_SEND_IN_RFKILL    = BIT(2),
    CMD_HIGH_PRIO        = BIT(3),
    CMD_SEND_IN_IDLE    = BIT(4),
    CMD_MAKE_TRANS_IDLE    = BIT(5),
    CMD_WAKE_UP_TRANS    = BIT(6),
    CMD_WANT_ASYNC_CALLBACK    = BIT(7),
};

#define DEF_CMD_PAYLOAD_SIZE 320

/**
 * struct iwl_device_cmd
 *
 * For allocation of the command and tx queues, this establishes the overall
 * size of the largest command we send to uCode, except for commands that
 * aren't fully copied and use other TFD space.
 */
struct __attribute__((packed)) iwl_device_cmd {
    union {
        struct {
            struct iwl_cmd_header hdr;    /* uCode API */
            UInt8 payload[DEF_CMD_PAYLOAD_SIZE];
        };
        struct {
            struct iwl_cmd_header_wide hdr_wide;
            UInt8 payload_wide[DEF_CMD_PAYLOAD_SIZE -
                            sizeof(struct iwl_cmd_header_wide) +
                            sizeof(struct iwl_cmd_header)];
        };
    };
};

#define TFD_MAX_PAYLOAD_SIZE (sizeof(struct iwl_device_cmd))

/*
 * number of transfer buffers (fragments) per transmit frame descriptor;
 * this is just the driver's idea, the hardware supports 20
 */
#define IWL_MAX_CMD_TBS_PER_TFD    2

/**
 * enum iwl_hcmd_dataflag - flag for each one of the chunks of the command
 *
 * @IWL_HCMD_DFL_NOCOPY: By default, the command is copied to the host command's
 *    ring. The transport layer doesn't map the command's buffer to DMA, but
 *    rather copies it to a previously allocated DMA buffer. This flag tells
 *    the transport layer not to copy the command, but to map the existing
 *    buffer (that is passed in) instead. This saves the memcpy and allows
 *    commands that are bigger than the fixed buffer to be submitted.
 *    Note that a TFD entry after a NOCOPY one cannot be a normal copied one.
 * @IWL_HCMD_DFL_DUP: Only valid without NOCOPY, duplicate the memory for this
 *    chunk internally and free it again after the command completes. This
 *    can (currently) be used only once per command.
 *    Note that a TFD entry after a DUP one cannot be a normal copied one.
 */
enum iwl_hcmd_dataflag {
    IWL_HCMD_DFL_NOCOPY    = BIT(0),
    IWL_HCMD_DFL_DUP    = BIT(1),
};

enum iwl_error_event_table_status {
    IWL_ERROR_EVENT_TABLE_LMAC1 = BIT(0),
    IWL_ERROR_EVENT_TABLE_LMAC2 = BIT(1),
    IWL_ERROR_EVENT_TABLE_UMAC = BIT(2),
};

/**
 * struct iwl_host_cmd - Host command to the uCode
 *
 * @data: array of chunks that composes the data of the host command
 * @resp_pkt: response packet, if %CMD_WANT_SKB was set
 * @_rx_page_order: (internally used to free response packet)
 * @_rx_page_addr: (internally used to free response packet)
 * @flags: can be CMD_*
 * @len: array of the lengths of the chunks in data
 * @dataflags: IWL_HCMD_DFL_*
 * @id: command id of the host command, for wide commands encoding the
 *    version and group as well
 */
struct iwl_host_cmd {
    const void *data[IWL_MAX_CMD_TBS_PER_TFD];
    struct iwl_rx_packet *resp_pkt;
    unsigned long _rx_page_addr;
    UInt32 _rx_page_order;
    
    UInt32 flags;
    UInt32 id;
    UInt16 len[IWL_MAX_CMD_TBS_PER_TFD];
    UInt8 dataflags[IWL_MAX_CMD_TBS_PER_TFD];
};

// TODO: Free the pages
static inline void iwl_free_resp(struct iwl_host_cmd *cmd)
{
    // free_pages(cmd->_rx_page_addr, cmd->_rx_page_order);
}

// TODO: rx_CMD_buffer DMA
struct iwl_rx_cmd_buffer {
    // struct page *_page;
    int _offset;
    bool _page_stolen;
    UInt32 _rx_page_order;
    unsigned int truesize;
};

// TODO
static inline void *rxb_addr(struct iwl_rx_cmd_buffer *r)
{
    // return (void *)((unsigned long)page_address(r->_page) + r->_offset);
    return NULL;
}

// TODO
static inline int rxb_offset(struct iwl_rx_cmd_buffer *r)
{
    return r->_offset;
}

// TODO
static inline struct page *rxb_steal_page(struct iwl_rx_cmd_buffer *r)
{
    //r->_page_stolen = true;
    //get_page(r->_page);
    //return r->_page;
    return NULL;
}

// TODO
static inline void iwl_free_rxb(struct iwl_rx_cmd_buffer *r)
{
    // __free_pages(r->_page, r->_rx_page_order);
}

#define MAX_NO_RECLAIM_CMDS    6

#define IWL_MASK(lo, hi) ((1 << (hi)) | ((1 << (hi)) - (1 << (lo))))

/*
 * Maximum number of HW queues the transport layer
 * currently supports
 */
#define IWL_MAX_HW_QUEUES        32
#define IWL_MAX_TVQM_QUEUES        512

#define IWL_MAX_TID_COUNT    8
#define IWL_MGMT_TID        15
#define IWL_FRAME_LIMIT    64
#define IWL_MAX_RX_HW_QUEUES    16

/**
 * enum iwl_wowlan_status - WoWLAN image/device status
 * @IWL_D3_STATUS_ALIVE: firmware is still running after resume
 * @IWL_D3_STATUS_RESET: device was reset while suspended
 */
enum iwl_d3_status {
    IWL_D3_STATUS_ALIVE,
    IWL_D3_STATUS_RESET,
};

/**
 * enum iwl_trans_status: transport status flags
 * @STATUS_SYNC_HCMD_ACTIVE: a SYNC command is being processed
 * @STATUS_DEVICE_ENABLED: APM is enabled
 * @STATUS_TPOWER_PMI: the device might be asleep (need to wake it up)
 * @STATUS_INT_ENABLED: interrupts are enabled
 * @STATUS_RFKILL_HW: the actual HW state of the RF-kill switch
 * @STATUS_RFKILL_OPMODE: RF-kill state reported to opmode
 * @STATUS_FW_ERROR: the fw is in error state
 * @STATUS_TRANS_GOING_IDLE: shutting down the trans, only special commands
 *    are sent
 * @STATUS_TRANS_IDLE: the trans is idle - general commands are not to be sent
 * @STATUS_TRANS_DEAD: trans is dead - avoid any read/write operation
 */
enum iwl_trans_status {
    STATUS_SYNC_HCMD_ACTIVE,
    STATUS_DEVICE_ENABLED,
    STATUS_TPOWER_PMI,
    STATUS_INT_ENABLED,
    STATUS_RFKILL_HW,
    STATUS_RFKILL_OPMODE,
    STATUS_FW_ERROR,
    STATUS_TRANS_GOING_IDLE,
    STATUS_TRANS_IDLE,
    STATUS_TRANS_DEAD,
};

static inline int
iwl_trans_get_rb_size_order(enum iwl_amsdu_size rb_size)
{
    switch (rb_size) {
        case IWL_AMSDU_2K:
            return get_order(2 * 1024);
        case IWL_AMSDU_4K:
            return get_order(4 * 1024);
        case IWL_AMSDU_8K:
            return get_order(8 * 1024);
        case IWL_AMSDU_12K:
            return get_order(12 * 1024);
        default:
            DEBUGLOG("[iwlwifix] iwl_trans_get_rb_size_order: You should not get here!");
            return -1;
    }
}

struct iwl_hcmd_names {
    UInt8 cmd_id;
    const char *const cmd_name;
};

#define HCMD_NAME(x)    \
{ .cmd_id = x, .cmd_name = #x }

struct iwl_hcmd_arr {
    const struct iwl_hcmd_names *arr;
    int size;
};

#define HCMD_ARR(x)    \
{ .arr = x, .size = ARRAY_SIZE(x) }


/**
 * struct iwl_trans_config - transport configuration
 *
 * @op_mode: pointer to the upper layer.
 * @cmd_queue: the index of the command queue.
 *    Must be set before start_fw.
 * @cmd_fifo: the fifo for host commands
 * @cmd_q_wdg_timeout: the timeout of the watchdog timer for the command queue.
 * @no_reclaim_cmds: Some devices erroneously don't set the
 *    SEQ_RX_FRAME bit on some notifications, this is the
 *    list of such notifications to filter. Max length is
 *    %MAX_NO_RECLAIM_CMDS.
 * @n_no_reclaim_cmds: # of commands in list
 * @rx_buf_size: RX buffer size needed for A-MSDUs
 *    if unset 4k will be the RX buffer size
 * @bc_table_dword: set to true if the BC table expects the byte count to be
 *    in DWORD (as opposed to bytes)
 * @scd_set_active: should the transport configure the SCD for HCMD queue
 * @sw_csum_tx: transport should compute the TCP checksum
 * @command_groups: array of command groups, each member is an array of the
 *    commands in the group; for debugging only
 * @command_groups_size: number of command groups, to avoid illegal access
 * @cb_data_offs: offset inside skb->cb to store transport data at, must have
 *    space for at least two pointers
 */
struct iwl_trans_config {
    struct iwl_op_mode *op_mode;
    
    UInt8 cmd_queue;
    UInt8 cmd_fifo;
    unsigned int cmd_q_wdg_timeout;
    const UInt8 *no_reclaim_cmds;
    unsigned int n_no_reclaim_cmds;
    
    enum iwl_amsdu_size rx_buf_size;
    bool bc_table_dword;
    bool scd_set_active;
    bool sw_csum_tx;
    const struct iwl_hcmd_arr *command_groups;
    int command_groups_size;
    
    UInt8 cb_data_offs;
};

struct iwl_trans_dump_data {
    UInt32 len;
    UInt8 data[];
};

struct iwl_trans;

struct iwl_trans_txq_scd_cfg {
    UInt8 fifo;
    UInt8 sta_id;
    UInt8 tid;
    bool aggregate;
    int frame_limit;
};

/**
 * struct iwl_trans_rxq_dma_data - RX queue DMA data
 * @fr_bd_cb: DMA address of free BD cyclic buffer
 * @fr_bd_wid: Initial write index of the free BD cyclic buffer
 * @urbd_stts_wrptr: DMA address of urbd_stts_wrptr
 * @ur_bd_cb: DMA address of used BD cyclic buffer
 */
struct iwl_trans_rxq_dma_data {
    UInt64 fr_bd_cb;
    UInt32 fr_bd_wid;
    UInt64 urbd_stts_wrptr;
    UInt64 ur_bd_cb;
};

struct iwl_trans_ops {
    int (*start_hw)(struct iwl_trans *iwl_trans, bool low_power);
    void (*op_mode_leave)(struct iwl_trans *iwl_trans);
    int (*start_fw)(struct iwl_trans *trans, const struct fw_img *fw,
                    bool run_in_rfkill);
    void (*fw_alive)(struct iwl_trans *trans, UInt32 scd_addr);
    void (*stop_device)(struct iwl_trans *trans, bool low_power);
    
    void (*d3_suspend)(struct iwl_trans *trans, bool test, bool reset);
    int (*d3_resume)(struct iwl_trans *trans, enum iwl_d3_status *status,
                     bool test, bool reset);
    
    int (*send_cmd)(struct iwl_trans *trans, struct iwl_host_cmd *cmd);
    
    int (*tx)(struct iwl_trans *trans, struct sk_buff *skb,
              struct iwl_device_cmd *dev_cmd, int queue);
    void (*reclaim)(struct iwl_trans *trans, int queue, int ssn,
                    struct sk_buff_head *skbs);
    
    bool (*txq_enable)(struct iwl_trans *trans, int queue, UInt16 ssn,
                       const struct iwl_trans_txq_scd_cfg *cfg,
                       unsigned int queue_wdg_timeout);
    void (*txq_disable)(struct iwl_trans *trans, int queue,
                        bool configure_scd);
    /* 22000 functions */
    int (*txq_alloc)(struct iwl_trans *trans,
                     __le16 flags, UInt8 sta_id, UInt8 tid,
                     int cmd_id, int size,
                     unsigned int queue_wdg_timeout);
    void (*txq_free)(struct iwl_trans *trans, int queue);
    int (*rxq_dma_data)(struct iwl_trans *trans, int queue,
                        struct iwl_trans_rxq_dma_data *data);
    
    void (*txq_set_shared_mode)(struct iwl_trans *trans, UInt32 txq_id,
                                bool shared);
    
    int (*wait_tx_queues_empty)(struct iwl_trans *trans, UInt32 txq_bm);
    int (*wait_txq_empty)(struct iwl_trans *trans, int queue);
    void (*freeze_txq_timer)(struct iwl_trans *trans, unsigned long txqs,
                             bool freeze);
    void (*block_txq_ptrs)(struct iwl_trans *trans, bool block);
    
    void (*write8)(struct iwl_trans *trans, UInt32 ofs, UInt8 val);
    void (*write32)(struct iwl_trans *trans, UInt32 ofs, UInt32 val);
    UInt32 (*read32)(struct iwl_trans *trans, UInt32 ofs);
    UInt32 (*read_prph)(struct iwl_trans *trans, UInt32 ofs);
    void (*write_prph)(struct iwl_trans *trans, UInt32 ofs, UInt32 val);
    int (*read_mem)(struct iwl_trans *trans, UInt32 addr,
                    void *buf, int dwords);
    int (*write_mem)(struct iwl_trans *trans, UInt32 addr,
                     const void *buf, int dwords);
    void (*configure)(struct iwl_trans *trans,
                      const struct iwl_trans_config *trans_cfg);
    void (*set_pmi)(struct iwl_trans *trans, bool state);
    void (*sw_reset)(struct iwl_trans *trans);
    bool (*grab_nic_access)(struct iwl_trans *trans, unsigned long *flags);
    void (*release_nic_access)(struct iwl_trans *trans,
                               unsigned long *flags);
    void (*set_bits_mask)(struct iwl_trans *trans, UInt32 reg, UInt32 mask,
                          UInt32 value);
    void (*ref)(struct iwl_trans *trans);
    void (*unref)(struct iwl_trans *trans);
    int  (*suspend)(struct iwl_trans *trans);
    void (*resume)(struct iwl_trans *trans);
    
    struct iwl_trans_dump_data *(*dump_data)(struct iwl_trans *trans,
                                             UInt32 dump_mask);
    void (*debugfs_cleanup)(struct iwl_trans *trans);
    void (*sync_nmi)(struct iwl_trans *trans);
};

/**
 * enum iwl_trans_state - state of the transport layer
 *
 * @IWL_TRANS_NO_FW: no fw has sent an alive response
 * @IWL_TRANS_FW_ALIVE: a fw has sent an alive response
 */
enum iwl_trans_state {
    IWL_TRANS_NO_FW = 0,
    IWL_TRANS_FW_ALIVE    = 1,
};

enum iwl_plat_pm_mode {
    IWL_PLAT_PM_MODE_DISABLED,
    IWL_PLAT_PM_MODE_D3,
    IWL_PLAT_PM_MODE_D0I3,
};

/* Max time to wait for trans to become idle/non-idle on d0i3
 * enter/exit (in msecs).
 */
#define IWL_TRANS_IDLE_TIMEOUT 2000

/* Max time to wait for nmi interrupt */
#define IWL_TRANS_NMI_TIMEOUT (HZ / 4)

/**
 * struct iwl_dram_data
 * @physical: page phy pointer
 * @block: pointer to the allocated block/page
 * @size: size of the block/page
 */
//struct iwl_dram_data {
//    dma_addr_t physical;
//    void *block;
//    int size;
//};
struct iwl_dram_data {
    IOBufferMemoryDescriptor *bufDesc;
    IODMACommand *command;
};

/**
 * struct iwl_self_init_dram - dram data used by self init process
 * @fw: lmac and umac dram data
 * @fw_cnt: total number of items in array
 * @paging: paging dram data
 * @paging_cnt: total number of items in array
 */
struct iwl_self_init_dram {
    struct iwl_dram_data *fw;
    int fw_cnt;
    struct iwl_dram_data *paging;
    int paging_cnt;
};

struct iwl_trans {
    const struct iwl_trans_ops *ops;
    struct iwl_op_mode *op_mode;
    const struct iwl_cfg *cfg;
    struct iwl_drv *drv;
    enum iwl_trans_state state;
    unsigned long status;
    
    IOPCIDevice *dev;
    UInt32 max_skb_frags;
    UInt32 hw_rev;
    UInt32 hw_rf_id;
    UInt32 hw_id;
    char hw_id_str[52];
    
    UInt8 rx_mpdu_cmd, rx_mpdu_cmd_hdr_size;
    
    bool pm_support;
    bool ltr_enabled;
    
    const struct iwl_hcmd_arr *command_groups;
    int command_groups_size;
    bool wide_cmd_header;
    
    UInt8 num_rx_queues;
    
    size_t iml_len;
    UInt8 *iml;
    
    /* The following fields are internal only */
    // KmemCache *dev_cmd_pool;
    char dev_cmd_pool_name[50];
    
    struct dentry *dbgfs_dir;
  
    // Linux-specific, ignore
// #ifdef CONFIG_LOCKDEP
//    struct lockdep_map sync_cmd_lockdep_map;
//#endif
    
    struct iwl_apply_point_data apply_points[IWL_FW_INI_APPLY_NUM];
    struct iwl_apply_point_data apply_points_ext[IWL_FW_INI_APPLY_NUM];
    
    bool external_ini_loaded;
    bool ini_valid;
    
    const struct iwl_fw_dbg_dest_tlv_v1 *dbg_dest_tlv;
    const struct iwl_fw_dbg_conf_tlv *dbg_conf_tlv[FW_DBG_CONF_MAX];
    struct iwl_fw_dbg_trigger_tlv * const *dbg_trigger_tlv;
    UInt8 dbg_n_dest_reg;
    int num_blocks;
    
    struct iwl_dram_data fw_mon[IWL_FW_INI_APPLY_NUM];
    struct iwl_self_init_dram init_dram;
    
    enum iwl_plat_pm_mode system_pm_mode;
    enum iwl_plat_pm_mode runtime_pm_mode;
    bool suspending;
    bool dbg_rec_on;
    
    UInt32 lmac_error_event_table[2];
    UInt32 umac_error_event_table;
    unsigned int error_event_table_tlv_status;
    bool hw_error;
    
    /* pointer to trans specific struct */
    /*Ensure that this pointer will always be aligned to sizeof pointer */
    char trans_specific[0] __attribute__((aligned(sizeof(void *))));
};

const char *iwl_get_cmd_string(struct iwl_trans *trans, UInt32 id);
int iwl_cmd_groups_verify_sorted(const struct iwl_trans_config *trans);

static inline void iwl_trans_configure(struct iwl_trans *trans,
                                       const struct iwl_trans_config *trans_cfg)
{
    trans->op_mode = trans_cfg->op_mode;
    
    trans->ops->configure(trans, trans_cfg);
    if(iwl_cmd_groups_verify_sorted(trans_cfg)) {
        DEBUGLOG("[iwlwifix] wARN ON in iwl_trans_configure! Group verify sorted failed.");
    } else {
        return;
    }
}

static inline int _iwl_trans_start_hw(struct iwl_trans *trans, bool low_power)
{
    might_sleep();
    
    return trans->ops->start_hw(trans, low_power);
}

static inline int iwl_trans_start_hw(struct iwl_trans *trans)
{
    return trans->ops->start_hw(trans, true);
}

static inline void iwl_trans_op_mode_leave(struct iwl_trans *trans)
{
    might_sleep();
    
    if (trans->ops->op_mode_leave)
        trans->ops->op_mode_leave(trans);
    
    trans->op_mode = NULL;
    
    trans->state = IWL_TRANS_NO_FW;
}

static inline void iwl_trans_fw_alive(struct iwl_trans *trans, UInt32 scd_addr)
{
    might_sleep();
    
    trans->state = IWL_TRANS_FW_ALIVE;
    
    trans->ops->fw_alive(trans, scd_addr);
}

static inline int iwl_trans_start_fw(struct iwl_trans *trans,
                                     const struct fw_img *fw,
                                     bool run_in_rfkill)
{
    might_sleep();
    
    // Turn off the Alarm...
    // WARN_ON_ONCE(!trans->rx_mpdu_cmd);
    
    clear_bit(STATUS_FW_ERROR, &trans->status);
    return trans->ops->start_fw(trans, fw, run_in_rfkill);
}

static inline void _iwl_trans_stop_device(struct iwl_trans *trans,
                                          bool low_power)
{
    might_sleep();
    
    trans->ops->stop_device(trans, low_power);
    
    trans->state = IWL_TRANS_NO_FW;
}

static inline void iwl_trans_stop_device(struct iwl_trans *trans)
{
    _iwl_trans_stop_device(trans, true);
}

static inline void iwl_trans_d3_suspend(struct iwl_trans *trans, bool test,
                                        bool reset)
{
    might_sleep();
    if (trans->ops->d3_suspend)
        trans->ops->d3_suspend(trans, test, reset);
}

static inline int iwl_trans_d3_resume(struct iwl_trans *trans,
                                      enum iwl_d3_status *status,
                                      bool test, bool reset)
{
    might_sleep();
    if (!trans->ops->d3_resume)
        return 0;
    
    return trans->ops->d3_resume(trans, status, test, reset);
}

static inline int iwl_trans_suspend(struct iwl_trans *trans)
{
    if (!trans->ops->suspend)
        return 0;
    
    return trans->ops->suspend(trans);
}

static inline void iwl_trans_resume(struct iwl_trans *trans)
{
    if (trans->ops->resume)
        trans->ops->resume(trans);
}

static inline struct iwl_trans_dump_data *
iwl_trans_dump_data(struct iwl_trans *trans, UInt32 dump_mask)
{
    if (!trans->ops->dump_data)
        return NULL;
    return trans->ops->dump_data(trans, dump_mask);
}

static inline struct iwl_device_cmd *
iwl_trans_alloc_tx_cmd(struct iwl_trans *trans)
{
    // return kmem_cache_alloc(trans->dev_cmd_pool, GFP_ATOMIC);
    return NULL;
}

int iwl_trans_send_cmd(struct iwl_trans *trans, struct iwl_host_cmd *cmd);

static inline void iwl_trans_free_tx_cmd(struct iwl_trans *trans,
                                         struct iwl_device_cmd *dev_cmd)
{
    // kmem_cache_free(trans->dev_cmd_pool, dev_cmd);
    // TODO Free memory
}

static inline int iwl_trans_tx(struct iwl_trans *trans, struct sk_buff *skb,
                               struct iwl_device_cmd *dev_cmd, int queue)
{
    
    if (test_bit(STATUS_FW_ERROR, &trans->status)) {
        DEBUGLOG("[iwlwifix] %s bad status = %lu\n", __func__, trans->status);
        return -EIO;
    }
    
    if(trans->state != IWL_TRANS_FW_ALIVE) {
        DEBUGLOG("[iwlwifix] %s bad state = %d\n", __func__, trans->state);
        return -EIO;
    }
    
    return trans->ops->tx(trans, skb, dev_cmd, queue);
}

static inline void iwl_trans_reclaim(struct iwl_trans *trans, int queue,
                                     int ssn, struct sk_buff_head *skbs)
{
    if ((trans->state != IWL_TRANS_FW_ALIVE)) {
        DEBUGLOG("[iwlwifix] %s bad state = %d\n", __func__, trans->state);
        return;
    }
    
    trans->ops->reclaim(trans, queue, ssn, skbs);
}

static inline void iwl_trans_txq_disable(struct iwl_trans *trans, int queue,
                                         bool configure_scd)
{
    trans->ops->txq_disable(trans, queue, configure_scd);
}

static inline bool
iwl_trans_txq_enable_cfg(struct iwl_trans *trans, int queue, UInt16 ssn,
                         const struct iwl_trans_txq_scd_cfg *cfg,
                         unsigned int queue_wdg_timeout)
{
    might_sleep();
    
    if ((trans->state != IWL_TRANS_FW_ALIVE)) {
        DEBUGLOG("[iwlwifix] %s bad state = %d\n", __func__, trans->state);
        return false;
    }
    
    return trans->ops->txq_enable(trans, queue, ssn,
                                  cfg, queue_wdg_timeout);
}

static inline int
iwl_trans_get_rxq_dma_data(struct iwl_trans *trans, int queue,
                           struct iwl_trans_rxq_dma_data *data)
{
    if ((!trans->ops->rxq_dma_data))
        return -ENOTSUPP;
    
    return trans->ops->rxq_dma_data(trans, queue, data);
}

static inline void
iwl_trans_txq_free(struct iwl_trans *trans, int queue)
{
    if ((!trans->ops->txq_free))
        return;
    
    trans->ops->txq_free(trans, queue);
}

static inline int
iwl_trans_txq_alloc(struct iwl_trans *trans,
                    __le16 flags, UInt8  sta_id, UInt8  tid,
                    int cmd_id, int size,
                    unsigned int wdg_timeout)
{
    might_sleep();
    
    if (!trans->ops->txq_alloc) {
        DEBUGLOG("[iwlfiwix] %s ops not txq_alloc\n", __func__);
        return -ENOTSUPP;
    }
    
    if (trans->state != IWL_TRANS_FW_ALIVE) {
        DEBUGLOG("[iwlfiwix] %s bad state = %d\n", __func__, trans->state);
        return -EIO;
    }
    
    return trans->ops->txq_alloc(trans, flags, sta_id, tid,
                                 cmd_id, size, wdg_timeout);
}

static inline void iwl_trans_txq_set_shared_mode(struct iwl_trans *trans,
                                                 int queue, bool shared_mode)
{
    if (trans->ops->txq_set_shared_mode)
        trans->ops->txq_set_shared_mode(trans, queue, shared_mode);
}

static inline void iwl_trans_txq_enable(struct iwl_trans *trans, int queue,
                                        int fifo, int sta_id, int tid,
                                        int frame_limit, UInt16 ssn,
                                        unsigned int queue_wdg_timeout)
{
    struct iwl_trans_txq_scd_cfg cfg = {
        .fifo = (UInt8)fifo,
        .sta_id = (UInt8)sta_id,
        .tid = (UInt8)tid,
        .frame_limit = frame_limit,
        .aggregate = sta_id >= 0,
    };
    
    iwl_trans_txq_enable_cfg(trans, queue, ssn, &cfg, queue_wdg_timeout);
}

static inline
void iwl_trans_ac_txq_enable(struct iwl_trans *trans, int queue, int fifo,
                             unsigned int queue_wdg_timeout)
{
    struct iwl_trans_txq_scd_cfg cfg = {
        .fifo = (UInt8)fifo,
        // .sta_id = -1, // [iwlwifix] Fix the UInt8 conversion bug
        .sta_id = 0xff,
        .tid = IWL_MAX_TID_COUNT,
        .frame_limit = IWL_FRAME_LIMIT,
        .aggregate = false,
    };
    
    iwl_trans_txq_enable_cfg(trans, queue, 0, &cfg, queue_wdg_timeout);
}

static inline void iwl_trans_freeze_txq_timer(struct iwl_trans *trans,
                                              unsigned long txqs,
                                              bool freeze)
{
    if ((trans->state != IWL_TRANS_FW_ALIVE)) {
        DEBUGLOG("[iwlfifix] %s bad state = %d\n", __func__, trans->state);
        return;
    }
    
    if (trans->ops->freeze_txq_timer)
        trans->ops->freeze_txq_timer(trans, txqs, freeze);
}

static inline void iwl_trans_block_txq_ptrs(struct iwl_trans *trans,
                                            bool block)
{
    if ((trans->state != IWL_TRANS_FW_ALIVE)) {
        DEBUGLOG("[iwlwifix] %s bad state = %d\n", __func__, trans->state);
        return;
    }
    
    if (trans->ops->block_txq_ptrs)
        trans->ops->block_txq_ptrs(trans, block);
}

static inline int iwl_trans_wait_tx_queues_empty(struct iwl_trans *trans,
                                                 UInt32 txqs)
{
    if ((!trans->ops->wait_tx_queues_empty))
        return -ENOTSUPP;
    
    if ((trans->state != IWL_TRANS_FW_ALIVE)) {
        DEBUGLOG("[iwlwifix] %s has bad state = %d\n", __func__, trans->state);
        return -EIO;
    }
    
    return trans->ops->wait_tx_queues_empty(trans, txqs);
}

static inline int iwl_trans_wait_txq_empty(struct iwl_trans *trans, int queue)
{
    if ((!trans->ops->wait_txq_empty))
        return -ENOTSUPP;
    
    if ((trans->state != IWL_TRANS_FW_ALIVE)) {
        DEBUGLOG("[iwlwifix] %s has bad state = %d\n", __func__, trans->state);
        return -EIO;
    }
    
    return trans->ops->wait_txq_empty(trans, queue);
}

static inline void iwl_trans_write8(struct iwl_trans *trans, UInt32 ofs, UInt8 val)
{
    trans->ops->write8(trans, ofs, val);
}

static inline void iwl_trans_write32(struct iwl_trans *trans, UInt32 ofs, UInt32 val)
{
    trans->ops->write32(trans, ofs, val);
}

static inline UInt32 iwl_trans_read32(struct iwl_trans *trans, UInt32 ofs)
{
    return trans->ops->read32(trans, ofs);
}

static inline UInt32 iwl_trans_read_prph(struct iwl_trans *trans, UInt32 ofs)
{
    return trans->ops->read_prph(trans, ofs);
}

static inline void iwl_trans_write_prph(struct iwl_trans *trans, UInt32 ofs,
                                        UInt32 val)
{
    return trans->ops->write_prph(trans, ofs, val);
}

static inline int iwl_trans_read_mem(struct iwl_trans *trans, UInt32 addr,
                                     void *buf, int dwords)
{
    return trans->ops->read_mem(trans, addr, buf, dwords);
}

#define iwl_trans_read_mem_bytes(trans, addr, buf, bufsize)              \
do {                                      \
if (__builtin_constant_p(bufsize))                  \
BUILD_BUG_ON((bufsize) % sizeof(u32));              \
iwl_trans_read_mem(trans, addr, buf, (bufsize) / sizeof(u32));\
} while (0)

static inline UInt32 iwl_trans_read_mem32(struct iwl_trans *trans, UInt32 addr)
{
    UInt32 value;
    
    if ((iwl_trans_read_mem(trans, addr, &value, 1)))
        return 0xa5a5a5a5;
    
    return value;
}

static inline int iwl_trans_write_mem(struct iwl_trans *trans, UInt32 addr,
                                      const void *buf, int dwords)
{
    return trans->ops->write_mem(trans, addr, buf, dwords);
}

static inline UInt32 iwl_trans_write_mem32(struct iwl_trans *trans, UInt32 addr,
                                        UInt32 val)
{
    return iwl_trans_write_mem(trans, addr, &val, 1);
}

static inline void iwl_trans_set_pmi(struct iwl_trans *trans, bool state)
{
    if (trans->ops->set_pmi)
        trans->ops->set_pmi(trans, state);
}

static inline void iwl_trans_sw_reset(struct iwl_trans *trans)
{
    if (trans->ops->sw_reset)
        trans->ops->sw_reset(trans);
}

static inline void
iwl_trans_set_bits_mask(struct iwl_trans *trans, UInt32 reg, UInt32 mask, UInt32 value)
{
    trans->ops->set_bits_mask(trans, reg, mask, value);
}

#define iwl_trans_grab_nic_access(trans, flags)    \
__cond_lock(nic_access,                \
(trans)->ops->grab_nic_access(trans, flags))

static inline void
iwl_trans_release_nic_access(struct iwl_trans *trans, unsigned long *flags)
{
    trans->ops->release_nic_access(trans, flags);
    __release(nic_access);
}

static inline void iwl_trans_fw_error(struct iwl_trans *trans)
{
    if ((!trans->op_mode))
        return;
    
    /* prevent double restarts due to the same erroneous FW */
    if (!test_and_set_bit(STATUS_FW_ERROR, &trans->status))
        iwl_op_mode_nic_error(trans->op_mode);
}

static inline void iwl_trans_sync_nmi(struct iwl_trans *trans)
{
    if (trans->ops->sync_nmi)
        trans->ops->sync_nmi(trans);
}

/*****************************************************
 * transport helper functions
 *****************************************************/
struct iwl_trans *iwl_trans_alloc(unsigned int priv_size,
                                  IOPCIDevice *dev,
                                  const struct iwl_cfg *cfg,
                                  const struct iwl_trans_ops *ops);
void iwl_trans_free(struct iwl_trans *trans, unsigned int priv_size);
void iwl_trans_ref(struct iwl_trans *trans);
void iwl_trans_unref(struct iwl_trans *trans);

#endif /* iwlTrans_hpp */
