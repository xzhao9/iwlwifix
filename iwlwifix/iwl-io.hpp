//
//  iwlIO.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-03.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef iwlIO_h
#define iwlIO_h

#include "iwl-trans.hpp"


void iwl_write8(struct iwl_trans *trans, UInt32 ofs, UInt8 val);
void iwl_write32(struct iwl_trans *trans, UInt32 ofs, UInt32 val);
void iwl_write64(struct iwl_trans *trans, UInt64 ofs, UInt64 val);
UInt32 iwl_read32(struct iwl_trans *trans, UInt32 ofs);

static inline void iwl_set_bit(struct iwl_trans *trans, UInt32 reg, UInt32 mask)
{
    iwl_trans_set_bits_mask(trans, reg, mask, mask);
}

static inline void iwl_clear_bit(struct iwl_trans *trans, UInt32 reg, UInt32 mask)
{
    iwl_trans_set_bits_mask(trans, reg, mask, 0);
}

int iwl_poll_bit(struct iwl_trans *trans, UInt32 addr,
                 UInt32 bits, UInt32 mask, int timeout);
int iwl_poll_direct_bit(struct iwl_trans *trans, UInt32 addr, UInt32 mask,
                        int timeout);

UInt32 iwl_read_direct32(struct iwl_trans *trans, UInt32 reg);
void iwl_write_direct32(struct iwl_trans *trans, UInt32 reg, UInt32 value);
void iwl_write_direct64(struct iwl_trans *trans, UInt64 reg, UInt64 value);


UInt32 iwl_read_prph_no_grab(struct iwl_trans *trans, UInt32 ofs);
UInt32 iwl_read_prph(struct iwl_trans *trans, UInt32 ofs);
void iwl_write_prph_no_grab(struct iwl_trans *trans, UInt32 ofs, UInt32 val);
void iwl_write_prph64_no_grab(struct iwl_trans *trans, UInt64 ofs, UInt64 val);
void iwl_write_prph(struct iwl_trans *trans, UInt32 ofs, UInt32 val);
int iwl_poll_prph_bit(struct iwl_trans *trans, UInt32 addr,
                      UInt32 bits, UInt32 mask, int timeout);
void iwl_set_bits_prph(struct iwl_trans *trans, UInt32 ofs, UInt32 mask);
void iwl_set_bits_mask_prph(struct iwl_trans *trans, UInt32 ofs,
                            UInt32 bits, UInt32 mask);
void iwl_clear_bits_prph(struct iwl_trans *trans, UInt32 ofs, UInt32 mask);
void iwl_force_nmi(struct iwl_trans *trans);

int iwl_finish_nic_init(struct iwl_trans *trans);

/* Error handling */
int iwl_dump_fh(struct iwl_trans *trans, char **buf);

/*
 * UMAC periphery address space changed from 0xA00000 to 0xD00000 starting from
 * device family AX200. So peripheries used in families above and below AX200
 * should go through iwl_..._umac_..._prph.
 */
static inline UInt32 iwl_umac_prph(struct iwl_trans *trans, UInt32 ofs)
{
    return ofs + trans->cfg->umac_prph_offset;
}

static inline UInt32 iwl_read_umac_prph_no_grab(struct iwl_trans *trans, UInt32 ofs)
{
    return iwl_read_prph_no_grab(trans, ofs + trans->cfg->umac_prph_offset);
}

static inline UInt32 iwl_read_umac_prph(struct iwl_trans *trans, UInt32 ofs)
{
    return iwl_read_prph(trans, ofs + trans->cfg->umac_prph_offset);
}

static inline void iwl_write_umac_prph_no_grab(struct iwl_trans *trans, UInt32 ofs,
                                               UInt32 val)
{
    iwl_write_prph_no_grab(trans,  ofs + trans->cfg->umac_prph_offset, val);
}

static inline void iwl_write_umac_prph(struct iwl_trans *trans, UInt32 ofs,
                                       UInt32 val)
{
    iwl_write_prph(trans,  ofs + trans->cfg->umac_prph_offset, val);
}

static inline int iwl_poll_umac_prph_bit(struct iwl_trans *trans, UInt32 addr,
                                         UInt32 bits, UInt32 mask, int timeout)
{
    return iwl_poll_prph_bit(trans, addr + trans->cfg->umac_prph_offset,
                             bits, mask, timeout);
}

#endif /* iwlIO_h */
