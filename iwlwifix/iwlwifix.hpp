//
//  iwlwifix.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-02.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

// supercedes iwl-drv.h/c
#ifndef iwlwifix_h
#define iwlwifix_h

/* for all modules */
#define DRV_NAME        "iwlwifix"

#include <IOKit/IOLib.h>
#include <IOKit/IOBufferMemoryDescriptor.h>
#include <IOKit/pci/IOPCIDevice.h>
#include <IOKit/network/IOEthernetController.h>
#include <IOKit/network/IOEthernetInterface.h>
#include <IOKit/network/IOBasicOutputQueue.h>
#include <IOKit/acpi/IOACPIPlatformDevice.h>
#include <IOKit/IOInterruptEventSource.h>
#include <libkern/OSKextLib.h>
#include "iwl-config.hpp"
#include "iwl-modparams.hpp"
#include "linux/pci_dev.hpp"
#include "fw/img.hpp"

/* radio config bits (actual values from NVM definition) */
#define NVM_RF_CFG_DASH_MSK(x)   (x & 0x3)         /* bits 0-1   */
#define NVM_RF_CFG_STEP_MSK(x)   ((x >> 2)  & 0x3) /* bits 2-3   */
#define NVM_RF_CFG_TYPE_MSK(x)   ((x >> 4)  & 0x3) /* bits 4-5   */
#define NVM_RF_CFG_PNUM_MSK(x)   ((x >> 6)  & 0x3) /* bits 6-7   */
#define NVM_RF_CFG_TX_ANT_MSK(x) ((x >> 8)  & 0xF) /* bits 8-11  */
#define NVM_RF_CFG_RX_ANT_MSK(x) ((x >> 12) & 0xF) /* bits 12-15 */

#define EXT_NVM_RF_CFG_FLAVOR_MSK(x)   ((x) & 0xF)
#define EXT_NVM_RF_CFG_DASH_MSK(x)   (((x) >> 4) & 0xF)
#define EXT_NVM_RF_CFG_STEP_MSK(x)   (((x) >> 8) & 0xF)
#define EXT_NVM_RF_CFG_TYPE_MSK(x)   (((x) >> 12) & 0xFFF)
#define EXT_NVM_RF_CFG_TX_ANT_MSK(x) (((x) >> 24) & 0xF)
#define EXT_NVM_RF_CFG_RX_ANT_MSK(x) (((x) >> 28) & 0xF)

struct firmware {
    UInt8 *data;
    size_t size;
};

struct fw_sec {
    const void *data;   // the sec data
    size_t size;        // section size
    UInt32 offset;      // offset of writing in the device
};

struct fw_img_parsing {
    struct fw_sec *sec;
    int sec_counter;
};

struct __attribute__((packed))  fw_sec_parsing {
    __le32 offset;
    const UInt8 data[];
};

struct __attribute__((packed)) iwl_tlv_calib_data {
    __le32 ucode_type;
    struct iwl_tlv_calib_ctrl calib;
};

struct iwl_firmware_pieces {
    struct fw_img_parsing img[IWL_UCODE_TYPE_MAX];
    
    UInt32 init_evtlog_ptr, init_evtlog_size, init_errlog_ptr;
    UInt32 inst_evtlog_ptr, inst_evtlog_size, inst_errlog_ptr;
    
    /* FW debug data parsed for driver usage */
//    bool dbg_dest_tlv_init;
//    u8 *dbg_dest_ver;
//    union {
//        struct iwl_fw_dbg_dest_tlv *dbg_dest_tlv;
//        struct iwl_fw_dbg_dest_tlv_v1 *dbg_dest_tlv_v1;
//    };
//    struct iwl_fw_dbg_conf_tlv *dbg_conf_tlv[FW_DBG_CONF_MAX];
//    size_t dbg_conf_tlv_len[FW_DBG_CONF_MAX];
//    struct iwl_fw_dbg_trigger_tlv *dbg_trigger_tlv[FW_DBG_TRIGGER_MAX];
//    size_t dbg_trigger_tlv_len[FW_DBG_TRIGGER_MAX];
//    struct iwl_fw_dbg_mem_seg_tlv *dbg_mem_tlv;
//    size_t n_mem_tlv;
};

enum {
    DVM_OP_MODE,
    MVM_OP_MODE,
};

struct iwlwifix_opmode_table {
    const char* name;
    const struct iwl_op_mode_ops *ops;
};

extern struct iwlwifix_opmode_table iwlwifix_opmode_table[];

#define FW_ADDR_CACHE_CONTROL 0xC0000000

extern struct iwl_mod_params iwlwifix_mod_params;

class Iwlwifix : public IOService {
    OSDeclareDefaultStructors(Iwlwifix);
public:
    virtual bool init(OSDictionary *properties) override;
    IOService* probe(IOService* provider, SInt32* score) override;
    virtual bool start(IOService *provider) override;
    virtual void stop(IOService *provider) override;
    virtual void free() override;
    
    /* IONetworkController methods */
//    virtual IOReturn enable(IONetworkInterface *netif) override;
//    virtual IOReturn disable(IONetworkInterface *netif) override;
//    
//    /* Power Management Support */
//    virtual IOReturn registerWithPolicyMaker(IOService *policyMaker);
//    virtual IOReturn setPowerState(unsigned long powerStateOrdinal,
//                                   IOService *policyMaker);
//    virtual void systemWillShutdown(IOOptionBits specifier);
//    
//    
//    virtual void getPacketBufferConstraints(IOPacketBufferConstraints *constraints) const;
//    
//    virtual IOOutputQueue* createOutputQueue();
//    
//    virtual const OSString* newVendorString() const;
//    virtual const OSString* newModelString() const;
//    
//    virtual IOReturn selectMedium(const IONetworkMedium *medium);
//    virtual bool configureInterface(IONetworkInterface *interface);
//    
//    virtual bool createWorkLoop();
//    virtual IOWorkLoop* getWorkLoop() const;
//    
//    /* Methods inherited from IOEthernetController. */
//    virtual IOReturn getHardwareAddress(IOEthernetAddress *addr);
//    virtual IOReturn setHardwareAddress(const IOEthernetAddress *addr);
//    virtual IOReturn setPromiscuousMode(bool active);
//    virtual IOReturn setMulticastMode(bool active);
//    virtual IOReturn setMulticastList(IOEthernetAddress *addrs, UInt32 count);
//    virtual IOReturn getChecksumSupport(UInt32 *checksumMask, UInt32 checksumFamily, bool isOutput);
//    virtual IOReturn getMinPacketSize(UInt32 *minSize) const;
//    virtual IOReturn setWakeOnMagicPacket(bool active);
//    virtual IOReturn getPacketFilters(const OSSymbol *group, UInt32 *filters) const;
//    
//    virtual UInt32 getFeatures() const;
//    virtual IOReturn getMaxPacketSize(UInt32 * maxSize) const;
//    virtual IOReturn setMaxPacketSize(UInt32 maxSize);
//    
private:
    bool initPCIConfigSpace(IOPCIDevice *provider);
    bool setupDMADescriptors();
    void freeDMADescriptors();
    bool initEventSources(IOService *provider);
    inline void intelInitConfig(IOPCIDevice *provider);
    inline void intelEnablePCIDevice(IOPCIDevice *provider);
    inline void intelDisablePCIDevice(IOPCIDevice *provider);
    
//  ====================== iwl-drv.c public functions ====================
public:
    struct iwl_op_mode* _iwl_op_mode_start(struct iwlwifix_opmode_table *op);
    void _iwl_op_mode_stop(void);
    bool firmwareLoaded;
    IOLock *firmwareLoadLock;
// ======================= iwl-drv.c private functions ===========
private:
    IOReturn iwl_drv_start(struct iwl_trans *transn);
    void iwl_drv_stop(void);
    static void iwl_req_fw_callback(OSKextRequestTag reqTag, OSReturn result, const void* resData, UInt32 resDataLen, void *context);
    void iwl_free_fw_desc(struct fw_desc *desc);
    void iwl_free_fw_img(struct fw_img *img);
    void iwl_dealloc_ucode(void);
    int  iwl_alloc_fw_desc(struct fw_desc *desc, struct fw_sec *sec);
    int  iwl_request_firmware(void);
    struct fw_sec *get_sec(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec);
    void alloc_sec_data(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec);
    void set_sec_data(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec, const void *data);
    void set_sec_size(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec, size_t size);
    size_t get_sec_size(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec);
    void set_sec_offset(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec, UInt32 offset);
    int iwl_store_cscheme(struct iwl_fw *fw, const UInt8 *data, const UInt32 len);
    int iwl_store_ucode_sec(struct iwl_firmware_pieces *pieces, const void* data, enum iwl_ucode_type type, int size);
    int iwl_set_default_calib(const UInt8 *data);
    void iwl_set_ucode_api_flags(const UInt8 *data, struct iwl_ucode_capabilities *capa);
    void iwl_set_ucode_capabilities(const UInt8 *data, struct iwl_ucode_capabilities *capa);
    // int iwl_parse_v1_v2_firmware(const struct firmware* ucode_raw, struct iwl_firmware_pieces *pieces); // DISABLED
    int iwl_parse_tlv_firmware(const struct firmware *ucode_raw, struct iwl_firmware_pieces *pieces, struct iwl_ucode_capabilities *capa, bool *usniffer_images);
    int iwl_alloc_ucode(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type);
    int validate_sizes(struct iwl_firmware_pieces *pieces, const struct iwl_cfg *cfg);
public:
    IOPCIDevice *pciDevice;
    IOACPIPlatformDevice *acpiDevice;
    
// ============================ Custom functions ===================
public:
    void setDMAMask(UInt64 mask);
    void setConsistentDMAMask(UInt64 mask);
    
private:
    // Basic Input/Output
    IOWorkLoop              *iwlWorkLoop;
    IOCommandGate           *commandGate;
    IOEthernetInterface     *netif;
    IOBasicOutputQueue      *txQeueue;
    IOInterruptEventSource  *interruptSource;
    IOTimerEventSource      *timerSource;
    
    /* tansmitter data */
    IODMACommand            *txDescDmaCmd;
    IOBufferMemoryDescriptor *txBufDesc;
    IOPhysicalAddress64     *txPhyAddr;
    
    /* receiver data */
    IODMACommand            *rxDescDmaCmd;
    IOBufferMemoryDescriptor *rxBufDesc;
    IOPhysicalAddress64     *rxPhyAddr;
    
    bool isEnabled;
    struct pci_dev pciDeviceData;
    int device_cardid;
    
    const iwl_cfg *cfg;
    
/********************  struct iwl_drv  members **************************/
    struct iwl_fw fw;
    struct iwl_op_mode *op_mode;
    struct iwl_trans *trans;
    int fw_index;            /* firmware we're trying to load  */
    char firmware_name[64];  /* name of firmware file to load */
    
    UInt64 dma_mask;
    UInt64 coherent_dma_mask;

private:
    void releaseAll(void);
    void loadFirmware(void);
};

#endif
