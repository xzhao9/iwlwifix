//
//  notif-wait.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-09.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef notif_wait_hpp
#define notif_wait_hpp

#include <IOKit/IOLib.h>
#include <IOKit/IOLocks.h>
#include <libkern/c++/OSArray.h>
#include "../linux/waitq.hpp"
#include "../iwl-trans.hpp"

struct iwl_notif_wait_data {
    IOSimpleLock *notif_wait_lock;
};

#define MAX_NOTIF_CMDS    5

#endif /* notif_wait_hpp */
