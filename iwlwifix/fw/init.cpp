//
//  init.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-08.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//
#include "runtime.hpp"

void iwl_fw_runtime_init(struct iwl_fw_runtime *fwrt, struct iwl_trans *trans,
                         const struct iwl_fw *fw,
                         const struct iwl_fw_runtime_ops *ops, void *ops_ctx,
                         struct dentry *dbgfs_dir)
{
    memset(fwrt, 0, sizeof(*fwrt));
    fwrt->trans = trans;
    fwrt->fw = fw;
//    fwrt->dev = trans->dev;
//     fwrt->dump.conf = FW_DBG_INVALID;
    fwrt->ops = ops;
    fwrt->ops_ctx = ops_ctx;
 //    INIT_DELAYED_WORK(&fwrt->dump.wk, iwl_fw_error_dump_wk);
  //   iwl_fwrt_dbgfs_register(fwrt, dbgfs_dir);
  //   timer_setup(&fwrt->dump.periodic_trig,
   //              iwl_fw_dbg_periodic_trig_handler, 0);
}

void iwl_fw_runtime_suspend(struct iwl_fw_runtime *fwrt)
{
   //  iwl_fw_suspend_timestamp(fwrt);
}

void iwl_fw_runtime_resume(struct iwl_fw_runtime *fwrt)
{
    // iwl_fw_resume_timestamp(fwrt);
}
