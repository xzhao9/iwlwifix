//
//  cmdhdr.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef fwapi_cmdhdr_h
#define fwapi_cmdhdr_h

/**
 * DOC: Host command section
 *
 * A host command is a command issued by the upper layer to the fw. There are
 * several versions of fw that have several APIs. The transport layer is
 * completely agnostic to these differences.
 * The transport does provide helper functionality (i.e. SYNC / ASYNC mode),
 */

#define SEQ_TO_QUEUE(s)    (((s) >> 8) & 0x1f)
#define QUEUE_TO_SEQ(q)    (((q) & 0x1f) << 8)
#define SEQ_TO_INDEX(s)    ((s) & 0xff)
#define INDEX_TO_SEQ(i)    ((i) & 0xff)
#define SEQ_RX_FRAME    cpu_to_le16(0x8000)

/*
 * those functions retrieve specific information from
 * the id field in the iwl_host_cmd struct which contains
 * the command id, the group id and the version of the command
 * and vice versa
 */
static inline UInt8 iwl_cmd_opcode(UInt32 cmdid)
{
    return cmdid & 0xFF;
}

static inline UInt8 iwl_cmd_groupid(UInt32 cmdid)
{
    return ((cmdid & 0xFF00) >> 8);
}

static inline UInt8 iwl_cmd_version(UInt32 cmdid)
{
    return ((cmdid & 0xFF0000) >> 16);
}

static inline UInt32 iwl_cmd_id(UInt8 opcode, UInt8 groupid, UInt8 version)
{
    return opcode + (groupid << 8) + (version << 16);
}

/* make u16 wide id out of u8 group and opcode */
#define WIDE_ID(grp, opcode) (((grp) << 8) | (opcode))
#define DEF_ID(opcode) ((1 << 8) | (opcode))

/* due to the conversion, this group is special; new groups
 * should be defined in the appropriate fw-api header files
 */
#define IWL_ALWAYS_LONG_GROUP    1

/**
 * struct iwl_cmd_header - (short) command header format
 *
 * This header format appears in the beginning of each command sent from the
 * driver, and each response/notification received from uCode.
 */
struct __attribute__((packed)) iwl_cmd_header  {
    /**
     * @cmd: Command ID: REPLY_RXON, etc.
     */
    UInt8 cmd;
    /**
     * @group_id: group ID, for commands with groups
     */
    UInt8 group_id;
    /**
     * @sequence:
     * Sequence number for the command.
     *
     * The driver sets up the sequence number to values of its choosing.
     * uCode does not use this value, but passes it back to the driver
     * when sending the response to each driver-originated command, so
     * the driver can match the response to the command.  Since the values
     * don't get used by uCode, the driver may set up an arbitrary format.
     *
     * There is one exception:  uCode sets bit 15 when it originates
     * the response/notification, i.e. when the response/notification
     * is not a direct response to a command sent by the driver.  For
     * example, uCode issues REPLY_RX when it sends a received frame
     * to the driver; it is not a direct response to any driver command.
     *
     * The Linux driver uses the following format:
     *
     *  0:7        tfd index - position within TX queue
     *  8:12    TX queue id
     *  13:14    reserved
     *  15        unsolicited RX or uCode-originated notification
     */
    __le16 sequence;
};

/**
 * struct iwl_cmd_header_wide
 *
 * This header format appears in the beginning of each command sent from the
 * driver, and each response/notification received from uCode.
 * this is the wide version that contains more information about the command
 * like length, version and command type
 *
 * @cmd: command ID, like in &struct iwl_cmd_header
 * @group_id: group ID, like in &struct iwl_cmd_header
 * @sequence: sequence, like in &struct iwl_cmd_header
 * @length: length of the command
 * @reserved: reserved
 * @version: command version
 */
struct __attribute__((packed)) iwl_cmd_header_wide {
    UInt8 cmd;
    UInt8 group_id;
    __le16 sequence;
    __le16 length;
    UInt8 reserved;
    UInt8 version;
};

/**
 * struct iwl_calib_res_notif_phy_db - Receive phy db chunk after calibrations
 * @type: type of the result - mostly ignored
 * @length: length of the data
 * @data: data, length in @length
 */
struct __attribute__((packed)) iwl_calib_res_notif_phy_db  {
    __le16 type;
    __le16 length;
    UInt8 data[];
};

/**
 * struct iwl_phy_db_cmd - configure operational ucode
 * @type: type of the data
 * @length: length of the data
 * @data: data, length in @length
 */
struct  __attribute__((packed)) iwl_phy_db_cmd {
    __le16 type;
    __le16 length;
    UInt8 data[];
} ;

/**
 * struct iwl_cmd_response - generic response struct for most commands
 * @status: status of the command asked, changes for each one
 */
struct iwl_cmd_response {
    __le32 status;
};

#endif /* cmdhdr_h */
