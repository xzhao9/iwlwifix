//
//  context.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-08.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef fwapi_context_h
#define fwapi_context_h

/**
 * enum iwl_ctxt_id_and_color - ID and color fields in context dword
 * @FW_CTXT_ID_POS: position of the ID
 * @FW_CTXT_ID_MSK: mask of the ID
 * @FW_CTXT_COLOR_POS: position of the color
 * @FW_CTXT_COLOR_MSK: mask of the color
 * @FW_CTXT_INVALID: value used to indicate unused/invalid
 */
enum iwl_ctxt_id_and_color {
    FW_CTXT_ID_POS        = 0,
    FW_CTXT_ID_MSK        = 0xff << FW_CTXT_ID_POS,
    FW_CTXT_COLOR_POS    = 8,
    FW_CTXT_COLOR_MSK    = 0xff << FW_CTXT_COLOR_POS,
    FW_CTXT_INVALID        = 0xffffffff,
};

#define FW_CMD_ID_AND_COLOR(_id, _color) (((_id) << FW_CTXT_ID_POS) |\
((_color) << FW_CTXT_COLOR_POS))

/* Possible actions on PHYs, MACs and Bindings */
enum iwl_ctxt_action {
    FW_CTXT_ACTION_STUB = 0,
    FW_CTXT_ACTION_ADD,
    FW_CTXT_ACTION_MODIFY,
    FW_CTXT_ACTION_REMOVE,
    FW_CTXT_ACTION_NUM
}; /* COMMON_CONTEXT_ACTION_API_E_VER_1 */

#endif /* context_h */
