//
//  led.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-08.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef fwapi_led_h
#define fwapi_led_h

/**
 * struct iwl_led_cmd - LED switching command
 *
 * @status: LED status (on/off)
 */
struct __attribute__((packed)) iwl_led_cmd {
    __le32 status;
}; /* LEDS_CMD_API_S_VER_2 */

#endif /* led_h */
