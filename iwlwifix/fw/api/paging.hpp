//
//  paging.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-08.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef fwapi_paging_h
#define fwapi_paging_h

#define NUM_OF_FW_PAGING_BLOCKS    33 /* 32 for data and 1 block for CSS */

/**
 * struct iwl_fw_paging_cmd - paging layout
 *
 * Send to FW the paging layout in the driver.
 *
 * @flags: various flags for the command
 * @block_size: the block size in powers of 2
 * @block_num: number of blocks specified in the command.
 * @device_phy_addr: virtual addresses from device side
 */
struct __attribute__((packed)) iwl_fw_paging_cmd {
    __le32 flags;
    __le32 block_size;
    __le32 block_num;
    __le32 device_phy_addr[NUM_OF_FW_PAGING_BLOCKS];
}; /* FW_PAGING_BLOCK_CMD_API_S_VER_1 */

#endif /* paging_h */
