//
//  phy-ctxt.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-08.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef fwapi_phy_ctxt_h
#define fwapi_phy_ctxt_h

/* Supported bands */
#define PHY_BAND_5  (0)
#define PHY_BAND_24 (1)

/* Supported channel width, vary if there is VHT support */
#define PHY_VHT_CHANNEL_MODE20    (0x0)
#define PHY_VHT_CHANNEL_MODE40    (0x1)
#define PHY_VHT_CHANNEL_MODE80    (0x2)
#define PHY_VHT_CHANNEL_MODE160    (0x3)

/*
 * Control channel position:
 * For legacy set bit means upper channel, otherwise lower.
 * For VHT - bit-2 marks if the control is lower/upper relative to center-freq
 *   bits-1:0 mark the distance from the center freq. for 20Mhz, offset is 0.
 *                                   center_freq
 *                                        |
 * 40Mhz                          |_______|_______|
 * 80Mhz                  |_______|_______|_______|_______|
 * 160Mhz |_______|_______|_______|_______|_______|_______|_______|_______|
 * code      011     010     001     000  |  100     101     110    111
 */
#define PHY_VHT_CTRL_POS_1_BELOW  (0x0)
#define PHY_VHT_CTRL_POS_2_BELOW  (0x1)
#define PHY_VHT_CTRL_POS_3_BELOW  (0x2)
#define PHY_VHT_CTRL_POS_4_BELOW  (0x3)
#define PHY_VHT_CTRL_POS_1_ABOVE  (0x4)
#define PHY_VHT_CTRL_POS_2_ABOVE  (0x5)
#define PHY_VHT_CTRL_POS_3_ABOVE  (0x6)
#define PHY_VHT_CTRL_POS_4_ABOVE  (0x7)

/*
 * struct iwl_fw_channel_info_v1 - channel information
 *
 * @band: PHY_BAND_*
 * @channel: channel number
 * @width: PHY_[VHT|LEGACY]_CHANNEL_*
 * @ctrl channel: PHY_[VHT|LEGACY]_CTRL_*
 */
struct __attribute__((packed)) iwl_fw_channel_info_v1 {
    UInt8 band;
    UInt8 channel;
    UInt8 width;
    UInt8 ctrl_pos;
}; /* CHANNEL_CONFIG_API_S_VER_1 */

/*
 * struct iwl_fw_channel_info - channel information
 *
 * @channel: channel number
 * @band: PHY_BAND_*
 * @width: PHY_[VHT|LEGACY]_CHANNEL_*
 * @ctrl channel: PHY_[VHT|LEGACY]_CTRL_*
 * @reserved: for future use and alignment
 */
struct __attribute__((packed)) iwl_fw_channel_info {
    __le32 channel;
    UInt8 band;
    UInt8 width;
    UInt8 ctrl_pos;
    UInt8 reserved;
}; /*CHANNEL_CONFIG_API_S_VER_2 */

#define PHY_RX_CHAIN_DRIVER_FORCE_POS    (0)
#define PHY_RX_CHAIN_DRIVER_FORCE_MSK \
(0x1 << PHY_RX_CHAIN_DRIVER_FORCE_POS)
#define PHY_RX_CHAIN_VALID_POS        (1)
#define PHY_RX_CHAIN_VALID_MSK \
(0x7 << PHY_RX_CHAIN_VALID_POS)
#define PHY_RX_CHAIN_FORCE_SEL_POS    (4)
#define PHY_RX_CHAIN_FORCE_SEL_MSK \
(0x7 << PHY_RX_CHAIN_FORCE_SEL_POS)
#define PHY_RX_CHAIN_FORCE_MIMO_SEL_POS    (7)
#define PHY_RX_CHAIN_FORCE_MIMO_SEL_MSK \
(0x7 << PHY_RX_CHAIN_FORCE_MIMO_SEL_POS)
#define PHY_RX_CHAIN_CNT_POS        (10)
#define PHY_RX_CHAIN_CNT_MSK \
(0x3 << PHY_RX_CHAIN_CNT_POS)
#define PHY_RX_CHAIN_MIMO_CNT_POS    (12)
#define PHY_RX_CHAIN_MIMO_CNT_MSK \
(0x3 << PHY_RX_CHAIN_MIMO_CNT_POS)
#define PHY_RX_CHAIN_MIMO_FORCE_POS    (14)
#define PHY_RX_CHAIN_MIMO_FORCE_MSK \
(0x1 << PHY_RX_CHAIN_MIMO_FORCE_POS)

/* TODO: fix the value, make it depend on firmware at runtime? */
#define NUM_PHY_CTX    3

/* TODO: complete missing documentation */
/**
 * struct iwl_phy_context_cmd_tail - tail of iwl_phy_ctx_cmd for alignment with
 *    various channel structures.
 *
 * @txchain_info: ???
 * @rxchain_info: ???
 * @acquisition_data: ???
 * @dsp_cfg_flags: set to 0
 */
struct  __attribute__((packed)) iwl_phy_context_cmd_tail {
    __le32 txchain_info;
    __le32 rxchain_info;
    __le32 acquisition_data;
    __le32 dsp_cfg_flags;
};

/**
 * struct iwl_phy_context_cmd - config of the PHY context
 * ( PHY_CONTEXT_CMD = 0x8 )
 * @id_and_color: ID and color of the relevant Binding
 * @action: action to perform, one of FW_CTXT_ACTION_*
 * @apply_time: 0 means immediate apply and context switch.
 *    other value means apply new params after X usecs
 * @tx_param_color: ???
 * @ci: channel info
 * @tail: command tail
 */
struct __attribute__((packed)) iwl_phy_context_cmd {
    /* COMMON_INDEX_HDR_API_S_VER_1 */
    __le32 id_and_color;
    __le32 action;
    /* PHY_CONTEXT_DATA_API_S_VER_1 */
    __le32 apply_time;
    __le32 tx_param_color;
    struct iwl_fw_channel_info ci;
    struct iwl_phy_context_cmd_tail tail;
}; /* PHY_CONTEXT_CMD_API_VER_1 */

#endif /* phy_ctxt_h */
