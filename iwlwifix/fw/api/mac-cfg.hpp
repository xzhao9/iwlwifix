//
//  mac-cfg.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-08.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef fwapi_mac_cfg_h
#define fwapi_mac_cfg_h

/**
 * enum iwl_mac_conf_subcmd_ids - mac configuration command IDs
 */
enum iwl_mac_conf_subcmd_ids {
    /**
     * @LOW_LATENCY_CMD: &struct iwl_mac_low_latency_cmd
     */
    LOW_LATENCY_CMD = 0x3,
    /**
     * @CHANNEL_SWITCH_TIME_EVENT_CMD: &struct iwl_chan_switch_te_cmd
     */
    CHANNEL_SWITCH_TIME_EVENT_CMD = 0x4,
    /**
     * @PROBE_RESPONSE_DATA_NOTIF: &struct iwl_probe_resp_data_notif
     */
    PROBE_RESPONSE_DATA_NOTIF = 0xFC,
    
    /**
     * @CHANNEL_SWITCH_NOA_NOTIF: &struct iwl_channel_switch_noa_notif
     */
    CHANNEL_SWITCH_NOA_NOTIF = 0xFF,
};

#define IWL_P2P_NOA_DESC_COUNT    (2)

/**
 * struct iwl_p2p_noa_attr - NOA attr contained in probe resp FW notification
 *
 * @id: attribute id
 * @len_low: length low half
 * @len_high: length high half
 * @idx: instance of NoA timing
 * @ctwin: GO's ct window and pwer save capability
 * @desc: NoA descriptor
 * @reserved: reserved for alignment purposes
 */
struct __attribute__((packed)) iwl_p2p_noa_attr {
    UInt8 id;
    UInt8 len_low;
    UInt8 len_high;
    UInt8 idx;
    UInt8 ctwin;
    struct ieee80211_p2p_noa_desc desc[IWL_P2P_NOA_DESC_COUNT];
    UInt8 reserved;
};

#define IWL_PROBE_RESP_DATA_NO_CSA (0xff)

/**
 * struct iwl_probe_resp_data_notif - notification with NOA and CSA counter
 *
 * @mac_id: the mac which should send the probe response
 * @noa_active: notifies if the noa attribute should be handled
 * @noa_attr: P2P NOA attribute
 * @csa_counter: current csa counter
 * @reserved: reserved for alignment purposes
 */
struct __attribute__((packed)) iwl_probe_resp_data_notif {
    __le32 mac_id;
    __le32 noa_active;
    struct iwl_p2p_noa_attr noa_attr;
    UInt8 csa_counter;
    UInt8 reserved[3];
}; /* PROBE_RESPONSE_DATA_NTFY_API_S_VER_1 */

/**
 * struct iwl_channel_switch_noa_notif - Channel switch NOA notification
 *
 * @id_and_color: ID and color of the MAC
 */
struct __attribute__((packed)) iwl_channel_switch_noa_notif {
    __le32 id_and_color;
}; /* CHANNEL_SWITCH_START_NTFY_API_S_VER_1 */

/**
 * struct iwl_chan_switch_te_cmd - Channel Switch Time Event command
 *
 * @mac_id: MAC ID for channel switch
 * @action: action to perform, one of FW_CTXT_ACTION_*
 * @tsf: beacon tsf
 * @cs_count: channel switch count from CSA/eCSA IE
 * @cs_delayed_bcn_count: if set to N (!= 0) GO/AP can delay N beacon intervals
 *    at the new channel after the channel switch, otherwise (N == 0) expect
 *    beacon right after the channel switch.
 * @cs_mode: 1 - quiet, 0 - otherwise
 * @reserved: reserved for alignment purposes
 */
struct __attribute__((packed)) iwl_chan_switch_te_cmd {
    __le32 mac_id;
    __le32 action;
    __le32 tsf;
    UInt8 cs_count;
    UInt8 cs_delayed_bcn_count;
    UInt8 cs_mode;
    UInt8 reserved;
}; /* MAC_CHANNEL_SWITCH_TIME_EVENT_S_VER_2 */

/**
 * struct iwl_mac_low_latency_cmd - set/clear mac to 'low-latency mode'
 *
 * @mac_id: MAC ID to whom to apply the low-latency configurations
 * @low_latency_rx: 1/0 to set/clear Rx low latency direction
 * @low_latency_tx: 1/0 to set/clear Tx low latency direction
 * @reserved: reserved for alignment purposes
 */
struct __attribute__((packed)) iwl_mac_low_latency_cmd {
    __le32 mac_id;
    UInt8 low_latency_rx;
    UInt8 low_latency_tx;
    __le16 reserved;
}; /* MAC_LOW_LATENCY_API_S_VER_1 */

#endif /* mac_cfg_h */
