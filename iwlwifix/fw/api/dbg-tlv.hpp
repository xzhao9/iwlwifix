//
//  dbg-tlv.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef dbg_tlv_h
#define dbg_tlv_h

#include "../../linux/types.hpp"
#include "../../linux/bitops.hpp"

/**
 * struct iwl_fw_ini_header: Common Header for all debug group TLV's structures
 *
 * @tlv_version: version info
 * @apply_point: &enum iwl_fw_ini_apply_point
 * @data: TLV data followed
 */
struct __attribute__((packed)) iwl_fw_ini_header {
    __le32 tlv_version;
    __le32 apply_point;
    UInt8 data[];
}; /* FW_DEBUG_TLV_HEADER_S */

/**
 * struct iwl_fw_ini_allocation_tlv - (IWL_UCODE_TLV_TYPE_BUFFER_ALLOCATION)
 * buffer allocation TLV - for debug
 *
 * @iwl_fw_ini_header: header
 * @allocation_id: &enum iwl_fw_ini_allocation_id - to bind allocation and hcmd
 *    if needed (DBGC1/DBGC2/SDFX/...)
 * @buffer_location: type of iwl_fw_ini_buffer_location
 * @size: size in bytes
 * @max_fragments: the maximum allowed fragmentation in the desired memory
 *    allocation above
 * @min_frag_size: the minimum allowed fragmentation size in bytes
 */
struct __attribute__((packed)) iwl_fw_ini_allocation_tlv {
    struct iwl_fw_ini_header header;
    __le32 allocation_id;
    __le32 buffer_location;
    __le32 size;
    __le32 max_fragments;
    __le32 min_frag_size;
}; /* FW_DEBUG_TLV_BUFFER_ALLOCATION_TLV_S_VER_1 */

/**
 * enum iwl_fw_ini_dbg_domain - debug domains
 * allows to send host cmd or collect memory region if a given domain is enabled
 *
 * @IWL_FW_INI_DBG_DOMAIN_ALWAYS_ON: the default domain, always on
 * @IWL_FW_INI_DBG_DOMAIN_REPORT_PS: power save domain
 */
enum iwl_fw_ini_dbg_domain {
    IWL_FW_INI_DBG_DOMAIN_ALWAYS_ON = 0,
    IWL_FW_INI_DBG_DOMAIN_REPORT_PS,
}; /* FW_DEBUG_TLV_DOMAIN_API_E_VER_1 */

/**
 * struct iwl_fw_ini_hcmd
 *
 * @id: the debug configuration command type for instance: 0xf6 / 0xf5 / DHC
 * @group: the desired cmd group
 * @reserved: to align to FW struct
 * @data: all of the relevant command data to be sent
 */
struct __attribute__((packed)) iwl_fw_ini_hcmd {
    UInt8 id;
    UInt8 group;
    __le16 reserved;
    UInt8 data[0];
}; /* FW_DEBUG_TLV_HCMD_DATA_API_S_VER_1 */

/**
 * struct iwl_fw_ini_hcmd_tlv - (IWL_UCODE_TLV_TYPE_HCMD)
 * Generic Host command pass through TLV
 *
 * @header: header
 * @domain: send command only if the specific domain is enabled
 *    &enum iwl_fw_ini_dbg_domain
 * @period_msec: period in which the hcmd will be sent to FW. Measured in msec
 *    (0 = one time command).
 * @hcmd: a variable length host-command to be sent to apply the configuration.
 */
struct __attribute__((packed)) iwl_fw_ini_hcmd_tlv {
    struct iwl_fw_ini_header header;
    __le32 domain;
    __le32 period_msec;
    struct iwl_fw_ini_hcmd hcmd;
}; /* FW_DEBUG_TLV_HCMD_API_S_VER_1 */

/**
 * struct iwl_fw_ini_debug_flow_tlv - (IWL_UCODE_TLV_TYPE_DEBUG_FLOW)
 *
 * @header: header
 * @debug_flow_cfg: &enum iwl_fw_ini_debug_flow
 */
struct __attribute__((packed)) iwl_fw_ini_debug_flow_tlv {
    struct iwl_fw_ini_header header;
    __le32 debug_flow_cfg;
}; /* FW_DEBUG_TLV_FLOW_TLV_S_VER_1 */

#define IWL_FW_INI_MAX_REGION_ID    64
#define IWL_FW_INI_MAX_NAME        32

/**
 * struct iwl_fw_ini_region_cfg_dhc - defines dhc response to dump.
 *
 * @id_and_grp: id and group of dhc response.
 * @desc: dhc response descriptor.
 */
struct __attribute__((packed)) iwl_fw_ini_region_cfg_dhc {
    __le32 id_and_grp;
    __le32 desc;
}; /* FW_DEBUG_TLV_REGION_DHC_API_S_VER_1 */

/**
 * struct iwl_fw_ini_region_cfg_internal - meta data of internal memory region
 *
 * @num_of_range: the amount of ranges in the region
 * @range_data_size: size of the data to read per range, in bytes.
 */
struct __attribute__((packed)) iwl_fw_ini_region_cfg_internal {
    __le32 num_of_ranges;
    __le32 range_data_size;
}; /* FW_DEBUG_TLV_REGION_NIC_INTERNAL_RANGES_S */

/**
 * struct iwl_fw_ini_region_cfg_fifos - meta data of fifos region
 *
 * @fid1: fifo id 1 - bitmap of lmac tx/rx fifos to include in the region
 * @fid2: fifo id 2 - bitmap of umac rx fifos to include in the region.
 *    It is unused for tx.
 * @num_of_registers: number of prph registers in the region, each register is
 *    4 bytes size.
 * @header_only: none zero value indicates that this region does not include
 *    fifo data and includes only the given registers.
 */
struct __attribute__((packed)) iwl_fw_ini_region_cfg_fifos {
    __le32 fid1;
    __le32 fid2;
    __le32 num_of_registers;
    __le32 header_only;
}; /* FW_DEBUG_TLV_REGION_FIFOS_S */

/**
 * struct iwl_fw_ini_region_cfg
 *
 * @region_id: ID of this dump configuration
 * @region_type: &enum iwl_fw_ini_region_type
 * @domain: dump this region only if the specific domain is enabled
 *    &enum iwl_fw_ini_dbg_domain
 * @name_len: name length
 * @name: file name to use for this region
 * @internal: used in case the region uses internal memory.
 * @allocation_id: For DRAM type field substitutes for allocation_id
 * @fifos: used in case of fifos region.
 * @dhc_desc: dhc response descriptor.
 * @notif_id_and_grp: dump this region only if the specific notification
 *    occurred.
 * @offset: offset to use for each memory base address
 * @start_addr: array of addresses.
 */
struct __attribute__((packed)) iwl_fw_ini_region_cfg {
    __le32 region_id;
    __le32 region_type;
    __le32 domain;
    __le32 name_len;
    UInt8 name[IWL_FW_INI_MAX_NAME];
    union {
        struct iwl_fw_ini_region_cfg_internal internal;
        __le32 allocation_id;
        struct iwl_fw_ini_region_cfg_fifos fifos;
        struct iwl_fw_ini_region_cfg_dhc dhc_desc;
        __le32 notif_id_and_grp;
    }; /* FW_DEBUG_TLV_REGION_EXT_INT_PARAMS_API_U_VER_1 */
    __le32 offset;
    __le32 start_addr[];
}; /* FW_DEBUG_TLV_REGION_CONFIG_API_S_VER_1 */

/**
 * struct iwl_fw_ini_region_tlv - (IWL_UCODE_TLV_TYPE_REGIONS)
 * defines memory regions to dump
 *
 * @header: header
 * @num_regions: how many different region section and IDs are coming next
 * @region_config: list of dump configurations
 */
struct __attribute__((packed)) iwl_fw_ini_region_tlv {
    struct iwl_fw_ini_header header;
    __le32 num_regions;
    struct iwl_fw_ini_region_cfg region_config[];
}; /* FW_DEBUG_TLV_REGIONS_API_S_VER_1 */

/**
 * struct iwl_fw_ini_trigger
 *
 * @trigger_id: &enum iwl_fw_ini_trigger_id
 * @override_trig: determines how apply trigger in case a trigger with the
 *    same id is already in use. Using the first 2 bytes:
 *    Byte 0: if 0, override trigger configuration, otherwise use the
 *    existing configuration.
 *    Byte 1: if 0, override trigger regions, otherwise append regions to
 *    existing trigger.
 * @dump_delay: delay from trigger fire to dump, in usec
 * @occurrences: max amount of times to be fired
 * @reserved: to align to FW struct
 * @ignore_consec: ignore consecutive triggers, in usec
 * @force_restart: force FW restart
 * @multi_dut: initiate debug dump data on several DUTs
 * @trigger_data: generic data to be utilized per trigger
 * @num_regions: number of dump regions defined for this trigger
 * @data: region IDs
 */
struct __attribute__((packed)) iwl_fw_ini_trigger {
    __le32 trigger_id;
    __le32 override_trig;
    __le32 dump_delay;
    __le32 occurrences;
    __le32 reserved;
    __le32 ignore_consec;
    __le32 force_restart;
    __le32 multi_dut;
    __le32 trigger_data;
    __le32 num_regions;
    __le32 data[];
}; /* FW_TLV_DEBUG_TRIGGER_CONFIG_API_S_VER_1 */

/**
 * struct iwl_fw_ini_trigger_tlv - (IWL_UCODE_TLV_TYPE_TRIGGERS)
 * Triggers that hold memory regions to dump in case a trigger fires
 *
 * @header: header
 * @num_triggers: how many different triggers section and IDs are coming next
 * @trigger_config: list of trigger configurations
 */
struct __attribute__((packed)) iwl_fw_ini_trigger_tlv {
    struct iwl_fw_ini_header header;
    __le32 num_triggers;
    struct iwl_fw_ini_trigger trigger_config[];
}; /* FW_TLV_DEBUG_TRIGGERS_API_S_VER_1 */

/**
 * enum iwl_fw_ini_trigger_id
 *
 * @IWL_FW_TRIGGER_ID_FW_ASSERT: FW assert
 * @IWL_FW_TRIGGER_ID_FW_HW_ERROR: HW assert
 * @IWL_FW_TRIGGER_ID_FW_TFD_Q_HANG: TFD queue hang
 * @IWL_FW_TRIGGER_ID_FW_DEBUG_HOST_TRIGGER: FW debug notification
 * @IWL_FW_TRIGGER_ID_FW_GENERIC_NOTIFICATION: FW generic notification
 * @IWL_FW_TRIGGER_ID_USER_TRIGGER: User trigger
 * @IWL_FW_TRIGGER_ID_PERIODIC_TRIGGER: triggers periodically
 * @IWL_FW_TRIGGER_ID_HOST_PEER_CLIENT_INACTIVITY: peer inactivity
 * @IWL_FW_TRIGGER_ID_HOST_TX_LATENCY_THRESHOLD_CROSSED: TX latency
 *    threshold was crossed
 * @IWL_FW_TRIGGER_ID_HOST_TX_RESPONSE_STATUS_FAILED: TX failed
 * @IWL_FW_TRIGGER_ID_HOST_OS_REQ_DEAUTH_PEER: Deauth initiated by host
 * @IWL_FW_TRIGGER_ID_HOST_STOP_GO_REQUEST: stop GO request
 * @IWL_FW_TRIGGER_ID_HOST_START_GO_REQUEST: start GO request
 * @IWL_FW_TRIGGER_ID_HOST_JOIN_GROUP_REQUEST: join P2P group request
 * @IWL_FW_TRIGGER_ID_HOST_SCAN_START: scan started event
 * @IWL_FW_TRIGGER_ID_HOST_SCAN_SUBMITTED: undefined
 * @IWL_FW_TRIGGER_ID_HOST_SCAN_PARAMS: undefined
 * @IWL_FW_TRIGGER_ID_HOST_CHECK_FOR_HANG: undefined
 * @IWL_FW_TRIGGER_ID_HOST_BAR_RECEIVED: BAR frame was received
 * @IWL_FW_TRIGGER_ID_HOST_AGG_TX_RESPONSE_STATUS_FAILED: agg TX failed
 * @IWL_FW_TRIGGER_ID_HOST_EAPOL_TX_RESPONSE_FAILED: EAPOL TX failed
 * @IWL_FW_TRIGGER_ID_HOST_FAKE_TX_RESPONSE_SUSPECTED: suspicious TX response
 * @IWL_FW_TRIGGER_ID_HOST_AUTH_REQ_FROM_ASSOC_CLIENT: received suspicious auth
 * @IWL_FW_TRIGGER_ID_HOST_ROAM_COMPLETE: roaming was completed
 * @IWL_FW_TRIGGER_ID_HOST_AUTH_ASSOC_FAST_FAILED: fast assoc failed
 * @IWL_FW_TRIGGER_ID_HOST_D3_START: D3 start
 * @IWL_FW_TRIGGER_ID_HOST_D3_END: D3 end
 * @IWL_FW_TRIGGER_ID_HOST_BSS_MISSED_BEACONS: missed beacon events
 * @IWL_FW_TRIGGER_ID_HOST_P2P_CLIENT_MISSED_BEACONS: P2P missed beacon events
 * @IWL_FW_TRIGGER_ID_HOST_PEER_CLIENT_TX_FAILURES:  undefined
 * @IWL_FW_TRIGGER_ID_HOST_TX_WFD_ACTION_FRAME_FAILED: undefined
 * @IWL_FW_TRIGGER_ID_HOST_AUTH_ASSOC_FAILED: authentication / association
 *    failed
 * @IWL_FW_TRIGGER_ID_HOST_SCAN_COMPLETE: scan complete event
 * @IWL_FW_TRIGGER_ID_HOST_SCAN_ABORT: scan abort complete
 * @IWL_FW_TRIGGER_ID_HOST_NIC_ALIVE: nic alive message was received
 * @IWL_FW_TRIGGER_ID_HOST_CHANNEL_SWITCH_COMPLETE: CSA was completed
 * @IWL_FW_TRIGGER_ID_NUM: number of trigger IDs
 */
enum iwl_fw_ini_trigger_id {
    IWL_FW_TRIGGER_ID_INVALID                = 0,
    
    /* Errors triggers */
    IWL_FW_TRIGGER_ID_FW_ASSERT                = 1,
    IWL_FW_TRIGGER_ID_FW_HW_ERROR                = 2,
    IWL_FW_TRIGGER_ID_FW_TFD_Q_HANG                = 3,
    
    /* FW triggers */
    IWL_FW_TRIGGER_ID_FW_DEBUG_HOST_TRIGGER            = 4,
    IWL_FW_TRIGGER_ID_FW_GENERIC_NOTIFICATION        = 5,
    
    /* User trigger */
    IWL_FW_TRIGGER_ID_USER_TRIGGER                = 6,
    
    /* periodic uses the data field for the interval time */
    IWL_FW_TRIGGER_ID_PERIODIC_TRIGGER            = 7,
    
    /* Host triggers */
    IWL_FW_TRIGGER_ID_HOST_PEER_CLIENT_INACTIVITY        = 8,
    IWL_FW_TRIGGER_ID_HOST_TX_LATENCY_THRESHOLD_CROSSED    = 9,
    IWL_FW_TRIGGER_ID_HOST_TX_RESPONSE_STATUS_FAILED    = 10,
    IWL_FW_TRIGGER_ID_HOST_OS_REQ_DEAUTH_PEER        = 11,
    IWL_FW_TRIGGER_ID_HOST_STOP_GO_REQUEST            = 12,
    IWL_FW_TRIGGER_ID_HOST_START_GO_REQUEST            = 13,
    IWL_FW_TRIGGER_ID_HOST_JOIN_GROUP_REQUEST        = 14,
    IWL_FW_TRIGGER_ID_HOST_SCAN_START            = 15,
    IWL_FW_TRIGGER_ID_HOST_SCAN_SUBMITTED            = 16,
    IWL_FW_TRIGGER_ID_HOST_SCAN_PARAMS            = 17,
    IWL_FW_TRIGGER_ID_HOST_CHECK_FOR_HANG            = 18,
    IWL_FW_TRIGGER_ID_HOST_BAR_RECEIVED            = 19,
    IWL_FW_TRIGGER_ID_HOST_AGG_TX_RESPONSE_STATUS_FAILED    = 20,
    IWL_FW_TRIGGER_ID_HOST_EAPOL_TX_RESPONSE_FAILED        = 21,
    IWL_FW_TRIGGER_ID_HOST_FAKE_TX_RESPONSE_SUSPECTED    = 22,
    IWL_FW_TRIGGER_ID_HOST_AUTH_REQ_FROM_ASSOC_CLIENT    = 23,
    IWL_FW_TRIGGER_ID_HOST_ROAM_COMPLETE            = 24,
    IWL_FW_TRIGGER_ID_HOST_AUTH_ASSOC_FAST_FAILED        = 25,
    IWL_FW_TRIGGER_ID_HOST_D3_START                = 26,
    IWL_FW_TRIGGER_ID_HOST_D3_END                = 27,
    IWL_FW_TRIGGER_ID_HOST_BSS_MISSED_BEACONS        = 28,
    IWL_FW_TRIGGER_ID_HOST_P2P_CLIENT_MISSED_BEACONS    = 29,
    IWL_FW_TRIGGER_ID_HOST_PEER_CLIENT_TX_FAILURES        = 30,
    IWL_FW_TRIGGER_ID_HOST_TX_WFD_ACTION_FRAME_FAILED    = 31,
    IWL_FW_TRIGGER_ID_HOST_AUTH_ASSOC_FAILED        = 32,
    IWL_FW_TRIGGER_ID_HOST_SCAN_COMPLETE            = 33,
    IWL_FW_TRIGGER_ID_HOST_SCAN_ABORT            = 34,
    IWL_FW_TRIGGER_ID_HOST_NIC_ALIVE            = 35,
    IWL_FW_TRIGGER_ID_HOST_CHANNEL_SWITCH_COMPLETE        = 36,
    
    IWL_FW_TRIGGER_ID_NUM,
}; /* FW_DEBUG_TLV_TRIGGER_ID_E_VER_1 */

/**
 * enum iwl_fw_ini_apply_point
 *
 * @IWL_FW_INI_APPLY_INVALID: invalid
 * @IWL_FW_INI_APPLY_EARLY: pre loading FW
 * @IWL_FW_INI_APPLY_AFTER_ALIVE: first cmd from host after alive
 * @IWL_FW_INI_APPLY_POST_INIT: last cmd in initialization sequence
 * @IWL_FW_INI_APPLY_MISSED_BEACONS: missed beacons notification
 * @IWL_FW_INI_APPLY_SCAN_COMPLETE: scan completed
 * @IWL_FW_INI_APPLY_NUM: number of apply points
 */
enum iwl_fw_ini_apply_point {
    IWL_FW_INI_APPLY_INVALID,
    IWL_FW_INI_APPLY_EARLY,
    IWL_FW_INI_APPLY_AFTER_ALIVE,
    IWL_FW_INI_APPLY_POST_INIT,
    IWL_FW_INI_APPLY_MISSED_BEACONS,
    IWL_FW_INI_APPLY_SCAN_COMPLETE,
    IWL_FW_INI_APPLY_NUM,
}; /* FW_DEBUG_TLV_APPLY_POINT_E_VER_1 */

/**
 * enum iwl_fw_ini_allocation_id
 *
 * @IWL_FW_INI_ALLOCATION_INVALID: invalid
 * @IWL_FW_INI_ALLOCATION_ID_DBGC1: allocation meant for DBGC1 configuration
 * @IWL_FW_INI_ALLOCATION_ID_DBGC2: allocation meant for DBGC2 configuration
 * @IWL_FW_INI_ALLOCATION_ID_DBGC3: allocation meant for DBGC3 configuration
 * @IWL_FW_INI_ALLOCATION_ID_SDFX: for SDFX module
 * @IWL_FW_INI_ALLOCATION_ID_FW_DUMP: used for crash and runtime dumps
 * @IWL_FW_INI_ALLOCATION_ID_USER_DEFINED: for future user scenarios
 */
enum iwl_fw_ini_allocation_id {
    IWL_FW_INI_ALLOCATION_INVALID,
    IWL_FW_INI_ALLOCATION_ID_DBGC1,
    IWL_FW_INI_ALLOCATION_ID_DBGC2,
    IWL_FW_INI_ALLOCATION_ID_DBGC3,
    IWL_FW_INI_ALLOCATION_ID_SDFX,
    IWL_FW_INI_ALLOCATION_ID_FW_DUMP,
    IWL_FW_INI_ALLOCATION_ID_USER_DEFINED,
}; /* FW_DEBUG_TLV_ALLOCATION_ID_E_VER_1 */

/**
 * enum iwl_fw_ini_buffer_location
 *
 * @IWL_FW_INI_LOCATION_INVALID: invalid
 * @IWL_FW_INI_LOCATION_SRAM_PATH: SRAM location
 * @IWL_FW_INI_LOCATION_DRAM_PATH: DRAM location
 * @IWL_FW_INI_LOCATION_NPK_PATH: NPK location
 */
enum iwl_fw_ini_buffer_location {
    IWL_FW_INI_LOCATION_INVALID,
    IWL_FW_INI_LOCATION_SRAM_PATH,
    IWL_FW_INI_LOCATION_DRAM_PATH,
    IWL_FW_INI_LOCATION_NPK_PATH,
}; /* FW_DEBUG_TLV_BUFFER_LOCATION_E_VER_1 */

/**
 * enum iwl_fw_ini_debug_flow
 *
 * @IWL_FW_INI_DEBUG_INVALID: invalid
 * @IWL_FW_INI_DEBUG_DBTR_FLOW: undefined
 * @IWL_FW_INI_DEBUG_TB2DTF_FLOW: undefined
 */
enum iwl_fw_ini_debug_flow {
    IWL_FW_INI_DEBUG_INVALID,
    IWL_FW_INI_DEBUG_DBTR_FLOW,
    IWL_FW_INI_DEBUG_TB2DTF_FLOW,
}; /* FW_DEBUG_TLV_FLOW_E_VER_1 */

/**
 * enum iwl_fw_ini_region_type
 *
 * @IWL_FW_INI_REGION_INVALID: invalid
 * @IWL_FW_INI_REGION_DEVICE_MEMORY: device internal memory
 * @IWL_FW_INI_REGION_PERIPHERY_MAC: periphery registers of MAC
 * @IWL_FW_INI_REGION_PERIPHERY_PHY: periphery registers of PHY
 * @IWL_FW_INI_REGION_PERIPHERY_AUX: periphery registers of AUX
 * @IWL_FW_INI_REGION_DRAM_BUFFER: DRAM buffer
 * @IWL_FW_INI_REGION_DRAM_IMR: IMR memory
 * @IWL_FW_INI_REGION_INTERNAL_BUFFER: undefined
 * @IWL_FW_INI_REGION_TXF: TX fifos
 * @IWL_FW_INI_REGION_RXF: RX fifo
 * @IWL_FW_INI_REGION_PAGING: paging memory
 * @IWL_FW_INI_REGION_CSR: CSR registers
 * @IWL_FW_INI_REGION_NOTIFICATION: FW notification data
 * @IWL_FW_INI_REGION_DHC: dhc response to dump
 * @IWL_FW_INI_REGION_LMAC_ERROR_TABLE: lmac error table
 * @IWL_FW_INI_REGION_UMAC_ERROR_TABLE: umac error table
 * @IWL_FW_INI_REGION_NUM: number of region types
 */
enum iwl_fw_ini_region_type {
    IWL_FW_INI_REGION_INVALID,
    IWL_FW_INI_REGION_DEVICE_MEMORY,
    IWL_FW_INI_REGION_PERIPHERY_MAC,
    IWL_FW_INI_REGION_PERIPHERY_PHY,
    IWL_FW_INI_REGION_PERIPHERY_AUX,
    IWL_FW_INI_REGION_DRAM_BUFFER,
    IWL_FW_INI_REGION_DRAM_IMR,
    IWL_FW_INI_REGION_INTERNAL_BUFFER,
    IWL_FW_INI_REGION_TXF,
    IWL_FW_INI_REGION_RXF,
    IWL_FW_INI_REGION_PAGING,
    IWL_FW_INI_REGION_CSR,
    IWL_FW_INI_REGION_NOTIFICATION,
    IWL_FW_INI_REGION_DHC,
    IWL_FW_INI_REGION_LMAC_ERROR_TABLE,
    IWL_FW_INI_REGION_UMAC_ERROR_TABLE,
    IWL_FW_INI_REGION_NUM
}; /* FW_DEBUG_TLV_REGION_TYPE_E_VER_1 */

#endif /* dbg_tlv_h */
