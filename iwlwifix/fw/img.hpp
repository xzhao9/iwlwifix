//
//  img.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef img_h
#define img_h

#include <libkern/OSTypes.h>
#include <sys/types.h>
#include "../linux/ethtool.hpp"
#include "api/dbg-tlv.hpp"
#include "img.hpp"
#include "error-dump.hpp"
#include "file.hpp"

// @IWL_UCODE_REGULAR: Normal runtime ucode
// @IWL_UCODE_INIT: Initial ucode
// @IWL_UCODE_WOWLAN: Wake on Wireless enabled ucode
// @IWL_UCODE_REGULAR_USNIFFER: Normal runtime ucode with usniffer image
enum iwl_ucode_type {
    IWL_UCODE_REGULAR,
    IWL_UCODE_INIT,
    IWL_UCODE_WOWLAN,
    IWL_UCODE_REGULAR_USNIFFER,
    IWL_UCODE_TYPE_MAX,
};

// enumeration of ucode section
// This enumeration is used directly for older firmware (before 16.0)
// For new firmware, there can be up to 4 sections but the first one
// packaged into the firmware file is the DATA section and some debugging
// code accesses that.
enum iwl_ucode_cec {
    IWL_UCODE_SECTION_DATA,
    IWL_UCODE_SECTION_INST,
};

struct iwl_ucode_capabilities {
    UInt32 max_probe_length;
    UInt32 n_scan_channels;
    UInt32 standard_phy_calibration_size;
    UInt32 flags;
    UInt32 error_log_addr;
    UInt32 error_log_size;
    unsigned long _api[BITS_TO_LONGS(NUM_IWL_UCODE_TLV_API)];
    unsigned long _capa[BITS_TO_LONGS(NUM_IWL_UCODE_TLV_CAPA)];
    
    const struct iwl_fw_cmd_version *cmd_versions;
    UInt32 n_cmd_versions;
};

static inline bool
fw_has_api(const struct iwl_ucode_capabilities *capabilities,
           iwl_ucode_tlv_api_t api) {
    return test_bit((long)api, capabilities->_api);
}

static inline bool
fw_has_capa(const struct iwl_ucode_capabilities *capabilities,
            iwl_ucode_tlv_capa_t capa) {
    return test_bit((long)capa, capabilities->_capa);
}

struct fw_desc {
    const void *data;    /* vmalloc'ed data */
    UInt32 len;        /* size in bytes */
    UInt32 offset;        /* offset in the device */
};

struct fw_img {
    struct fw_desc *sec;
    int num_sec;
    bool is_dual_cpus;
    UInt32 paging_mem_size;
};

/*
 * Block paging calculations
 */
#define PAGE_2_EXP_SIZE 12 /* 4K == 2^12 */
#define FW_PAGING_SIZE BIT(PAGE_2_EXP_SIZE) /* page size is 4KB */
#define PAGE_PER_GROUP_2_EXP_SIZE 3
/* 8 pages per group */
#define NUM_OF_PAGE_PER_GROUP BIT(PAGE_PER_GROUP_2_EXP_SIZE)
/* don't change, support only 32KB size */
#define PAGING_BLOCK_SIZE (NUM_OF_PAGE_PER_GROUP * FW_PAGING_SIZE)
/* 32K == 2^15 */
#define BLOCK_2_EXP_SIZE (PAGE_2_EXP_SIZE + PAGE_PER_GROUP_2_EXP_SIZE)

/*
 * Image paging calculations
 */
#define BLOCK_PER_IMAGE_2_EXP_SIZE 5
/* 2^5 == 32 blocks per image */
#define NUM_OF_BLOCK_PER_IMAGE BIT(BLOCK_PER_IMAGE_2_EXP_SIZE)
/* maximum image size 1024KB */
#define MAX_PAGING_IMAGE_SIZE (NUM_OF_BLOCK_PER_IMAGE * PAGING_BLOCK_SIZE)

/* Virtual address signature */
#define PAGING_ADDR_SIG 0xAA000000

#define PAGING_CMD_IS_SECURED BIT(9)
#define PAGING_CMD_IS_ENABLED BIT(8)
#define PAGING_CMD_NUM_OF_PAGES_IN_LAST_GRP_POS    0
#define PAGING_TLV_SECURE_MASK 1

// TODO: iwl_fw_paging
// @fw_paging_phys: page phy pointer
// @fw_paging_block: pointer to the allocated block
// @fw_paging_size: page size
struct iwl_fw_paging {
    UInt64 fw_paging_phys;
    UInt64 fw_paging_block;
    UInt32 fw_paging_size;
};

struct __attribute__((packed)) iwl_fw_cscheme_list {
    UInt8 size;
    struct iwl_fw_cipher_scheme cs[];
};

enum iwl_fw_type {
    IWL_FW_DVM,
    IWL_FW_MVM,
};

struct iwl_fw_dbg {
    struct iwl_fw_dbg_dest_tlv_v1 *dest_tlv;
    UInt8 n_dext_reg;
    struct iwl_fw_dbg_conf_tlv *conf_tlv[FW_DBG_CONF_MAX];
    struct iwl_fw_dbg_trigger_tlv *trigger_tlv[FW_DBG_TRIGGER_MAX];
    size_t trigger_tlv_len[FW_DBG_TRIGGER_MAX];
    struct iwl_fw_dbg_mem_seg_tlv *mem_tlv;
    size_t n_mem_tlv;
    UInt32 dump_mask;
};

struct __attribute__((packed)) iwl_fw_ini_allocation_data {
    struct iwl_fw_ini_allocation_tlv tlv;
    UInt32 is_alloc;
};

struct iwl_fw_ini_active_triggers {
    bool active;
    size_t size;
    struct iwl_fw_ini_trigger *trig;
};

struct iwl_fw {
    UInt32 ucode_ver;
    char fw_version[ETHTOOL_FWVERS_LEN];
    
    // ucode images
    struct fw_img img[IWL_UCODE_TYPE_MAX];
    size_t iml_len;
    UInt8 *iml;
    
    struct iwl_ucode_capabilities ucode_capa;
    bool   enhance_sensitivity_table;
    
    UInt32 init_evtlog_ptr, init_evtlog_size, init_errlog_ptr;
    UInt32 inst_evtlog_ptr, inst_evtlog_size, inst_errlog_ptr;
    
    struct iwl_tlv_calib_ctrl default_calib[IWL_UCODE_TYPE_MAX];
    UInt32 phy_config;
    UInt8 valid_tx_ant;
    UInt8 valid_rx_ant;
    
    enum iwl_fw_type type;
    
    struct iwl_fw_cipher_scheme cs[IWL_UCODE_MAX_CS];
    UInt8 human_readable[FW_VER_HUMAN_READABLE_SZ];
    
    // Disable firmware debug tool
    // struct iwl_fw_dbg dbg;
};

static inline const char *get_fw_dbg_mode_string(int mode) {
    switch (mode) {
        case SMEM_MODE:
            return "SMEM";
        case EXTERNAL_MODE:
            return "EXTERNAL_DRAM";
        case MARBH_MODE:
            return "MARBH";
        case MIPI_MODE:
            return "MIPI";
        default:
            return "UNKNOWN";
    }
}

//static inline bool
//iwl_fw_dbg_conf_usniffer(const struct iwl_fw *fw, UInt8 id)
//{
//    const struct iwl_fw_dbg_conf_tlv *conf_tlv = fw->dbg.conf_tlv[id];
//    
//    if (!conf_tlv)
//        return false;
//    
//    return conf_tlv->usniffer;
//}

static inline const struct fw_img *
iwl_get_ucode_image(const struct iwl_fw *fw, enum iwl_ucode_type ucode_type)
{
    if (ucode_type >= IWL_UCODE_TYPE_MAX)
        // 0UL == NULL
        return 0UL;
    
    return &fw->img[ucode_type];
}

#endif /* img_h */
