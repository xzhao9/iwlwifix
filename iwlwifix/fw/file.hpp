//
//  file.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef file_h
#define file_h

#include "../linux/types.hpp"
#include "../linux/nl80211.hpp"
#include "../net80211/if_ether.hpp"

struct iwl_ucode_header {
    __le32 ver;
    union {
        struct {
            __le32 inst_size;       // bytes of runtime code
            __le32 data_size;       // bytes of runtime data
            __le32 init_size;       // bytes of init code
            __le32 init_data_size;  // bytes of init data
            __le32 boot_size;       // bytes of bootstrap code
            UInt8  data[0];         // In the same order as sizes
        } v1;
        struct {
            __le32 build;
            __le32 inst_size;
            __le32 data_size;
            __le32 init_size;
            __le32 init_data_size;
            __le32 boot_size;
            UInt8  data[0];         // In the same order as sizes
        } v2;
    } u;
};

#define IWL_UCODE_INI_TLV_GROUP    0x1000000

// The new TLV uCode layout
enum iwl_ucode_tlv_type {
    IWL_UCODE_TLV_INVALID        = 0, /* unused */
    IWL_UCODE_TLV_INST        = 1,
    IWL_UCODE_TLV_DATA        = 2,
    IWL_UCODE_TLV_INIT        = 3,
    IWL_UCODE_TLV_INIT_DATA        = 4,
    IWL_UCODE_TLV_BOOT        = 5,
    IWL_UCODE_TLV_PROBE_MAX_LEN    = 6, /* a u32 value */
    IWL_UCODE_TLV_PAN        = 7,
    IWL_UCODE_TLV_RUNT_EVTLOG_PTR    = 8,
    IWL_UCODE_TLV_RUNT_EVTLOG_SIZE    = 9,
    IWL_UCODE_TLV_RUNT_ERRLOG_PTR    = 10,
    IWL_UCODE_TLV_INIT_EVTLOG_PTR    = 11,
    IWL_UCODE_TLV_INIT_EVTLOG_SIZE    = 12,
    IWL_UCODE_TLV_INIT_ERRLOG_PTR    = 13,
    IWL_UCODE_TLV_ENHANCE_SENS_TBL    = 14,
    IWL_UCODE_TLV_PHY_CALIBRATION_SIZE = 15,
    IWL_UCODE_TLV_WOWLAN_INST    = 16,
    IWL_UCODE_TLV_WOWLAN_DATA    = 17,
    IWL_UCODE_TLV_FLAGS        = 18,
    IWL_UCODE_TLV_SEC_RT        = 19,
    IWL_UCODE_TLV_SEC_INIT        = 20,
    IWL_UCODE_TLV_SEC_WOWLAN    = 21,
    IWL_UCODE_TLV_DEF_CALIB        = 22,
    IWL_UCODE_TLV_PHY_SKU        = 23,
    IWL_UCODE_TLV_SECURE_SEC_RT    = 24,
    IWL_UCODE_TLV_SECURE_SEC_INIT    = 25,
    IWL_UCODE_TLV_SECURE_SEC_WOWLAN    = 26,
    IWL_UCODE_TLV_NUM_OF_CPU    = 27,
    IWL_UCODE_TLV_CSCHEME        = 28,
    IWL_UCODE_TLV_API_CHANGES_SET    = 29,
    IWL_UCODE_TLV_ENABLED_CAPABILITIES    = 30,
    IWL_UCODE_TLV_N_SCAN_CHANNELS        = 31,
    IWL_UCODE_TLV_PAGING        = 32,
    IWL_UCODE_TLV_SEC_RT_USNIFFER    = 34,
    /* 35 is unused */
    IWL_UCODE_TLV_FW_VERSION    = 36,
    IWL_UCODE_TLV_FW_DBG_DEST    = 38,
    IWL_UCODE_TLV_FW_DBG_CONF    = 39,
    IWL_UCODE_TLV_FW_DBG_TRIGGER    = 40,
    IWL_UCODE_TLV_CMD_VERSIONS    = 48,
    IWL_UCODE_TLV_FW_GSCAN_CAPA    = 50,
    IWL_UCODE_TLV_FW_MEM_SEG    = 51,
    IWL_UCODE_TLV_IML        = 52,
    IWL_UCODE_TLV_UMAC_DEBUG_ADDRS    = 54,
    IWL_UCODE_TLV_LMAC_DEBUG_ADDRS    = 55,
    IWL_UCODE_TLV_FW_RECOVERY_INFO    = 57,
    IWL_UCODE_TLV_FW_FSEQ_VERSION    = 60,
    
    IWL_UCODE_TLV_TYPE_BUFFER_ALLOCATION    = IWL_UCODE_INI_TLV_GROUP + 0x1,
    IWL_UCODE_TLV_DEBUG_BASE = IWL_UCODE_TLV_TYPE_BUFFER_ALLOCATION,
    IWL_UCODE_TLV_TYPE_HCMD            = IWL_UCODE_INI_TLV_GROUP + 0x2,
    IWL_UCODE_TLV_TYPE_REGIONS        = IWL_UCODE_INI_TLV_GROUP + 0x3,
    IWL_UCODE_TLV_TYPE_TRIGGERS        = IWL_UCODE_INI_TLV_GROUP + 0x4,
    IWL_UCODE_TLV_TYPE_DEBUG_FLOW        = IWL_UCODE_INI_TLV_GROUP + 0x5,
    IWL_UCODE_TLV_DEBUG_MAX = IWL_UCODE_TLV_TYPE_DEBUG_FLOW,
    
    /* TLVs 0x1000-0x2000 are for internal driver usage */
    IWL_UCODE_TLV_FW_DBG_DUMP_LST    = 0x1000,
};

struct iwl_ucode_tlv {
    __le32 type;        /* see above */
    __le32 length;        /* not including type/length fields */
    UInt8 data[0];
};

#define IWL_TLV_UCODE_MAGIC        0x0a4c5749
#define FW_VER_HUMAN_READABLE_SZ    64

struct iwl_tlv_ucode_header {
    /*
     * The TLV style ucode header is distinguished from
     * the v1/v2 style header by first four bytes being
     * zero, as such is an invalid combination of
     * major/minor/API/serial versions.
     */
    __le32 zero;
    __le32 magic;
    UInt8 human_readable[FW_VER_HUMAN_READABLE_SZ];
    /* major/minor/API/serial or major in new format */
    __le32 ver;
    __le32 build;
    __le64 ignore;
    /*
     * The data contained herein has a TLV layout,
     * see above for the TLV header and types.
     * Note that each TLV is padded to a length
     * that is a multiple of 4 for alignment.
     */
    UInt8 data[0];
};

struct __attribute__((packed)) iwl_ucode_api {
    __le32 api_index;
    __le32 api_flags;
};

struct __attribute__((packed)) iwl_ucode_capa {
    __le32 api_index;
    __le32 api_capa;
};

enum iwl_ucode_tlv_flag {
    IWL_UCODE_TLV_FLAGS_PAN            = BIT(0),
    IWL_UCODE_TLV_FLAGS_NEWSCAN        = BIT(1),
    IWL_UCODE_TLV_FLAGS_MFP            = BIT(2),
    IWL_UCODE_TLV_FLAGS_SHORT_BL        = BIT(7),
    IWL_UCODE_TLV_FLAGS_D3_6_IPV6_ADDRS    = BIT(10),
    IWL_UCODE_TLV_FLAGS_NO_BASIC_SSID    = BIT(12),
    IWL_UCODE_TLV_FLAGS_NEW_NSOFFL_SMALL    = BIT(15),
    IWL_UCODE_TLV_FLAGS_NEW_NSOFFL_LARGE    = BIT(16),
    IWL_UCODE_TLV_FLAGS_UAPSD_SUPPORT    = BIT(24),
    IWL_UCODE_TLV_FLAGS_EBS_SUPPORT        = BIT(25),
    IWL_UCODE_TLV_FLAGS_P2P_PS_UAPSD    = BIT(26),
    IWL_UCODE_TLV_FLAGS_BCAST_FILTERING    = BIT(29),
};

typedef UInt32 iwl_ucode_tlv_api_t;

enum iwl_ucode_tlv_api {
    /* API Set 0 */
    IWL_UCODE_TLV_API_FRAGMENTED_SCAN    = (iwl_ucode_tlv_api_t)8,
    IWL_UCODE_TLV_API_WIFI_MCC_UPDATE    = (iwl_ucode_tlv_api_t)9,
    IWL_UCODE_TLV_API_LQ_SS_PARAMS        = (iwl_ucode_tlv_api_t)18,
    IWL_UCODE_TLV_API_NEW_VERSION        = (iwl_ucode_tlv_api_t)20,
    IWL_UCODE_TLV_API_SCAN_TSF_REPORT    = (iwl_ucode_tlv_api_t)28,
    IWL_UCODE_TLV_API_TKIP_MIC_KEYS        = (iwl_ucode_tlv_api_t)29,
    IWL_UCODE_TLV_API_STA_TYPE        = (iwl_ucode_tlv_api_t)30,
    IWL_UCODE_TLV_API_NAN2_VER2        = (iwl_ucode_tlv_api_t)31,
    /* API Set 1 */
    IWL_UCODE_TLV_API_ADAPTIVE_DWELL    = (iwl_ucode_tlv_api_t)32,
    IWL_UCODE_TLV_API_OCE            = (iwl_ucode_tlv_api_t)33,
    IWL_UCODE_TLV_API_NEW_BEACON_TEMPLATE    = (iwl_ucode_tlv_api_t)34,
    IWL_UCODE_TLV_API_NEW_RX_STATS        = (iwl_ucode_tlv_api_t)35,
    IWL_UCODE_TLV_API_WOWLAN_KEY_MATERIAL    = (iwl_ucode_tlv_api_t)36,
    IWL_UCODE_TLV_API_QUOTA_LOW_LATENCY    = (iwl_ucode_tlv_api_t)38,
    IWL_UCODE_TLV_API_DEPRECATE_TTAK    = (iwl_ucode_tlv_api_t)41,
    IWL_UCODE_TLV_API_ADAPTIVE_DWELL_V2    = (iwl_ucode_tlv_api_t)42,
    IWL_UCODE_TLV_API_FRAG_EBS        = (iwl_ucode_tlv_api_t)44,
    IWL_UCODE_TLV_API_REDUCE_TX_POWER    = (iwl_ucode_tlv_api_t)45,
    IWL_UCODE_TLV_API_SHORT_BEACON_NOTIF    = (iwl_ucode_tlv_api_t)46,
    IWL_UCODE_TLV_API_BEACON_FILTER_V4      = (iwl_ucode_tlv_api_t)47,
    IWL_UCODE_TLV_API_REGULATORY_NVM_INFO   = (iwl_ucode_tlv_api_t)48,
    IWL_UCODE_TLV_API_FTM_NEW_RANGE_REQ     = (iwl_ucode_tlv_api_t)49,
    IWL_UCODE_TLV_API_SCAN_OFFLOAD_CHANS    = (iwl_ucode_tlv_api_t)50,
    IWL_UCODE_TLV_API_MBSSID_HE        = (iwl_ucode_tlv_api_t)52,
    IWL_UCODE_TLV_API_WOWLAN_TCP_SYN_WAKE    = (iwl_ucode_tlv_api_t)53,
    IWL_UCODE_TLV_API_FTM_RTT_ACCURACY      = (iwl_ucode_tlv_api_t)54,
    NUM_IWL_UCODE_TLV_API
};

typedef UInt32 iwl_ucode_tlv_capa_t;

enum iwl_ucode_tlv_capa {
    /* set 0 */
    IWL_UCODE_TLV_CAPA_D0I3_SUPPORT            = ( iwl_ucode_tlv_capa_t)0,
    IWL_UCODE_TLV_CAPA_LAR_SUPPORT            = ( iwl_ucode_tlv_capa_t)1,
    IWL_UCODE_TLV_CAPA_UMAC_SCAN            = ( iwl_ucode_tlv_capa_t)2,
    IWL_UCODE_TLV_CAPA_BEAMFORMER            = ( iwl_ucode_tlv_capa_t)3,
    IWL_UCODE_TLV_CAPA_TDLS_SUPPORT            = ( iwl_ucode_tlv_capa_t)6,
    IWL_UCODE_TLV_CAPA_TXPOWER_INSERTION_SUPPORT    = ( iwl_ucode_tlv_capa_t)8,
    IWL_UCODE_TLV_CAPA_DS_PARAM_SET_IE_SUPPORT    = ( iwl_ucode_tlv_capa_t)9,
    IWL_UCODE_TLV_CAPA_WFA_TPC_REP_IE_SUPPORT    = ( iwl_ucode_tlv_capa_t)10,
    IWL_UCODE_TLV_CAPA_QUIET_PERIOD_SUPPORT        = ( iwl_ucode_tlv_capa_t)11,
    IWL_UCODE_TLV_CAPA_DQA_SUPPORT            = ( iwl_ucode_tlv_capa_t)12,
    IWL_UCODE_TLV_CAPA_TDLS_CHANNEL_SWITCH        = ( iwl_ucode_tlv_capa_t)13,
    IWL_UCODE_TLV_CAPA_CNSLDTD_D3_D0_IMG        = ( iwl_ucode_tlv_capa_t)17,
    IWL_UCODE_TLV_CAPA_HOTSPOT_SUPPORT        = ( iwl_ucode_tlv_capa_t)18,
    IWL_UCODE_TLV_CAPA_DC2DC_CONFIG_SUPPORT        = ( iwl_ucode_tlv_capa_t)19,
    IWL_UCODE_TLV_CAPA_CSUM_SUPPORT            = ( iwl_ucode_tlv_capa_t)21,
    IWL_UCODE_TLV_CAPA_RADIO_BEACON_STATS        = ( iwl_ucode_tlv_capa_t)22,
    IWL_UCODE_TLV_CAPA_P2P_SCM_UAPSD        = ( iwl_ucode_tlv_capa_t)26,
    IWL_UCODE_TLV_CAPA_BT_COEX_PLCR            = ( iwl_ucode_tlv_capa_t)28,
    IWL_UCODE_TLV_CAPA_LAR_MULTI_MCC        = ( iwl_ucode_tlv_capa_t)29,
    IWL_UCODE_TLV_CAPA_BT_COEX_RRC            = ( iwl_ucode_tlv_capa_t)30,
    IWL_UCODE_TLV_CAPA_GSCAN_SUPPORT        = ( iwl_ucode_tlv_capa_t)31,
    
    /* set 1 */
    IWL_UCODE_TLV_CAPA_STA_PM_NOTIF            = ( iwl_ucode_tlv_capa_t)38,
    IWL_UCODE_TLV_CAPA_BINDING_CDB_SUPPORT        = ( iwl_ucode_tlv_capa_t)39,
    IWL_UCODE_TLV_CAPA_CDB_SUPPORT            = ( iwl_ucode_tlv_capa_t)40,
    IWL_UCODE_TLV_CAPA_D0I3_END_FIRST        = ( iwl_ucode_tlv_capa_t)41,
    IWL_UCODE_TLV_CAPA_TLC_OFFLOAD                  = ( iwl_ucode_tlv_capa_t)43,
    IWL_UCODE_TLV_CAPA_DYNAMIC_QUOTA                = ( iwl_ucode_tlv_capa_t)44,
    IWL_UCODE_TLV_CAPA_COEX_SCHEMA_2        = ( iwl_ucode_tlv_capa_t)45,
    IWL_UCODE_TLV_CAPA_CHANNEL_SWITCH_CMD        = ( iwl_ucode_tlv_capa_t)46,
    IWL_UCODE_TLV_CAPA_ULTRA_HB_CHANNELS        = ( iwl_ucode_tlv_capa_t)48,
    IWL_UCODE_TLV_CAPA_FTM_CALIBRATED        = ( iwl_ucode_tlv_capa_t)47,
    IWL_UCODE_TLV_CAPA_CS_MODIFY            = ( iwl_ucode_tlv_capa_t)49,
    
    /* set 2 */
    IWL_UCODE_TLV_CAPA_EXTENDED_DTS_MEASURE        = ( iwl_ucode_tlv_capa_t)64,
    IWL_UCODE_TLV_CAPA_SHORT_PM_TIMEOUTS        = ( iwl_ucode_tlv_capa_t)65,
    IWL_UCODE_TLV_CAPA_BT_MPLUT_SUPPORT        = ( iwl_ucode_tlv_capa_t)67,
    IWL_UCODE_TLV_CAPA_MULTI_QUEUE_RX_SUPPORT    = ( iwl_ucode_tlv_capa_t)68,
    IWL_UCODE_TLV_CAPA_CSA_AND_TBTT_OFFLOAD        = ( iwl_ucode_tlv_capa_t)70,
    IWL_UCODE_TLV_CAPA_BEACON_ANT_SELECTION        = ( iwl_ucode_tlv_capa_t)71,
    IWL_UCODE_TLV_CAPA_BEACON_STORING        = ( iwl_ucode_tlv_capa_t)72,
    IWL_UCODE_TLV_CAPA_LAR_SUPPORT_V3        = ( iwl_ucode_tlv_capa_t)73,
    IWL_UCODE_TLV_CAPA_CT_KILL_BY_FW        = ( iwl_ucode_tlv_capa_t)74,
    IWL_UCODE_TLV_CAPA_TEMP_THS_REPORT_SUPPORT    = ( iwl_ucode_tlv_capa_t)75,
    IWL_UCODE_TLV_CAPA_CTDP_SUPPORT            = ( iwl_ucode_tlv_capa_t)76,
    IWL_UCODE_TLV_CAPA_USNIFFER_UNIFIED        = ( iwl_ucode_tlv_capa_t)77,
    IWL_UCODE_TLV_CAPA_EXTEND_SHARED_MEM_CFG    = ( iwl_ucode_tlv_capa_t)80,
    IWL_UCODE_TLV_CAPA_LQM_SUPPORT            = ( iwl_ucode_tlv_capa_t)81,
    IWL_UCODE_TLV_CAPA_TX_POWER_ACK            = ( iwl_ucode_tlv_capa_t)84,
    IWL_UCODE_TLV_CAPA_D3_DEBUG            = ( iwl_ucode_tlv_capa_t)87,
    IWL_UCODE_TLV_CAPA_LED_CMD_SUPPORT        = ( iwl_ucode_tlv_capa_t)88,
    IWL_UCODE_TLV_CAPA_MCC_UPDATE_11AX_SUPPORT    = ( iwl_ucode_tlv_capa_t)89,
    IWL_UCODE_TLV_CAPA_CSI_REPORTING        = ( iwl_ucode_tlv_capa_t)90,
    
    /* set 3 */
    IWL_UCODE_TLV_CAPA_MLME_OFFLOAD            = ( iwl_ucode_tlv_capa_t)96,
    
    NUM_IWL_UCODE_TLV_CAPA
};

/* The default calibrate table size if not specified by firmware file */
#define IWL_DEFAULT_STANDARD_PHY_CALIBRATE_TBL_SIZE    18
#define IWL_MAX_STANDARD_PHY_CALIBRATE_TBL_SIZE        19
#define IWL_MAX_PHY_CALIBRATE_TBL_SIZE            253

/* The default max probe length if not specified by the firmware file */
#define IWL_DEFAULT_MAX_PROBE_LENGTH    200

/*
 * For 16.0 uCode and above, there is no differentiation between sections,
 * just an offset to the HW address.
 */
#define CPU1_CPU2_SEPARATOR_SECTION    0xFFFFCCCC
#define PAGING_SEPARATOR_SECTION    0xAAAABBBB

/* uCode version contains 4 values: Major/Minor/API/Serial */
#define IWL_UCODE_MAJOR(ver)    (((ver) & 0xFF000000) >> 24)
#define IWL_UCODE_MINOR(ver)    (((ver) & 0x00FF0000) >> 16)
#define IWL_UCODE_API(ver)    (((ver) & 0x0000FF00) >> 8)
#define IWL_UCODE_SERIAL(ver)    ((ver) & 0x000000FF)

struct __attribute__((packed)) iwl_tlv_calib_ctrl {
    __le32 flow_trigger;
    __le32 event_trigger;
};

enum iwl_fw_phy_cfg {
    FW_PHY_CFG_RADIO_TYPE_POS = 0,
    FW_PHY_CFG_RADIO_TYPE = 0x3 << FW_PHY_CFG_RADIO_TYPE_POS,
    FW_PHY_CFG_RADIO_STEP_POS = 2,
    FW_PHY_CFG_RADIO_STEP = 0x3 << FW_PHY_CFG_RADIO_STEP_POS,
    FW_PHY_CFG_RADIO_DASH_POS = 4,
    FW_PHY_CFG_RADIO_DASH = 0x3 << FW_PHY_CFG_RADIO_DASH_POS,
    FW_PHY_CFG_TX_CHAIN_POS = 16,
    FW_PHY_CFG_TX_CHAIN = 0xf << FW_PHY_CFG_TX_CHAIN_POS,
    FW_PHY_CFG_RX_CHAIN_POS = 20,
    FW_PHY_CFG_RX_CHAIN = 0xf << FW_PHY_CFG_RX_CHAIN_POS,
    FW_PHY_CFG_SHARED_CLK = BIT(31),
};

#define IWL_UCODE_MAX_CS        1

struct __attribute__((packed)) iwl_fw_cipher_scheme {
    __le32 cipher;
    UInt8 flags;
    UInt8 hdr_len;
    UInt8 pn_len;
    UInt8 pn_off;
    UInt8 key_idx_off;
    UInt8 key_idx_mask;
    UInt8 key_idx_shift;
    UInt8 mic_len;
    UInt8 hw_cipher;
};

enum iwl_fw_dbg_reg_operator {
    CSR_ASSIGN,
    CSR_SETBIT,
    CSR_CLEARBIT,
    
    PRPH_ASSIGN,
    PRPH_SETBIT,
    PRPH_CLEARBIT,
    
    INDIRECT_ASSIGN,
    INDIRECT_SETBIT,
    INDIRECT_CLEARBIT,
    
    PRPH_BLOCKBIT,
};

struct __attribute__((packed)) iwl_fw_dbg_reg_op {
    UInt8 op;
    UInt8 reserved[3];
    __le32 addr;
    __le32 val;
};

enum iwl_fw_dbg_monitor_mode {
    SMEM_MODE = 0,
    EXTERNAL_MODE = 1,
    MARBH_MODE = 2,
    MIPI_MODE = 3,
};

struct __attribute__((packed)) iwl_fw_dbg_mem_seg_tlv {
    __le32 data_type;
    __le32 ofs;
    __le32 len;
} ;

struct __attribute__((packed)) iwl_fw_dbg_dest_tlv_v1 {
    UInt8 version;
    UInt8 monitor_mode;
    UInt8 size_power;
    UInt8 reserved;
    __le32 base_reg;
    __le32 end_reg;
    __le32 write_ptr_reg;
    __le32 wrap_count;
    UInt8 base_shift;
    UInt8 end_shift;
    struct iwl_fw_dbg_reg_op reg_ops[0];
} ;

/* Mask of the register for defining the LDBG MAC2SMEM buffer SMEM size */
#define IWL_LDBG_M2S_BUF_SIZE_MSK    0x0fff0000
/* Mask of the register for defining the LDBG MAC2SMEM SMEM base address */
#define IWL_LDBG_M2S_BUF_BA_MSK        0x00000fff
/* The smem buffer chunks are in units of 256 bits */
#define IWL_M2S_UNIT_SIZE            0x100

struct __attribute__((packed)) iwl_fw_dbg_dest_tlv {
    UInt8 version;
    UInt8 monitor_mode;
    UInt8 size_power;
    UInt8 reserved;
    __le32 cfg_reg;
    __le32 write_ptr_reg;
    __le32 wrap_count;
    UInt8 base_shift;
    UInt8 size_shift;
    struct iwl_fw_dbg_reg_op reg_ops[0];
} ;

struct __attribute__((packed)) iwl_fw_dbg_conf_hcmd {
    UInt8 id;
    UInt8 reserved;
    __le16 len;
    UInt8 data[0];
} ;

enum iwl_fw_dbg_trigger_mode {
    IWL_FW_DBG_TRIGGER_START = BIT(0),
    IWL_FW_DBG_TRIGGER_STOP = BIT(1),
    IWL_FW_DBG_TRIGGER_MONITOR_ONLY = BIT(2),
};

enum iwl_fw_dbg_trigger_flags {
    IWL_FW_DBG_FORCE_RESTART = BIT(0),
};

/**
 * enum iwl_fw_dbg_trigger_vif_type - define the VIF type for a trigger
 * @IWL_FW_DBG_CONF_VIF_ANY: any vif type
 * @IWL_FW_DBG_CONF_VIF_IBSS: IBSS mode
 * @IWL_FW_DBG_CONF_VIF_STATION: BSS mode
 * @IWL_FW_DBG_CONF_VIF_AP: AP mode
 * @IWL_FW_DBG_CONF_VIF_P2P_CLIENT: P2P Client mode
 * @IWL_FW_DBG_CONF_VIF_P2P_GO: P2P GO mode
 * @IWL_FW_DBG_CONF_VIF_P2P_DEVICE: P2P device
 */
enum iwl_fw_dbg_trigger_vif_type {
    IWL_FW_DBG_CONF_VIF_ANY = NL80211_IFTYPE_UNSPECIFIED,
    IWL_FW_DBG_CONF_VIF_IBSS = NL80211_IFTYPE_ADHOC,
    IWL_FW_DBG_CONF_VIF_STATION = NL80211_IFTYPE_STATION,
    IWL_FW_DBG_CONF_VIF_AP = NL80211_IFTYPE_AP,
    IWL_FW_DBG_CONF_VIF_P2P_CLIENT = NL80211_IFTYPE_P2P_CLIENT,
    IWL_FW_DBG_CONF_VIF_P2P_GO = NL80211_IFTYPE_P2P_GO,
    IWL_FW_DBG_CONF_VIF_P2P_DEVICE = NL80211_IFTYPE_P2P_DEVICE,
};

struct __attribute__((packed)) iwl_fw_dbg_trigger_tlv {
    __le32 id;
    __le32 vif_type;
    __le32 stop_conf_ids;
    __le32 stop_delay;
    UInt8 mode;
    UInt8 start_conf_id;
    __le16 occurrences;
    __le16 trig_dis_ms;
    UInt8 flags;
    UInt8 reserved[5];
    
    UInt8 data[0];
} ;

#define FW_DBG_START_FROM_ALIVE    0
#define FW_DBG_CONF_MAX        32
#define FW_DBG_INVALID        0xff

struct __attribute__((packed)) iwl_fw_dbg_trigger_missed_bcon {
    __le32 stop_consec_missed_bcon;
    __le32 stop_consec_missed_bcon_since_rx;
    __le32 reserved2[2];
    __le32 start_consec_missed_bcon;
    __le32 start_consec_missed_bcon_since_rx;
    __le32 reserved1[2];
} ;

struct __attribute__((packed)) iwl_fw_dbg_trigger_cmd {
    struct __attribute__((packed)) cmd {
        UInt8 cmd_id;
        UInt8 group_id;
    }  cmds[16];
} ;

struct __attribute__((packed)) iwl_fw_dbg_trigger_stats {
    __le32 stop_offset;
    __le32 stop_threshold;
    __le32 start_offset;
    __le32 start_threshold;
} ;

struct __attribute__((packed)) iwl_fw_dbg_trigger_low_rssi {
    __le32 rssi;
};

struct __attribute__((packed)) iwl_fw_dbg_trigger_mlme {
    UInt8 stop_auth_denied;
    UInt8 stop_auth_timeout;
    UInt8 stop_rx_deauth;
    UInt8 stop_tx_deauth;
    
    UInt8 stop_assoc_denied;
    UInt8 stop_assoc_timeout;
    UInt8 stop_connection_loss;
    UInt8 reserved;
    
    UInt8 start_auth_denied;
    UInt8 start_auth_timeout;
    UInt8 start_rx_deauth;
    UInt8 start_tx_deauth;
    
    UInt8 start_assoc_denied;
    UInt8 start_assoc_timeout;
    UInt8 start_connection_loss;
    UInt8 reserved2;
};

struct __attribute__((packed)) iwl_fw_dbg_trigger_txq_timer {
    __le32 command_queue;
    __le32 bss;
    __le32 softap;
    __le32 p2p_go;
    __le32 p2p_client;
    __le32 p2p_device;
    __le32 ibss;
    __le32 tdls;
    __le32 reserved[4];
};

struct __attribute__((packed)) iwl_fw_dbg_trigger_time_event {
    struct __attribute__((packed)) {
        __le32 id;
        __le32 action_bitmap;
        __le32 status_bitmap;
    } time_events[16];
};

struct __attribute__((packed)) iwl_fw_dbg_trigger_ba {
    __le16 rx_ba_start;
    __le16 rx_ba_stop;
    __le16 tx_ba_start;
    __le16 tx_ba_stop;
    __le16 rx_bar;
    __le16 tx_bar;
    __le16 frame_timeout;
};

struct __attribute__((packed)) iwl_fw_dbg_trigger_tdls {
    UInt8 action_bitmap;
    UInt8 peer_mode;
    UInt8 peer[ETH_ALEN];
    UInt8 reserved[4];
};

struct __attribute__((packed)) iwl_fw_dbg_trigger_tx_status {
    struct __attribute__((packed)) tx_status {
        UInt8 status;
        UInt8 reserved[3];
    } statuses[16];
    __le32 reserved[2];
};

struct __attribute__((packed)) iwl_fw_dbg_conf_tlv {
    UInt8 id;
    UInt8 usniffer;
    UInt8 reserved;
    UInt8 num_of_hcmds;
    struct iwl_fw_dbg_conf_hcmd hcmd;
};

#define IWL_FW_CMD_VER_UNKNOWN 99

struct __attribute__((packed)) iwl_fw_cmd_version {
    UInt8 cmd;
    UInt8 group;
    UInt8 cmd_ver;
    UInt8 notif_ver;
};

#endif /* file_h */
