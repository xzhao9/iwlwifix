//
//  acpi.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#include "utils.hpp"
#include "acpi.hpp"
#include "../linux/errno.hpp"

OSObject *iwl_acpi_get_object(Iwlwifix *dev, const char * method) {
    OSObject *result;
    IOReturn ret = dev->acpiDevice->evaluateObject(method, &result);
    if(ret) {
        DEBUGLOG("[iwlwifix] Failed to call the %s method!\n", method);
    }
    return result;
}

OSArray *iwl_acpi_get_wifi_pkg(Iwlwifix *dev, OSObject *data, int data_size) {
    int i;
    OSArray *wifi_pkg = NULL;
    
    OSArray *array = OSDynamicCast(OSArray, data);
    if(!array) {
        DEBUGLOG("[iwlwifix] Passed in a non-array package.");
    }
    int count = array->getCount();
    if(count < 2) {
        DEBUGLOG("[iwlwifix] Unsupported package structure.");
    }
    OSNumber *num = OSDynamicCast(OSNumber, array->getObject(0));
    if(!num || num->unsigned32BitValue() != 0) {
        DEBUGLOG("[iwlwifix] Unsupported package: revision number.");
    }
    // Loop through all packages to find the one for WiFi
    for(i = 1; i < count; i ++) {
        OSArray *pkg = OSDynamicCast(OSArray, array->getObject(i));
        if(pkg) { // found OSArray
            // check if the first element is Number
            OSNumber *pkgtype = OSDynamicCast(OSNumber, pkg->getObject(0));
            DEBUGLOG("Found package count: %u, type : %u", pkg->getCount(), pkgtype->unsigned32BitValue());
            if(pkg->getCount() != data_size) {
                continue;
            }
            if(pkgtype->unsigned32BitValue() == ACPI_WIFI_DOMAIN) {
                wifi_pkg = pkg;
                break;
            }
        }
    }
    if(wifi_pkg) {
        return wifi_pkg;
    } else {
        DEBUGLOG("[iwlwifix] Sorry I don't find where is wifipkg.");
        return NULL;
    }
}

int iwl_acpi_get_mcc(Iwlwifix *dev, char *mcc) {
    OSObject *data;
    OSArray *wifi_pkg;
    int ret = 0;
    
    data = iwl_acpi_get_object(dev, ACPI_WRDD_METHOD);
    if(data == NULL) {
        DEBUGLOG("[iwlwifix] Failed to get object using WRDD.");
        if(IS_ERR(data)) {
            return PTR_ERR(data);
        }
    } else {
        wifi_pkg = iwl_acpi_get_wifi_pkg(dev, data, ACPI_WRDD_WIFI_DATA_SIZE);
        if(IS_ERR(wifi_pkg)) {
            DEBUGLOG("[iwlwifix] Failed to get wiki pkg using WRDD.");
            ret = PTR_ERR(wifi_pkg);
        } else {
            OSNumber* mcc_val = OSDynamicCast(OSNumber, wifi_pkg->getObject(1));
            UInt16 mmcc_val = mcc_val->unsigned16BitValue();
            mcc[0] = (mmcc_val >> 8) & 0xff;
            mcc[1] = mmcc_val & 0xff;
            mcc[2] = '\0';
            ret = 0;
            DEBUGLOG("[iwlwifix] Finally I get the mcc val number: %u", mcc_val->unsigned32BitValue());
        }
    }
    OSSafeReleaseNULL(data);
    return ret;
}

UInt64 iwl_acpi_get_pwr_limit(Iwlwifix *dev) {
    OSObject *data;
    OSArray *wifi_pkg;
    UInt64 dflt_pwr_limit = 0;
    
    data = iwl_acpi_get_object(dev, ACPI_SPLC_METHOD);
    if(IS_ERR(data)) {
        DEBUGLOG("[iwlwifix] Failed to get object using SPLC.");
        dflt_pwr_limit = 0;
        return dflt_pwr_limit;
    } else {
        wifi_pkg = iwl_acpi_get_wifi_pkg(dev, data, ACPI_SPLC_WIFI_DATA_SIZE);
        if(wifi_pkg == NULL) {
            DEBUGLOG("[iwlwifix] Failed to get wiki pkg using SPLC.");
            dflt_pwr_limit = 0;
            OSSafeReleaseNULL(data);
            return dflt_pwr_limit;
        } else {
            OSNumber* res = OSDynamicCast(OSNumber, wifi_pkg->getObject(1));
            DEBUGLOG("[iwlwifix] Finally I get the mcc val number: %u", res->unsigned32BitValue());
            dflt_pwr_limit = res->unsigned64BitValue();
        }
    }
    OSSafeReleaseNULL(data);
    return dflt_pwr_limit;
}

int iwl_acpi_get_eckv(Iwlwifix *dev, UInt32* extl_clk) {
    OSObject *data;
    OSArray *wifi_pkg;
    int ret = 0;
    
    data = iwl_acpi_get_object(dev, ACPI_ECKV_METHOD);
    if(IS_ERR(data)) {
        DEBUGLOG("[iwlwifix] Failed to get object using ECKV.");
        return PTR_ERR(data);
    } else {
        wifi_pkg = iwl_acpi_get_wifi_pkg(dev, data, ACPI_ECKV_WIFI_DATA_SIZE);
        if(wifi_pkg == NULL) {
            DEBUGLOG("[iwlwifix] Failed to get wiki pkg using ECKV.");
            ret = PTR_ERR(wifi_pkg);
            OSSafeReleaseNULL(wifi_pkg);
            return ret;
        } else {
            OSNumber* res = OSDynamicCast(OSNumber, wifi_pkg->getObject(1));
            DEBUGLOG("[iwlwifix] Finally I get the eckv number: %u", res->unsigned32BitValue());
        }
    }
    OSSafeReleaseNULL(data);
    return ret;
}
