//
//  acpi.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef acpi_hpp
#define acpi_hpp

#include <IOKit/IOLib.h>
#include <IOKit/pci/IOPCIDevice.h>
#include <libkern/OSKextLib.h>
#include "../linux/acpi.hpp"
#include "../iwlwifix.hpp"

#define ACPI_WRDS_METHOD    "WRDS"
#define ACPI_EWRD_METHOD    "EWRD"
#define ACPI_WGDS_METHOD    "WGDS"
#define ACPI_WRDD_METHOD    "WRDD"
#define ACPI_SPLC_METHOD    "SPLC"
#define ACPI_ECKV_METHOD    "ECKV"

#define ACPI_WIFI_DOMAIN    (0x07)

#define ACPI_SAR_TABLE_SIZE        10
#define ACPI_SAR_PROFILE_NUM        4

#define ACPI_GEO_TABLE_SIZE         6
#define ACPI_NUM_GEO_PROFILES       3
#define ACPI_GEO_PER_CHAIN_SIZE     3

#define ACPI_SAR_NUM_CHAIN_LIMITS   2
#define ACPI_SAR_NUM_SUB_BANDS      5

#define ACPI_WRDS_WIFI_DATA_SIZE    (ACPI_SAR_TABLE_SIZE + 2)
#define ACPI_EWRD_WIFI_DATA_SIZE    ((ACPI_SAR_PROFILE_NUM - 1) * \
ACPI_SAR_TABLE_SIZE + 3)
#define ACPI_WGDS_WIFI_DATA_SIZE    19
#define ACPI_WRDD_WIFI_DATA_SIZE    2
#define ACPI_SPLC_WIFI_DATA_SIZE    2
#define ACPI_ECKV_WIFI_DATA_SIZE    2

#define ACPI_WGDS_NUM_BANDS        2
#define ACPI_WGDS_TABLE_SIZE        3

OSObject *iwl_acpi_get_object(Iwlwifix *dev, const acpi_string method);
OSArray *iwl_acpi_get_wifi_pkg(Iwlwifix *dev, OSObject *data, int data_size);

/**
 * iwl_acpi_get_mcc - read MCC from ACPI, if available
 *
 * @dev: the struct device
 * @mcc: output buffer (3 bytes) that will get the MCC
 *
 * This function tries to read the current MCC from ACPI if available.
 */
int iwl_acpi_get_mcc(Iwlwifix *dev, char *mcc);

UInt64 iwl_acpi_get_pwr_limit(Iwlwifix *dev);

/*
 * iwl_acpi_get_eckv - read external clock validation from ACPI, if available
 *
 * @dev: the struct device
 * @extl_clk: output var (2 bytes) that will get the clk indication.
 *
 * This function tries to read the external clock indication
 * from ACPI if available.
 */
int iwl_acpi_get_eckv(Iwlwifix *dev, UInt32 *extl_clk);


#endif /* acpi_hpp */
