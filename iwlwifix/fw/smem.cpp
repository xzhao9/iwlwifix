//
//  smem.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-09.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//
#include "../linux/align.hpp"
#include "runtime.hpp"
#include "./api/commands.hpp"
#include "./api/debug.hpp"

static void iwl_parse_shared_mem(struct iwl_fw_runtime *fwrt,
                                 struct iwl_rx_packet *pkt)
{
    struct iwl_shared_mem_cfg_v2 *mem_cfg = (struct iwl_shared_mem_cfg_v2 *)pkt->data;
    int i;
    
    fwrt->smem_cfg.num_lmacs = 1;
    
    fwrt->smem_cfg.num_txfifo_entries = ARRAY_SIZE(mem_cfg->txfifo_size);
    for (i = 0; i < ARRAY_SIZE(mem_cfg->txfifo_size); i++)
        fwrt->smem_cfg.lmac[0].txfifo_size[i] =
        le32_to_cpu(mem_cfg->txfifo_size[i]);
    
    fwrt->smem_cfg.lmac[0].rxfifo1_size =
    le32_to_cpu(mem_cfg->rxfifo_size[0]);
    fwrt->smem_cfg.rxfifo2_size = le32_to_cpu(mem_cfg->rxfifo_size[1]);
    
    /* new API has more data, from rxfifo_addr field and on */
    if (fw_has_capa(&fwrt->fw->ucode_capa,
                    IWL_UCODE_TLV_CAPA_EXTEND_SHARED_MEM_CFG)) {
        /*BUILD_BUG_ON(sizeof(fwrt->smem_cfg.internal_txfifo_size) !=
                     sizeof(mem_cfg->internal_txfifo_size));*/
        
        fwrt->smem_cfg.internal_txfifo_addr =
        le32_to_cpu(mem_cfg->internal_txfifo_addr);
        
        for (i = 0;
             i < ARRAY_SIZE(fwrt->smem_cfg.internal_txfifo_size);
             i++)
            fwrt->smem_cfg.internal_txfifo_size[i] =
            le32_to_cpu(mem_cfg->internal_txfifo_size[i]);
    }
}

void iwl_get_shared_mem_conf(struct iwl_fw_runtime *fwrt)
{
    struct iwl_host_cmd cmd = {
        .flags = CMD_WANT_SKB,
        .data = { NULL, },
        .len = { 0, },
    };
    struct iwl_rx_packet *pkt;
    int ret;
    
    if (fw_has_capa(&fwrt->fw->ucode_capa,
                    IWL_UCODE_TLV_CAPA_EXTEND_SHARED_MEM_CFG))
        cmd.id = iwl_cmd_id(SHARED_MEM_CFG_CMD, SYSTEM_GROUP, 0);
    else
        cmd.id = SHARED_MEM_CFG;
    
    ret = iwl_trans_send_cmd(fwrt->trans, &cmd);
    
    if (ret) {
        /*WARN(ret != -ERFKILL,
             "Could not send the SMEM command: %d\n", ret);*/
        return;
    }
    
    pkt = cmd.resp_pkt;
    
    iwl_parse_shared_mem(fwrt, pkt);
    
    // IWL_DEBUG_INFO(fwrt, "SHARED MEM CFG: got memory offsets/sizes\n");
    
    iwl_free_resp(&cmd);
}
