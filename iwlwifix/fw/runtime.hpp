//
//  runtime.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-08.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef runtime_h
#define runtime_h

#include "../iwl-config.hpp"
#include "../iwl-trans.hpp"
#include "img.hpp"
#include "./api/paging.hpp"
#include "../iwl-eeprom-parse.hpp"

struct iwl_fw_runtime_ops {
    int (*dump_start)(void *ctx);
    void (*dump_end)(void *ctx);
    bool (*fw_running)(void *ctx);
    int (*send_hcmd)(void *ctx, struct iwl_host_cmd *host_cmd);
    bool (*d3_debug_enable)(void *ctx);
};

#define MAX_NUM_LMAC 2
struct iwl_fwrt_shared_mem_cfg {
    int num_lmacs;
    int num_txfifo_entries;
    struct {
        UInt32 txfifo_size[TX_FIFO_MAX_NUM];
        UInt32 rxfifo1_size;
    } lmac[MAX_NUM_LMAC];
    UInt32 rxfifo2_size;
    UInt32 internal_txfifo_addr;
    UInt32 internal_txfifo_size[TX_FIFO_INTERNAL_MAX_NUM];
};

enum iwl_fw_runtime_status {
    IWL_FWRT_STATUS_DUMPING = 0,
};

/**
 * struct iwl_fw_runtime - runtime data for firmware
 * @fw: firmware image
 * @cfg: NIC configuration
 * @dev: device pointer
 * @ops: user ops
 * @ops_ctx: user ops context
 * @status: status flags
 * @fw_paging_db: paging database
 * @num_of_paging_blk: number of paging blocks
 * @num_of_pages_in_last_blk: number of pages in the last block
 * @smem_cfg: saved firmware SMEM configuration
 * @cur_fw_img: current firmware image, must be maintained by
 *    the driver by calling &iwl_fw_set_current_image()
 * @dump: debug dump data
 */
struct iwl_fw_runtime {
    struct iwl_trans *trans;
    const struct iwl_fw *fw;
    struct IOPCIDevice *dev;
    
    const struct iwl_fw_runtime_ops *ops;
    void *ops_ctx;
    
    unsigned long status;
    
    /* Paging */
    struct iwl_fw_paging fw_paging_db[NUM_OF_FW_PAGING_BLOCKS];
    UInt16 num_of_paging_blk;
    UInt16 num_of_pages_in_last_blk;
    
    enum iwl_ucode_type cur_fw_img;
    
    /* memory configuration */
    struct iwl_fwrt_shared_mem_cfg smem_cfg;
    
    /* debug */
//    struct {
//        const struct iwl_fw_dump_desc *desc;
//        bool monitor_only;
//        struct delayed_work wk;
//
//        UInt8 conf;
//
//        /* ts of the beginning of a non-collect fw dbg data period */
//        unsigned long non_collect_ts_start[IWL_FW_TRIGGER_ID_NUM];
//        UInt32 *d3_debug_data;
//        struct iwl_fw_ini_region_cfg *active_regs[IWL_FW_INI_MAX_REGION_ID];
//        struct iwl_fw_ini_active_triggers active_trigs[IWL_FW_TRIGGER_ID_NUM];
//        UInt32 lmac_err_id[MAX_NUM_LMAC];
//        UInt32 umac_err_id;
//        void *fifo_iter;
//        enum iwl_fw_ini_trigger_id ini_trig_id;
//        struct timer_list periodic_trig;
//    } dump;
};

void iwl_fw_runtime_init(struct iwl_fw_runtime *fwrt, struct iwl_trans *trans,
                         const struct iwl_fw *fw,
                         const struct iwl_fw_runtime_ops *ops, void *ops_ctx,
                         struct dentry *dbgfs_dir);

static inline void iwl_fw_runtime_free(struct iwl_fw_runtime *fwrt)
{
    int i;
//
//    kfree(fwrt->dump.d3_debug_data);
//    fwrt->dump.d3_debug_data = NULL;
    
    for (i = 0; i < IWL_FW_TRIGGER_ID_NUM; i++) {
    //    struct iwl_fw_ini_active_triggers *active =
//        &fwrt->dump.active_trigs[i];
//
     /*   active->active = false;
        active->size = 0;
        IOFree(active->trig, sizeof(struct iwl_fw_ini_trigger));
        active->trig = NULL;*/
    }
}

void iwl_fw_runtime_suspend(struct iwl_fw_runtime *fwrt);

void iwl_fw_runtime_resume(struct iwl_fw_runtime *fwrt);

static inline void iwl_fw_set_current_image(struct iwl_fw_runtime *fwrt,
                                            enum iwl_ucode_type cur_fw_img)
{
    fwrt->cur_fw_img = cur_fw_img;
}

int iwl_init_paging(struct iwl_fw_runtime *fwrt, enum iwl_ucode_type type);
void iwl_free_fw_paging(struct iwl_fw_runtime *fwrt);

void iwl_get_shared_mem_conf(struct iwl_fw_runtime *fwrt);

#endif /* runtime_h */
