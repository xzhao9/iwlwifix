//
//  iwlwifix.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-02.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#include "iwlwifix.hpp"
#include "utils.hpp"
#include "pcie/drv.hpp"
#include "linux/firmware.hpp"
#include "linux/memory.hpp"
#include "pcie/internal.hpp"
#include "fw/acpi.hpp"
#include "iwl-agn-hw.hpp"
#include <IOKit/IOLib.h>

#define IWL_DEFAULT_SCAN_CHANNELS 40

struct iwlwifix_opmode_table iwlwifix_opmode_table[] = {
    [DVM_OP_MODE] = {.name = "iwldvm", .ops = NULL},
    [MVM_OP_MODE] = {.name = "iwlmvm", .ops = NULL},
};

OSDefineMetaClassAndStructors(Iwlwifix, IOService);

// ------------------------ IOService Methods ----------------------------

bool Iwlwifix::init(OSDictionary *dict) {
    bool res = IOService::init(dict);
    return res;
}

IOService* Iwlwifix::probe(IOService* provider, SInt32* score) {
    IOService::probe(provider, score);
    IOPCIDevice *pciDevice;
    pciDevice = OSDynamicCast(IOPCIDevice, provider);
    if(!pciDevice) {
        DEBUGLOG("[iwlwifix] Probe: provider is not a PCIDevice.\n");
        return NULL;
    }
    UInt16 vendor = pciDevice->configRead16(kIOPCIConfigVendorID);
    UInt16 device = pciDevice->configRead16(kIOPCIConfigDeviceID);
    UInt16 subsystemVendor = pciDevice->configRead16(kIOPCIConfigSubSystemVendorID);
    UInt16 subsystemDevice = pciDevice->configRead16(kIOPCIConfigSubSystemID);
    
    // Model: Intel Dual Band 9560 Wireless AC (2 x 2) & Bluetooth 5.0
    // Vendor ID: 0x8086, Device ID: 0xa370, Subsystem Vendor ID: 0x8086, SubSystem Device ID: 0x0030
    DEBUGLOG("[iwlwifix] Probe: Vendor ID: %#06x, device ID: %#06x, subsystem vendor id: %#06x, subsystem device id: %#06x\n", \
             vendor, device, subsystemVendor, subsystemDevice);
    cfg = NULL;
    for(UInt32 i = 0; i < sizeof(iwlHwCardIds); i ++) {
        if(vendor == iwlHwCardIds[i].vendor && device == iwlHwCardIds[i].device && \
           subsystemVendor == iwlHwCardIds[i].subvendor && subsystemDevice == iwlHwCardIds[i].subdevice) {
            device_cardid = i;
            cfg = iwlHwCardIds[i].driver_data;
            break;
        }
    }
    if(cfg == NULL) {
        DEBUGLOG("[iwlwifix] Error Probe: Cannot find the matched device id: %#06x\n", device);
        return NULL;
    }
    if(!cfg->csr) {
        DEBUGLOG("[iwlwifix] Error Probe: CSR addresses aren't configured\n");
        return NULL;
    }
    
    DEBUGLOG("[iwlwifix] Device probing successful, your device model is: %s\n", cfg->name);
    return this;
}

bool Iwlwifix::start(IOService *provider) {
    if(!IOService::start(provider) || !cfg) {
        DEBUGLOG("[iwlwifix] Super class start failed or cfg is NULL.\n");
        cfg = NULL;
        return false;
    }
    if(!cfg->csr) {
        DEBUGLOG("[iwlwifix] CSR addresses aren't configured\n");
        cfg = NULL;
        return false;
    }
    pciDevice = OSDynamicCast(IOPCIDevice, provider);
    if(!pciDevice) {
        DEBUGLOG("[iwlwifix] Probe: provider is not a PCIDevice.\n");
        pciDevice = NULL;
        cfg = NULL;
        return false;
    }
    pciDevice->retain();
    // Find the ACPI Device
    acpiDevice = (IOACPIPlatformDevice*) pciDevice->getProperty("acpi-device")->metaCast("IOACPIPlatformDevice");
    if(!acpiDevice) {
        DEBUGLOG("[iwlwifix] Failed to get the ACPI Platform device.\n");
        pciDevice->release();
        pciDevice = NULL;
        cfg = NULL;
        return false;
    }
    acpiDevice->retain();
    // Both PCIDevice and ACPIDevice are ready, open the interface and run enable()
    if(!pciDevice->open(this)) {
        DEBUGLOG("[iwlwifix] Failed to open provider.\n");
        releaseAll();
        return false;
    }
    intelInitConfig(pciDevice);         // Init pciDeviceData
    intelEnablePCIDevice(pciDevice);    // Enable the Bus Master and Memory Space
    //    DEBUGLOG("[iwlwifix] PCIDevice retrieved Data HW rev-id: %#04x", pciDeviceData.revision); // 0x10
    
    
    struct iwl_trans* transl = iwl_trans_pcie_alloc(this, cfg);
//    if(IS_ERR(transl)) {
//        DEBUGLOG("[iwlwifix] Failed to allocated pcie trans. Aborting\n");
//        pciDevice->close(this);
//        releaseAll();
//        return false;
//    }
    // Free trans after we get the HW REV, verify that it is correct towards Linux!

    // ret = iwl_drv_start(transl);
//    if(ret != kOSReturnSuccess) {
//        return false;
//    } else {
//        DEBUGLOG("[iwlwifix] I am ready to roll.");
//        return true;
//    }
    // iwl_trans_free(trans, sizeof(struct iwl_trans_pcie));
    // After allocating trans, init firmware
    // Start successful
    
    // getParams()
    // intelStart()
    // getCommandGate()
    // initEventSources()
    // attachInterface()
    pciDevice->close(this);
    return true;
}

void Iwlwifix::setDMAMask(UInt64 mask) {
    dma_mask = mask;
}

void Iwlwifix::setConsistentDMAMask(UInt64 mask) {
    coherent_dma_mask = mask;
}

void Iwlwifix::stop(IOService *provider) {
    IOPCIDevice *pciDevice2 = OSDynamicCast(IOPCIDevice, provider);
    if(pciDevice2) {
        intelDisablePCIDevice(pciDevice2);
    } else if(pciDevice) {
        intelDisablePCIDevice(pciDevice);
    }
    
    IOService::stop(provider);
    DEBUGLOG("[iwlwifix] Device stopped.\n");
}

void Iwlwifix::free() {
    // DEBUGLOG("[iwlwifix] pciDevice count: %d ", pciDevice->getRetainCount());
    // DEBUGLOG("[iwlwifix] acpiDevice count: %#02x ", acpiDevice->getRetainCount());
    releaseAll();
    IOService::free();
    DEBUGLOG("[iwlwifix] Device freed.\n");
}

// ----------------------- Private methods ---------------------------------
void Iwlwifix::releaseAll(void) {
    pciDevice->release();
    pciDevice = NULL;
    acpiDevice->release();
    acpiDevice = NULL;
    cfg = NULL;
    return;
}

void Iwlwifix::intelInitConfig(IOPCIDevice *provider) {
    pciDeviceData.vendor = provider->extendedConfigRead16(kIOPCIConfigVendorID);
    pciDeviceData.device = provider->extendedConfigRead16(kIOPCIConfigDeviceID);
    pciDeviceData.subsystem_vendor = provider->extendedConfigRead16(kIOPCIConfigSubSystemVendorID);
    pciDeviceData.subsystem_device = provider->extendedConfigRead16(kIOPCIConfigSubSystemID);
    pciDeviceData.revision = provider->extendedConfigRead8(kIOPCIConfigRevisionID);
}

inline void Iwlwifix::intelEnablePCIDevice(IOPCIDevice *provider) {
    UInt16 cmdReg;
    cmdReg = provider->extendedConfigRead16(kIOPCIConfigCommand);
    cmdReg |= (kIOPCICommandBusMaster | kIOPCICommandMemorySpace);
    cmdReg &= ~kIOPCICommandIOSpace; // No one is using PCI IOSpace, so disable it
    provider->extendedConfigWrite16(kIOPCIConfigCommand, cmdReg);
}

inline void Iwlwifix::intelDisablePCIDevice(IOPCIDevice *provider) {
    UInt16 cmdReg;
    cmdReg = provider->extendedConfigRead16(kIOPCIConfigCommand);
    cmdReg &= ~(kIOPCICommandBusMaster | kIOPCICommandMemorySpace | kIOPCICommandIOSpace);
    provider->extendedConfigWrite16(kIOPCIConfigCommand, cmdReg);
}

void Iwlwifix::iwl_free_fw_desc(struct fw_desc *desc) {
    IOFree((void*)desc->data, desc->len);
    desc->data = NULL;
    desc->len = 0;
}

void Iwlwifix::iwl_free_fw_img(struct fw_img *img) {
    int i;
    for(i = 0; i < img->num_sec; i ++) {
        iwl_free_fw_desc(&img->sec[i]);
    }
    IOFree((void*)img->sec, sizeof(struct fw_desc) * img->num_sec);
}

void Iwlwifix::iwl_dealloc_ucode(void) {
    int i;
    // Disable fw debugging tool
//    kfree(drv->fw.dbg.dest_tlv);
//    for (i = 0; i < ARRAY_SIZE(drv->fw.dbg.conf_tlv); i++)
//        kfree(drv->fw.dbg.conf_tlv[i]);
//    for (i = 0; i < ARRAY_SIZE(drv->fw.dbg.trigger_tlv); i++)
//        kfree(drv->fw.dbg.trigger_tlv[i]);
//    kfree(drv->fw.dbg.mem_tlv);
    IOFree((void*)fw.iml, fw.iml_len);
    IOFree((void*)fw.ucode_capa.cmd_versions, fw.ucode_capa.n_cmd_versions * (sizeof(struct iwl_fw_cmd_version)));
    
    for(i = 0; i < IWL_UCODE_TYPE_MAX; i ++) {
        iwl_free_fw_img(fw.img + i);
    }
}

// ----------------------- Private methods ---------------------------------
int Iwlwifix::iwl_request_firmware(void) {
    const struct iwl_cfg *cfg = this->trans->cfg;
    char tag[8];
    
    if(trans->cfg->device_family == IWL_DEVICE_FAMILY_9000 &&
       (CSR_HW_REV_STEP(trans->hw_rev) != SILICON_B_STEP) &&
       (CSR_HW_REV_STEP(trans->hw_rev) != SILICON_C_STEP)) {
        DEBUGLOG("[iwlwifix] ERROR: Only HW Steps B and C are supporetd");
        return -EINVAL;
    }
    
    // Hardcode the firmware tag for Intel 9265
    fw_index = 34;
    sprintf(tag, "%d", fw_index);
    
    if(fw_index < cfg->ucode_api_min) {
        DEBUGLOG("[iwlwifix] ERROR: on suitable firmware found!");
        if(cfg->ucode_api_min == cfg->ucode_api_max) {
            DEBUGLOG("[iwlwifix] ERROR: %s%d is required", cfg->fw_name_pre, cfg->ucode_api_min);
        } else {
            DEBUGLOG("[iwlwifix] ERROR: minimum - maximum version is required %s%d - %d", cfg->fw_name_pre, cfg->ucode_api_min, cfg->ucode_api_max);
        }
        DEBUGLOG("[iwlwifix] Please check your firmware file");
        return -ENOENT;
    }
    
    snprintf(firmware_name, sizeof(firmware_name), "%s%s.ucode", cfg->fw_name_pre, tag);
    
    OSReturn ret = OSKextRequestResource(OSKextGetCurrentIdentifier(), firmware_name,
                                         Iwlwifix::iwl_req_fw_callback, this, NULL);
    return ret;
}

struct fw_sec* Iwlwifix::get_sec(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec) {
    return &pieces->img[type].sec[sec];
}

void Iwlwifix::alloc_sec_data(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec) {
    struct fw_img_parsing *img = &pieces->img[type];
    struct fw_sec *sec_memory;
    int size = sec + 1;
    if(img->sec && img->sec_counter >= size) {
        return;
    }
    if(img->sec && img->sec_counter) { // Free existing memory if exists
        IOFree(img->sec, sizeof(*img->sec) * img->sec_counter);
    }
    size_t alloc_size = sizeof(*img->sec) * size;
    sec_memory = (struct fw_sec *)IOMalloc(alloc_size);
    if(!sec_memory) {
        return;
    }
    img->sec = sec_memory;
    img->sec_counter = size;
}

// data is pre-allocated
void Iwlwifix::set_sec_data(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec, const void *data) {
    alloc_sec_data(pieces, type, sec);
    pieces->img[type].sec[sec].data = data;
}

void Iwlwifix::set_sec_size(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec, size_t size) {
    alloc_sec_data(pieces, type, sec);
    pieces->img[type].sec[sec].size = size;
}

size_t Iwlwifix::get_sec_size(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec) {
    return pieces->img[type].sec[sec].size;
}

void Iwlwifix::set_sec_offset(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type, int sec, UInt32 offset) {
    alloc_sec_data(pieces, type, sec);
    pieces->img[type].sec[sec].offset = offset;
}

int Iwlwifix::iwl_store_cscheme(struct iwl_fw *fw, const UInt8 *data, const UInt32 len) {
    int i, j;
    struct iwl_fw_cscheme_list *l = (struct iwl_fw_cscheme_list*) data;
    struct iwl_fw_cipher_scheme *fwcs;
    if (len < sizeof(*l) ||
        len < sizeof(l->size) + l->size * sizeof(l->cs[0]))
        return -EINVAL;
    
    for (i = 0, j = 0; i < IWL_UCODE_MAX_CS && i < l->size; i++) {
        fwcs = &l->cs[j];
        
        /* we skip schemes with zero cipher suite selector */
        if (!fwcs->cipher)
            continue;
        fw->cs[j++] = *fwcs;
    }
    return 0;
}

int Iwlwifix::iwl_store_ucode_sec(struct iwl_firmware_pieces *pieces, const void *data, enum iwl_ucode_type type, int size) {
    struct fw_img_parsing *img;
    struct fw_sec *sec;
    struct fw_sec_parsing *sec_parse;
    size_t alloc_size;
    
    if (WARN_ON(!pieces || !data || type >= IWL_UCODE_TYPE_MAX))
        return -1;
    
    sec_parse = (struct fw_sec_parsing *)data;
    
    img = &pieces->img[type];
    
    alloc_size = sizeof(*img->sec) * (img->sec_counter + 1);
    sec =  (struct fw_sec *)krealloc(img->sec, img->sec_counter, alloc_size);
    if (!sec)
        return -ENOMEM;
    img->sec = sec;
    
    sec = &img->sec[img->sec_counter];
    
    sec->offset = le32_to_cpu(sec_parse->offset);
    sec->data = sec_parse->data;
    sec->size = size - sizeof(sec_parse->offset);
    
    ++img->sec_counter;
    
    return 0;
}

int Iwlwifix::iwl_set_default_calib(const UInt8 *data) {
    struct iwl_tlv_calib_data *def_calib = (struct iwl_tlv_calib_data *)data;
    UInt32 ucode_type = le32_to_cpu(def_calib->ucode_type);
    if(ucode_type >= IWL_UCODE_TYPE_MAX) {
        DEBUGLOG("[iwlwifix] Wrong ucode_type %u for default calibration.", ucode_type);
        return -EINVAL;
    }
    fw.default_calib[ucode_type].flow_trigger = def_calib->calib.flow_trigger;
    fw.default_calib[ucode_type].event_trigger = def_calib->calib.event_trigger;
    return 0;
}

static void iwl_set_ucode_api_flags(struct iwl_drv *drv, const UInt8 *data,
                                    struct iwl_ucode_capabilities *capa)
{
    const struct iwl_ucode_api *ucode_api = (struct iwl_ucode_api *)data;
    UInt32 api_index = le32_to_cpu(ucode_api->api_index);
    UInt32 api_flags = le32_to_cpu(ucode_api->api_flags);
    int i;
    
    if (api_index >= DIV_ROUND_UP(NUM_IWL_UCODE_TLV_API, 32)) {
        DEBUGLOG("[iwlwifix] api flags index %d larger than supported by driver\n",
                 api_index);
        return;
    }
    
    for (i = 0; i < 32; i++) {
        if (api_flags & BIT(i)) // Use setbit here
            set_bit(i + 32 * api_index, capa->_api);
    }
}

void Iwlwifix::iwl_set_ucode_capabilities(const UInt8 *data,
                                          struct iwl_ucode_capabilities *capa)
{
    const struct iwl_ucode_capa *ucode_capa = (struct iwl_ucode_capa *)data;
    UInt32 api_index = le32_to_cpu(ucode_capa->api_index);
    UInt32 api_flags = le32_to_cpu(ucode_capa->api_capa);
    int i;
    
    if (api_index >= DIV_ROUND_UP(NUM_IWL_UCODE_TLV_CAPA, 32)) {
        DEBUGLOG("[iwlwifix] capa flags index %d larger than supported by driver",
                 api_index);
        return;
    }
    
    for (i = 0; i < 32; i++) {
        if (api_flags & BIT(i))
            set_bit(i + 32 * api_index, capa->_capa);
    }
}

#define FW_ADDR_CACHE_CONTROL 0xC0000000

// We support TLV firmware only - because Inel 9560 uses TLV firmware   
int Iwlwifix::iwl_parse_tlv_firmware(const struct firmware *ucode_raw, struct iwl_firmware_pieces *pieces, struct iwl_ucode_capabilities *capa, bool *usniffer_images) {
    struct iwl_tlv_ucode_header *ucode = (struct iwl_tlv_ucode_header*)ucode_raw->data;
    struct iwl_ucode_tlv *tlv;
    size_t len = ucode_raw->size;
    const UInt8* data;
    UInt32 tlv_len;
    UInt32 usniffer_img;
    enum iwl_ucode_tlv_type tlv_type;
    const UInt8 *tlv_data;
    char buildstr[25];
    UInt32 build, paging_mem_size;
    int num_of_cpus;
    bool usniffer_req = false;

    if(len < sizeof(*ucode)) {
        DEBUGLOG("[iwlwifix] ERROR: uCode has invalid length: %zd", len);
        return -EINVAL;
    }
    
    if(ucode->magic != cpu_to_le32(IWL_TLV_UCODE_MAGIC)) {
        DEBUGLOG("[iwlwifix] ERROR: invalid uCode magic: %#08x", le32_to_cpu(ucode->magic));
        return -EINVAL;
    }
    fw.ucode_ver = le32_to_cpu(ucode->ver);
    memcpy(fw.human_readable, ucode->human_readable, sizeof(fw.human_readable));
    build = le32_to_cpu(ucode->build);
    
    if(build) {
        sprintf(buildstr, " build %u", build);
    } else {
        buildstr[0] = '\0';
    }
    
    snprintf(fw.fw_version,
             sizeof(fw.fw_version),
             "%u.%u.%u.%u%s",
             IWL_UCODE_MAJOR(fw.ucode_ver),
             IWL_UCODE_MINOR(fw.ucode_ver),
             IWL_UCODE_API(fw.ucode_ver),
             IWL_UCODE_SERIAL(fw.ucode_ver),
             buildstr);
    data = ucode->data;
    len -= sizeof(*ucode);
    
    if(iwlwifix_mod_params.enable_ini) {
        // We don't allocate debug tlv
        // iwl_alloc_dbg_tlv(drv->trans, len, data, false);
    }
    
    while (len >= sizeof(*tlv)) {
        len -= sizeof(*tlv);
        tlv = (struct iwl_ucode_tlv *)data;
        
        tlv_len = le32_to_cpu(tlv->length);
        tlv_type = (enum iwl_ucode_tlv_type) le32_to_cpu(tlv->type);
        tlv_data = tlv->data;
        
        if (len < tlv_len) {
            DEBUGLOG("[iwlwifix] invalid TLV len: %zd/%u\n",
                    len, tlv_len);
            return -EINVAL;
        }
        len -= ALIGN(tlv_len, 4);
        data += sizeof(*tlv) + ALIGN(tlv_len, 4);
        
        switch (tlv_type) {
            case IWL_UCODE_TLV_INST:
                set_sec_data(pieces, IWL_UCODE_REGULAR,
                             IWL_UCODE_SECTION_INST, tlv_data);
                set_sec_size(pieces, IWL_UCODE_REGULAR,
                             IWL_UCODE_SECTION_INST, tlv_len);
                set_sec_offset(pieces, IWL_UCODE_REGULAR,
                               IWL_UCODE_SECTION_INST,
                               IWLAGN_RTC_INST_LOWER_BOUND);
                break;
            case IWL_UCODE_TLV_DATA:
                set_sec_data(pieces, IWL_UCODE_REGULAR,
                             IWL_UCODE_SECTION_DATA, tlv_data);
                set_sec_size(pieces, IWL_UCODE_REGULAR,
                             IWL_UCODE_SECTION_DATA, tlv_len);
                set_sec_offset(pieces, IWL_UCODE_REGULAR,
                               IWL_UCODE_SECTION_DATA,
                               IWLAGN_RTC_DATA_LOWER_BOUND);
                break;
            case IWL_UCODE_TLV_INIT:
                set_sec_data(pieces, IWL_UCODE_INIT,
                             IWL_UCODE_SECTION_INST, tlv_data);
                set_sec_size(pieces, IWL_UCODE_INIT,
                             IWL_UCODE_SECTION_INST, tlv_len);
                set_sec_offset(pieces, IWL_UCODE_INIT,
                               IWL_UCODE_SECTION_INST,
                               IWLAGN_RTC_INST_LOWER_BOUND);
                break;
            case IWL_UCODE_TLV_INIT_DATA:
                set_sec_data(pieces, IWL_UCODE_INIT,
                             IWL_UCODE_SECTION_DATA, tlv_data);
                set_sec_size(pieces, IWL_UCODE_INIT,
                             IWL_UCODE_SECTION_DATA, tlv_len);
                set_sec_offset(pieces, IWL_UCODE_INIT,
                               IWL_UCODE_SECTION_DATA,
                               IWLAGN_RTC_DATA_LOWER_BOUND);
                break;
            case IWL_UCODE_TLV_BOOT:
                DEBUGLOG("[iwlwifix] ERROR: Found unexpected BOOT ucode\n");
                break;
            case IWL_UCODE_TLV_PROBE_MAX_LEN:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                capa->max_probe_length =
                le32_to_cpup((__le32 *)tlv_data);
                break;
            case IWL_UCODE_TLV_PAN:
                if (tlv_len)
                    goto invalid_tlv_len;
                capa->flags |= IWL_UCODE_TLV_FLAGS_PAN;
                break;
            case IWL_UCODE_TLV_FLAGS:
                /* must be at least one u32 */
                if (tlv_len < sizeof(UInt32))
                    goto invalid_tlv_len;
                /* and a proper number of u32s */
                if (tlv_len % sizeof(UInt32))
                    goto invalid_tlv_len;
                /*
                 * This driver only reads the first u32 as
                 * right now no more features are defined,
                 * if that changes then either the driver
                 * will not work with the new firmware, or
                 * it'll not take advantage of new features.
                 */
                capa->flags = le32_to_cpup((__le32 *)tlv_data);
                break;
            case IWL_UCODE_TLV_API_CHANGES_SET:
                if (tlv_len != sizeof(struct iwl_ucode_api))
                    goto invalid_tlv_len;
                iwl_set_ucode_api_flags(tlv_data, capa);
                break;
            case IWL_UCODE_TLV_ENABLED_CAPABILITIES:
                if (tlv_len != sizeof(struct iwl_ucode_capa))
                    goto invalid_tlv_len;
                iwl_set_ucode_capabilities(tlv_data, capa);
                break;
            case IWL_UCODE_TLV_INIT_EVTLOG_PTR:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                pieces->init_evtlog_ptr =
                le32_to_cpup((__le32 *)tlv_data);
                break;
            case IWL_UCODE_TLV_INIT_EVTLOG_SIZE:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                pieces->init_evtlog_size =
                le32_to_cpup((__le32 *)tlv_data);
                break;
            case IWL_UCODE_TLV_INIT_ERRLOG_PTR:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                pieces->init_errlog_ptr =
                le32_to_cpup((__le32 *)tlv_data);
                break;
            case IWL_UCODE_TLV_RUNT_EVTLOG_PTR:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                pieces->inst_evtlog_ptr =
                le32_to_cpup((__le32 *)tlv_data);
                break;
            case IWL_UCODE_TLV_RUNT_EVTLOG_SIZE:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                pieces->inst_evtlog_size =
                le32_to_cpup((__le32 *)tlv_data);
                break;
            case IWL_UCODE_TLV_RUNT_ERRLOG_PTR:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                pieces->inst_errlog_ptr =
                le32_to_cpup((__le32 *)tlv_data);
                break;
            case IWL_UCODE_TLV_ENHANCE_SENS_TBL:
                if (tlv_len)
                    goto invalid_tlv_len;
                fw.enhance_sensitivity_table = true;
                break;
            case IWL_UCODE_TLV_WOWLAN_INST:
                set_sec_data(pieces, IWL_UCODE_WOWLAN,
                             IWL_UCODE_SECTION_INST, tlv_data);
                set_sec_size(pieces, IWL_UCODE_WOWLAN,
                             IWL_UCODE_SECTION_INST, tlv_len);
                set_sec_offset(pieces, IWL_UCODE_WOWLAN,
                               IWL_UCODE_SECTION_INST,
                               IWLAGN_RTC_INST_LOWER_BOUND);
                break;
            case IWL_UCODE_TLV_WOWLAN_DATA:
                set_sec_data(pieces, IWL_UCODE_WOWLAN,
                             IWL_UCODE_SECTION_DATA, tlv_data);
                set_sec_size(pieces, IWL_UCODE_WOWLAN,
                             IWL_UCODE_SECTION_DATA, tlv_len);
                set_sec_offset(pieces, IWL_UCODE_WOWLAN,
                               IWL_UCODE_SECTION_DATA,
                               IWLAGN_RTC_DATA_LOWER_BOUND);
                break;
            case IWL_UCODE_TLV_PHY_CALIBRATION_SIZE:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                capa->standard_phy_calibration_size =
                le32_to_cpup((__le32 *)tlv_data);
                break;
            case IWL_UCODE_TLV_SEC_RT:
                iwl_store_ucode_sec(pieces, tlv_data, IWL_UCODE_REGULAR,
                                    tlv_len);
                fw.type = IWL_FW_MVM;
                break;
            case IWL_UCODE_TLV_SEC_INIT:
                iwl_store_ucode_sec(pieces, tlv_data, IWL_UCODE_INIT,
                                    tlv_len);
                fw.type = IWL_FW_MVM;
                break;
            case IWL_UCODE_TLV_SEC_WOWLAN:
                iwl_store_ucode_sec(pieces, tlv_data, IWL_UCODE_WOWLAN,
                                    tlv_len);
                fw.type = IWL_FW_MVM;
                break;
            case IWL_UCODE_TLV_DEF_CALIB:
                if (tlv_len != sizeof(struct iwl_tlv_calib_data))
                    goto invalid_tlv_len;
                if (iwl_set_default_calib(tlv_data))
                    goto tlv_error;
                break;
            case IWL_UCODE_TLV_PHY_SKU:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                fw.phy_config = le32_to_cpup((__le32 *)tlv_data);
                fw.valid_tx_ant = (fw.phy_config &
                                        FW_PHY_CFG_TX_CHAIN) >>
                FW_PHY_CFG_TX_CHAIN_POS;
                fw.valid_rx_ant = (fw.phy_config &
                                        FW_PHY_CFG_RX_CHAIN) >>
                FW_PHY_CFG_RX_CHAIN_POS;
                break;
            case IWL_UCODE_TLV_SECURE_SEC_RT:
                iwl_store_ucode_sec(pieces, tlv_data, IWL_UCODE_REGULAR,
                                    tlv_len);
                fw.type = IWL_FW_MVM;
                break;
            case IWL_UCODE_TLV_SECURE_SEC_INIT:
                iwl_store_ucode_sec(pieces, tlv_data, IWL_UCODE_INIT,
                                    tlv_len);
                fw.type = IWL_FW_MVM;
                break;
            case IWL_UCODE_TLV_SECURE_SEC_WOWLAN:
                iwl_store_ucode_sec(pieces, tlv_data, IWL_UCODE_WOWLAN,
                                    tlv_len);
                fw.type = IWL_FW_MVM;
                break;
            case IWL_UCODE_TLV_NUM_OF_CPU:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                num_of_cpus =
                le32_to_cpup((__le32 *)tlv_data);
                
                if (num_of_cpus == 2) {
                    fw.img[IWL_UCODE_REGULAR].is_dual_cpus =
                    true;
                    fw.img[IWL_UCODE_INIT].is_dual_cpus =
                    true;
                    fw.img[IWL_UCODE_WOWLAN].is_dual_cpus =
                    true;
                } else if ((num_of_cpus > 2) || (num_of_cpus < 1)) {
                    DEBUGLOG("[iwlwlfix] ERROR: Driver support upto 2 CPUs\n");
                    return -EINVAL;
                }
                break;
            case IWL_UCODE_TLV_CSCHEME:
                if (iwl_store_cscheme(&fw, tlv_data, tlv_len))
                    goto invalid_tlv_len;
                break;
            case IWL_UCODE_TLV_N_SCAN_CHANNELS:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                capa->n_scan_channels =
                le32_to_cpup((__le32 *)tlv_data);
                break;
            case IWL_UCODE_TLV_FW_VERSION: {
                __le32 *ptr = (__le32 *)tlv_data;
                UInt32 major, minor;
                UInt8 local_comp;
                
                if (tlv_len != sizeof(UInt32) * 3)
                    goto invalid_tlv_len;
                
                major = le32_to_cpup(ptr++);
                minor = le32_to_cpup(ptr++);
                local_comp = le32_to_cpup(ptr);
                
                if (major >= 35)
                    snprintf(fw.fw_version,
                             sizeof(fw.fw_version),
                             "%u.%08x.%u", major, minor, local_comp);
                else
                    snprintf(fw.fw_version,
                             sizeof(fw.fw_version),
                             "%u.%u.%u", major, minor, local_comp);
                break;
            }
            case IWL_UCODE_TLV_FW_DBG_DEST: {
//                struct iwl_fw_dbg_dest_tlv *dest = NULL;
//                struct iwl_fw_dbg_dest_tlv_v1 *dest_v1 = NULL;
//                UInt8 mon_mode;
//
//                pieces->dbg_dest_ver = (UInt8 *)tlv_data;
//                if (*pieces->dbg_dest_ver == 1) {
//                    dest = (void *)tlv_data;
//                } else if (*pieces->dbg_dest_ver == 0) {
//                    dest_v1 = (void *)tlv_data;
//                } else {
//                    IWL_ERR(drv,
//                            "The version is %d, and it is invalid\n",
//                            *pieces->dbg_dest_ver);
//                    break;
//                }
//
//                if (pieces->dbg_dest_tlv_init) {
//                    IWL_ERR(drv,
//                            "dbg destination ignored, already exists\n");
//                    break;
//                }
//
//                pieces->dbg_dest_tlv_init = true;
//
//                if (dest_v1) {
//                    pieces->dbg_dest_tlv_v1 = dest_v1;
//                    mon_mode = dest_v1->monitor_mode;
//                } else {
//                    pieces->dbg_dest_tlv = dest;
//                    mon_mode = dest->monitor_mode;
//                }
//
//                IWL_INFO(drv, "Found debug destination: %s\n",
//                         get_fw_dbg_mode_string(mon_mode));
//
//                drv->fw.dbg.n_dest_reg = (dest_v1) ?
//                tlv_len -
//                offsetof(struct iwl_fw_dbg_dest_tlv_v1,
//                         reg_ops) :
//                tlv_len -
//                offsetof(struct iwl_fw_dbg_dest_tlv,
//                         reg_ops);
//
//                drv->fw.dbg.n_dest_reg /=
//                sizeof(drv->fw.dbg.dest_tlv->reg_ops[0]);
//
                break;
            }
            case IWL_UCODE_TLV_FW_DBG_CONF: {
//                struct iwl_fw_dbg_conf_tlv *conf = (void *)tlv_data;
//
//                if (!pieces->dbg_dest_tlv_init) {
//                    IWL_ERR(drv,
//                            "Ignore dbg config %d - no destination configured\n",
//                            conf->id);
//                    break;
//                }
//
//                if (conf->id >= ARRAY_SIZE(drv->fw.dbg.conf_tlv)) {
//                    IWL_ERR(drv,
//                            "Skip unknown configuration: %d\n",
//                            conf->id);
//                    break;
//                }
//
//                if (pieces->dbg_conf_tlv[conf->id]) {
//                    IWL_ERR(drv,
//                            "Ignore duplicate dbg config %d\n",
//                            conf->id);
//                    break;
//                }
//
//                if (conf->usniffer)
//                    usniffer_req = true;
//
//                IWL_INFO(drv, "Found debug configuration: %d\n",
//                         conf->id);
//
//                pieces->dbg_conf_tlv[conf->id] = conf;
//                pieces->dbg_conf_tlv_len[conf->id] = tlv_len;
                break;
            }
            case IWL_UCODE_TLV_FW_DBG_TRIGGER: {
//                struct iwl_fw_dbg_trigger_tlv *trigger =
//                (void *)tlv_data;
//                u32 trigger_id = le32_to_cpu(trigger->id);
//
//                if (trigger_id >= ARRAY_SIZE(drv->fw.dbg.trigger_tlv)) {
//                    IWL_ERR(drv,
//                            "Skip unknown trigger: %u\n",
//                            trigger->id);
//                    break;
//                }
//
//                if (pieces->dbg_trigger_tlv[trigger_id]) {
//                    IWL_ERR(drv,
//                            "Ignore duplicate dbg trigger %u\n",
//                            trigger->id);
//                    break;
//                }
//
//                IWL_INFO(drv, "Found debug trigger: %u\n", trigger->id);
//
//                pieces->dbg_trigger_tlv[trigger_id] = trigger;
//                pieces->dbg_trigger_tlv_len[trigger_id] = tlv_len;
                break;
            }
            case IWL_UCODE_TLV_FW_DBG_DUMP_LST: {
//                if (tlv_len != sizeof(u32)) {
//                    IWL_ERR(drv,
//                            "dbg lst mask size incorrect, skip\n");
//                    break;
//                }
//
//                drv->fw.dbg.dump_mask =
//                le32_to_cpup((__le32 *)tlv_data);
                break;
            }
            case IWL_UCODE_TLV_SEC_RT_USNIFFER:
                *usniffer_images = true;
                iwl_store_ucode_sec(pieces, tlv_data,
                                    IWL_UCODE_REGULAR_USNIFFER,
                                    tlv_len);
                break;
            case IWL_UCODE_TLV_PAGING:
                if (tlv_len != sizeof(UInt32))
                    goto invalid_tlv_len;
                paging_mem_size = le32_to_cpup((__le32 *)tlv_data);
                
                DEBUGLOG("[iwlwifix] INFO Paging: paging enabled (size = %u bytes)\n",
                             paging_mem_size);
                
                if (paging_mem_size > MAX_PAGING_IMAGE_SIZE) {
                    DEBUGLOG(
                            "[iwlwifix] ERROR Paging: driver supports up to %lu bytes for paging image\n",
                            MAX_PAGING_IMAGE_SIZE);
                    return -EINVAL;
                }
                
                if (paging_mem_size & (FW_PAGING_SIZE - 1)) {
                    DEBUGLOG(
                            "[iwlwifix] ERROR Paging: image isn't multiple %lu\n",
                            FW_PAGING_SIZE);
                    return -EINVAL;
                }
                
                fw.img[IWL_UCODE_REGULAR].paging_mem_size =
                paging_mem_size;
                usniffer_img = IWL_UCODE_REGULAR_USNIFFER;
                fw.img[usniffer_img].paging_mem_size =
                paging_mem_size;
                break;
            case IWL_UCODE_TLV_FW_GSCAN_CAPA:
                /* ignored */
                break;
            case IWL_UCODE_TLV_FW_MEM_SEG: {
//                struct iwl_fw_dbg_mem_seg_tlv *dbg_mem =
//                (void *)tlv_data;
//                size_t size;
//                struct iwl_fw_dbg_mem_seg_tlv *n;
//
//                if (tlv_len != (sizeof(*dbg_mem)))
//                    goto invalid_tlv_len;
//
//                IWL_DEBUG_INFO(drv, "Found debug memory segment: %u\n",
//                               dbg_mem->data_type);
//
//                size = sizeof(*pieces->dbg_mem_tlv) *
//                (pieces->n_mem_tlv + 1);
//                n = krealloc(pieces->dbg_mem_tlv, size, GFP_KERNEL);
//                if (!n)
//                    return -ENOMEM;
//                pieces->dbg_mem_tlv = n;
//                pieces->dbg_mem_tlv[pieces->n_mem_tlv] = *dbg_mem;
//                pieces->n_mem_tlv++;
                break;
            }
            case IWL_UCODE_TLV_IML: {
                fw.iml_len = tlv_len;
                fw.iml = (UInt8 *) kmemdup(tlv_data, tlv_len);
                if (!fw.iml)
                    return -ENOMEM;
                break;
            }
            case IWL_UCODE_TLV_FW_RECOVERY_INFO: {
                struct tlvtemp {
                    __le32 buf_addr;
                    __le32 buf_size;
                };
                struct tlvtemp *recov_info = (struct tlvtemp *)tlv_data;
                
                if (tlv_len != sizeof(*recov_info))
                    goto invalid_tlv_len;
                capa->error_log_addr =
                le32_to_cpu(recov_info->buf_addr);
                capa->error_log_size =
                le32_to_cpu(recov_info->buf_size);
            }
                break;
            case IWL_UCODE_TLV_UMAC_DEBUG_ADDRS: {
//                struct iwl_umac_debug_addrs *dbg_ptrs =
//                (struct iwl_umac_debug_addrs *)tlv_data;
//
//                if (tlv_len != sizeof(*dbg_ptrs))
//                    goto invalid_tlv_len;
//                if (drv->trans->cfg->device_family ==
//                    IWL_DEVICE_FAMILY_9000)
//                    break;
//                drv->trans->umac_error_event_table =
//                le32_to_cpu(dbg_ptrs->error_info_addr) &
//                ~FW_ADDR_CACHE_CONTROL;
//                drv->trans->error_event_table_tlv_status |=
//                IWL_ERROR_EVENT_TABLE_UMAC;
                break;
            }
            case IWL_UCODE_TLV_LMAC_DEBUG_ADDRS: {
//                struct iwl_lmac_debug_addrs *dbg_ptrs =
//                (struct iwl_lmac_debug_addrs *)tlv_data;
//
//                if (tlv_len != sizeof(*dbg_ptrs))
//                    goto invalid_tlv_len;
//                if (drv->trans->cfg->device_family ==
//                    IWL_DEVICE_FAMILY_9000)
//                    break;
//                drv->trans->lmac_error_event_table[0] =
//                le32_to_cpu(dbg_ptrs->error_event_table_ptr) &
//                ~FW_ADDR_CACHE_CONTROL;
//                drv->trans->error_event_table_tlv_status |=
//                IWL_ERROR_EVENT_TABLE_LMAC1;
                break;
            }
            case IWL_UCODE_TLV_TYPE_BUFFER_ALLOCATION:
            case IWL_UCODE_TLV_TYPE_HCMD:
            case IWL_UCODE_TLV_TYPE_REGIONS:
            case IWL_UCODE_TLV_TYPE_TRIGGERS:
            case IWL_UCODE_TLV_TYPE_DEBUG_FLOW:
                if (iwlwifix_mod_params.enable_ini) {
                    iwl_fw_dbg_copy_tlv(trans, tlv, false);
                }
                break;
            case IWL_UCODE_TLV_CMD_VERSIONS:
                if (tlv_len % sizeof(struct iwl_fw_cmd_version)) {
                    DEBUGLOG("[iwlwifix] ERROR Invalid length for command versions: %u\n",
                            tlv_len);
                    tlv_len /= sizeof(struct iwl_fw_cmd_version);
                    tlv_len *= sizeof(struct iwl_fw_cmd_version);
                }
                if (WARN_ON(capa->cmd_versions))
                    return -EINVAL;
                capa->cmd_versions = (struct iwl_fw_cmd_version *) kmemdup(tlv_data, tlv_len);
                if (!capa->cmd_versions)
                    return -ENOMEM;
                capa->n_cmd_versions =
                tlv_len / sizeof(struct iwl_fw_cmd_version);
                break;
            default:
                DEBUGLOG("[iwlwifix] INFO unknown TLV: %d\n", tlv_type);
                break;
        }
    }
    
    if (!fw_has_capa(capa, IWL_UCODE_TLV_CAPA_USNIFFER_UNIFIED) &&
        usniffer_req && !*usniffer_images) {
        DEBUGLOG(
                "[iwlwifix] user selected to work with usniffer but usniffer image isn't available in ucode package\n");
        return -EINVAL;
    }
    
    if (len) {
        DEBUGLOG("[iwlwifix] ERROR invalid TLV after parsing: %zd\n", len);
        // iwl_print_hex_dump(drv, IWL_DL_FW, (UInt8 *)data, len);
        return -EINVAL;
    }
    return 0;
    
invalid_tlv_len:
    DEBUGLOG("[iwlwifix] ERROR TLV %d has invalid size: %u\n", tlv_type, tlv_len);
tlv_error:
    // iwl_print_hex_dump(drv, IWL_DL_FW, tlv_data, tlv_len);
    DEBUGLOG("[iwlwifix] ERROR TLV firmware ... Please double check.\n");
    return -EINVAL;
}

void Iwlwifix::iwl_set_ucode_api_flags(const UInt8 *data, struct iwl_ucode_capabilities *capa) {
    const struct iwl_ucode_api *ucode_api = (struct iwl_ucode_api*) data;
    UInt32 api_index = le32_to_cpu(ucode_api->api_index);
    UInt32 api_flags = le32_to_cpu(ucode_api->api_flags);
    int i;
    
    if(api_index >= DIV_ROUND_UP(NUM_IWL_UCODE_TLV_API, 32)) {
        DEBUGLOG("[iwlwifix] WARN: api flags index larger than supported by driver.");
        return;
    }
    
    for(i = 0; i < 32; i ++) {
        if(api_flags & BIT(i)) {
            set_bit(i + 32 * api_index, capa->_api);
        }
    }
}

int Iwlwifix::iwl_alloc_fw_desc(struct fw_desc *desc, struct fw_sec *sec) {
    void *data;
    desc->data = NULL;
    if(!sec || sec->size) {
        return -EINVAL;
    }
    data = IOMalloc(sec->size);
    if(!data) {
        return -ENOMEM;
    }
    desc->len = sec->size;
    desc->offset = sec->offset;
    memcpy(data, sec->data, desc->len);
    desc->data = data;
    return 0;
}

int Iwlwifix::iwl_alloc_ucode(struct iwl_firmware_pieces *pieces, enum iwl_ucode_type type)
{
    int i;
    struct fw_desc *sec;
    
    sec = (struct fw_desc *) kcalloc(pieces->img[type].sec_counter, sizeof(*sec));
    if (!sec)
        return -ENOMEM;
    fw.img[type].sec = sec;
    fw.img[type].num_sec = pieces->img[type].sec_counter;
    
    for (i = 0; i < pieces->img[type].sec_counter; i++)
        if (iwl_alloc_fw_desc(&sec[i], get_sec(pieces, type, i)))
            return -ENOMEM;
    
    return 0;
}

int Iwlwifix::validate_sizes(struct iwl_firmware_pieces *pieces, const struct iwl_cfg *cfg) {
    if(get_sec_size(pieces, IWL_UCODE_REGULAR, IWL_UCODE_SECTION_INST) > cfg->max_inst_size) {
        DEBUGLOG("[iwlwifix] ERROR: uCode instr len %zd too large", get_sec_size(pieces, IWL_UCODE_REGULAR, IWL_UCODE_SECTION_INST));
        return -1;
    }
    
    if(get_sec_size(pieces, IWL_UCODE_REGULAR, IWL_UCODE_SECTION_DATA) > cfg->max_data_size) {
        DEBUGLOG("[iwlwifix] uCode data len %zd too large to fit in", get_sec_size(pieces, IWL_UCODE_REGULAR, IWL_UCODE_SECTION_DATA));
        return -1;
    }
    
    if(get_sec_size(pieces, IWL_UCODE_INIT, IWL_UCODE_SECTION_INST) > cfg->max_inst_size) {
        DEBUGLOG("[iwlwifix] ERROR: uCode init instr len %zd too large", get_sec_size(pieces, IWL_UCODE_INIT, IWL_UCODE_SECTION_INST));
        return -1;
    }
    
    if(get_sec_size(pieces, IWL_UCODE_INIT, IWL_UCODE_SECTION_DATA) > cfg->max_inst_size) {
        DEBUGLOG("[iwlwifix] ERROR: uCode init data len %zd too large", get_sec_size(pieces, IWL_UCODE_INIT, IWL_UCODE_SECTION_DATA));
        return -1;
    }
    return 0;
}

struct iwl_op_mode *Iwlwifix::_iwl_op_mode_start(struct iwlwifix_opmode_table *op) {
    const struct iwl_op_mode_ops *ops = op->ops;
    struct iwl_op_mode *op_mode = NULL;
    
//#ifdef CONFIG_IWLWIFI_DEBUGFS
//    drv->dbgfs_op_mode = debugfs_create_dir(op->name,
//                                            drv->dbgfs_drv);
//    dbgfs_dir = drv->dbgfs_op_mode;
//#endif
    
    op_mode = ops->start(trans, trans->cfg, &fw);
//
//#ifdef CONFIG_IWLWIFI_DEBUGFS
//    if (!op_mode) {
//        debugfs_remove_recursive(drv->dbgfs_op_mode);
//        drv->dbgfs_op_mode = NULL;
//    }
//#endif
    return op_mode;
}

void Iwlwifix::_iwl_op_mode_stop() {
    if(op_mode) {
        iwl_op_mode_stop(op_mode);
        op_mode = NULL;
//#ifdef CONFIG_IWLWIFI_DEBUGFS
//        debugfs_remove_recursive(drv->dbgfs_op_mode);
//        drv->dbgfs_op_mode = NULL;
//#endif
    }
}

// System firmware load callback
void Iwlwifix::iwl_req_fw_callback(OSKextRequestTag reqTag, OSReturn result, const void* resData, UInt32 resDataLen, void *context) {
    Iwlwifix *drv = (Iwlwifix*) context;
    const struct firmware* ucode_raw = (struct firmware *)resData;
    struct iwl_ucode_header *ucode;
    struct iwl_fw *fwp = &drv->fw;
    struct iwlwifix_opmode_table *op;
    int err;
    struct iwl_firmware_pieces *pieces;
    const unsigned int api_max = drv->trans->cfg->ucode_api_max;
    const unsigned int api_min = drv->trans->cfg->ucode_api_min;
    // size_t trigger_tlv_sz[FW_DBG_TRIGGER_MAX];
    UInt32 api_ver;
    int i;
    bool load_module = false;
    bool usniffer_images = false;
    
    fwp->ucode_capa.max_probe_length = IWL_DEFAULT_MAX_PROBE_LENGTH;
    fwp->ucode_capa.standard_phy_calibration_size = IWL_DEFAULT_STANDARD_PHY_CALIBRATE_TBL_SIZE;
    fwp->ucode_capa.n_scan_channels = IWL_DEFAULT_SCAN_CHANNELS;
    // Dump all fw memory areas by default
    // fwp->dbg.dump_mask = 0xffffffff;
    pieces = (struct iwl_firmware_pieces *)IWLIOMallocZero(sizeof(*pieces));
    if(!pieces) {
        goto out_free_fw;
    }
    if(!ucode_raw) {
        goto try_again;
    }
    DEBUGLOG("[iwlwifix] Loaded firmware file %s (%zd bytes)", drv->firmware_name, ucode_raw->size);
    if(ucode_raw->size < 4) {
        DEBUGLOG("[iwlwifix] ERROR File size way too small!");
        goto try_again;
    }
    
    ucode = (struct iwl_ucode_header *) ucode_raw->data;
    if(ucode->ver) {
        DEBUGLOG("[iwlwifix] ERROR v1 or v2 firmware file!");
        goto out_free_fw;
    } else {
        err = drv->iwl_parse_tlv_firmware(ucode_raw, pieces, &fwp->ucode_capa, &usniffer_images);
    }
    if(err) {
        goto try_again;
    }
    
    if(fw_has_api(&fwp->ucode_capa, IWL_UCODE_TLV_API_NEW_VERSION)) {
        api_ver = drv->fw.ucode_ver;
    } else {
        api_ver = IWL_UCODE_API(drv->fw.ucode_ver);
    }
    
    /*
     * api_ver should match the api version forming part of the
     * firmware filename ... but we don't check for that and only rely
     * on the API version read from firmware header from here on forward
     */
    if (api_ver < api_min || api_ver > api_max) {
        DEBUGLOG("[iwlwifix] Driver unable to support your firmware API. "
                "Driver supports v%u, firmware is v%u.\n",
                api_max, api_ver);
        goto try_again;
    }
    /*
     * In mvm uCode there is no difference between data and instructions
     * sections.
     */
//    if (fw->type == IWL_FW_DVM && validate_sec_sizes(drv, pieces,
//                                                     drv->trans->cfg))
//        goto try_again;
    
    /* Runtime instructions and 2 copies of data:
     * 1) unmodified from disk
     * 2) backup cache for save/restore during power-downs
     */
    for (i = 0; i < IWL_UCODE_TYPE_MAX; i++)
        if (drv->iwl_alloc_ucode(pieces, (enum iwl_ucode_type)i))
            goto out_free_fw;
    
    // Ignore the dbg part
    
    /*
     * The (size - 16) / 12 formula is based on the information recorded
     * for each event, which is of mode 1 (including timestamp) for all
     * new microcodes that include this information.
     */
    fwp->init_evtlog_ptr = pieces->init_evtlog_ptr;
    if(pieces->init_evtlog_size) {
        fwp->init_evtlog_size = (pieces->init_evtlog_size - 16) / 12;
    } else {
        fwp->init_evtlog_size = drv->trans->cfg->base_params->max_event_log_size;
    }
    fwp->init_errlog_ptr = pieces->init_errlog_ptr;
    fwp->inst_evtlog_ptr = pieces->inst_evtlog_ptr;
    if (pieces->inst_evtlog_size)
        fwp->inst_evtlog_size = (pieces->inst_evtlog_size - 16)/12;
    else
        fwp->inst_evtlog_size =
        drv->trans->cfg->base_params->max_event_log_size;
    fwp->inst_errlog_ptr = pieces->inst_errlog_ptr;
    
    /*
     * figure out the offset of chain noise reset and gain commands
     * base on the size of standard phy calibration commands table size
     */
    if (fwp->ucode_capa.standard_phy_calibration_size >
        IWL_MAX_PHY_CALIBRATE_TBL_SIZE)
        fwp->ucode_capa.standard_phy_calibration_size =
        IWL_MAX_STANDARD_PHY_CALIBRATE_TBL_SIZE;
    
    /* We have our copies now, allow OS release its copies */
    release_firmware(ucode_raw);

    op = &iwlwifix_opmode_table[MVM_OP_MODE];
    
    DEBUGLOG("[iwlwifix] Loaded firmware version %s op_mode %s", drv->fw.fw_version, op->name);
    
    /* add this device to the list of devices using this op_mode */
    // list_add_tail(&drv->list, &op->drv);
    
    if (op->ops) {
        drv->op_mode = drv->_iwl_op_mode_start(op);
        
        if (!drv->op_mode) {
            // mutex_unlock(&iwlwifi_opmode_table_mtx);
            goto out_unbind;
        }
    } else {
        load_module = true;
    }
    
    /*
     * Complete the firmware request last so that
     * a driver unbind (stop) doesn't run while we
     * are doing the start() above.
     */
    // complete(&drv->request_firmware_complete);
    goto freelbl;

try_again:
    // We don't retry because firmware version is hardcoded. Instead, we simply release the firmware.
//    goto out_unbind;
//    release_firmware(ucode_raw);
//    if(iwl_request_firmware(false)) {
//        goto out_unbind; // failed
//    }
//    goto freelbl; // success
out_free_fw:
    release_firmware(ucode_raw); // release the firmware stored in kernel
out_unbind:
    // complete
    // release driver
freelbl:
    if(pieces) {
        for(i = 0; i < ARRAY_SIZE(pieces->img); i ++) {
            int alloc_size = pieces->img[i].sec_counter * sizeof(*pieces->img[i].sec);
            IOFree(pieces->img[i].sec, alloc_size);
        }
        IOFree(pieces, (sizeof(*pieces)));
    }
    // Set flag and notify waiting threads (like stop)
    IOLockLock(drv->firmwareLoadLock);
    drv->firmwareLoaded = true;
    IOLockUnlock(drv->firmwareLoadLock);
    IOLockWakeup(drv->firmwareLoadLock, context, false);
}

IOReturn Iwlwifix::iwl_drv_start(struct iwl_trans *transn) {
    int ret = 0;
    trans = transn;
    firmwareLoadLock = IOLockAlloc();
    firmwareLoaded = false;
    ret = iwl_request_firmware();
    if(ret != kOSReturnSuccess) {
        DEBUGLOG("[iwlwifix] ERROR Can't request firmware.");
        return ret;
    }
    return ret;
}

void Iwlwifix::iwl_drv_stop() {
    IOLockLock(firmwareLoadLock);
    while(!firmwareLoaded) { // If the firmware is still loading, wait for it
        IOLockSleep(firmwareLoadLock, this, THREAD_INTERRUPTIBLE);
    }
    IOLockUnlock(firmwareLoadLock);
    _iwl_op_mode_stop();
    iwl_dealloc_ucode();
    return;
}

// ------------------------ Extern variables ----------------------------
struct iwl_mod_params iwlwifix_mod_params = {
    .fw_restart = true,
    .bt_coex_active = true,
    .power_level = IWL_POWER_INDEX_1,
    .d0i3_disable = true,
    .d0i3_timeout = 1000,
    .uapsd_disable = IWL_DISABLE_UAPSD_BSS | IWL_DISABLE_UAPSD_P2P_CLIENT,
    // The rest are 0 by default
};
