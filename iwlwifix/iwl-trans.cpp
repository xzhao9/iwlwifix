//
//  iwlTrans.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-03.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#include "iwl-trans.hpp"
#include "utils.hpp"

#include <IOKit/pci/IOPCIDevice.h>

static void *bsearch(const char *key, const char *base, size_t num, size_t size,
              int (*cmp)(const void *key, const void *elt))
{
    const char *pivot;
    int result;
    
    while (num > 0) {
        pivot = base + (num >> 1) * size;
        result = cmp(key, pivot);
        
        if (result == 0)
            return (void *)pivot;
        
        if (result > 0) {
            base = pivot + size;
            num--;
        }
        num >>= 1;
    }
    
    return NULL;
}

// [DONE] Allocate and initiate the struct iwl_trans
// We don't have LOCKDEP in kernel so don't add it
struct iwl_trans *iwl_trans_alloc(unsigned int priv_size,
                                  IOPCIDevice *pci_device,
                                  const struct iwl_cfg *cfg,
                                  const struct iwl_trans_ops *ops) {
    struct iwl_trans *trans;
    trans = (struct iwl_trans*)IWLIOMallocZero(sizeof(*trans)+priv_size);
    if(!trans) {
        return NULL;
    }
    trans->dev = pci_device;
    pci_device->retain(); // add reference count
    trans->cfg = cfg;
    trans->ops = ops;
    trans->num_rx_queues = 1;
    // trans->dev_cmd_pool = NULL;
    snprintf(trans->dev_cmd_pool_name, sizeof(trans->dev_cmd_pool_name), "iwl_cmd_pool: Intel 9250");
//    trans->dev_cmd_pool = kmem_cache_create(trans->dev_cmd_pool_name, sizeof(struct iwl_device_cmd), sizeof(void*));
//    if(!trans->dev_cmd_pool) {
//        IOFree(trans, sizeof(*trans)+priv_size);
//        return NULL;
//    }
    // DEBUGLOG("[iwlwifix] memory alloc %#16lx.\n", (unsigned long)trans);
    
    WARN_ON(!ops->wait_txq_empty && !ops->wait_tx_queues_empty);
    return trans;
}

void iwl_trans_free(struct iwl_trans *trans, unsigned int priv_size) {
    // nothing needs to be done here
    trans->dev->release(); // decrease ref count
    IOFree(trans, sizeof(*trans)+priv_size);
}

int iwl_trans_send_cmd(struct iwl_trans *trans, struct iwl_host_cmd *cmd) {
    int ret;
    if(!(cmd->flags & CMD_SEND_IN_RFKILL) &&
       test_bit(STATUS_RFKILL_OPMODE, &trans->status)) {
        return -ERFKILL;
    }
    if(test_bit(STATUS_FW_ERROR, &trans->status)) {
        return -EIO;
    }
    if(trans->state != IWL_TRANS_FW_ALIVE) {
        DEBUGLOG("%s bad state = %d\n", __func__, trans->state);
        return -EIO;
    }
    // If you want async callback, you have to be async
    if(WARN_ON(cmd->flags & CMD_WANT_ASYNC_CALLBACK &&
                !(cmd->flags & CMD_ASYNC))) {
        return -EINVAL;
    }
    // LOCK_DEP specific
    // if (!(cmd->flags & CMD_ASYNC))
    //    lock_map_acquire_read(&trans->sync_cmd_lockdep_map);
    
    if(trans->wide_cmd_header && !iwl_cmd_groupid(cmd->id)) {
        cmd->id = DEF_ID(cmd->id);
    }
    ret = trans->ops->send_cmd(trans, cmd);
    // if (!(cmd->flags & CMD_ASYNC))
    //    lock_map_release(&trans->sync_cmd_lockdep_map);
    if(WARN_ON((cmd->flags & CMD_WANT_SKB) && !ret && !cmd->resp_pkt)) {
        return -EIO;
    }
    return ret;
}

static int iwl_hcmd_names_cmp(const void *key, const void *elt)
{
    const struct iwl_hcmd_names *name = (const struct iwl_hcmd_names *)elt;
    UInt8 cmd1 = *(UInt8 *)key;
    UInt8 cmd2 = name->cmd_id;
    
    return (cmd1 - cmd2);
}

const char *iwl_get_cmd_string(struct iwl_trans *trans, UInt32 id)
{
    UInt8 grp, cmd;
    struct iwl_hcmd_names *ret;
    const struct iwl_hcmd_arr *arr;
    size_t size = sizeof(struct iwl_hcmd_names);
    
    grp = iwl_cmd_groupid(id);
    cmd = iwl_cmd_opcode(id);
    
    if (!trans->command_groups || grp >= trans->command_groups_size ||
        !trans->command_groups[grp].arr)
        return "UNKNOWN";
    
    arr = &trans->command_groups[grp];
    ret = (struct iwl_hcmd_names *)bsearch((const char*)&cmd, (const char *)arr->arr, arr->size, size, iwl_hcmd_names_cmp);
    if (!ret)
        return "UNKNOWN";
    return ret->cmd_name;
}


int iwl_cmd_groups_verify_sorted(const struct iwl_trans_config *trans)
{
    int i, j;
    const struct iwl_hcmd_arr *arr;
    
    for (i = 0; i < trans->command_groups_size; i++) {
        arr = &trans->command_groups[i];
        if (!arr->arr)
            continue;
        for (j = 0; j < arr->size - 1; j++)
            if (arr->arr[j].cmd_id > arr->arr[j + 1].cmd_id)
                return -1;
    }
    return 0;
}

void iwl_trans_ref(struct iwl_trans *trans) {
    if(trans->ops->ref) {
        trans->ops->ref(trans);
    }
}

void iwl_trans_unref(struct iwl_trans *trans) {
    if(trans->ops->unref) {
        trans->ops->unref(trans);
    }
}
