//
//  iwl-phy-db.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-05.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef iwl_phy_db_hpp
#define iwl_phy_db_hpp

#include "linux/types.hpp"
#include "iwl-op-mode.hpp"
#include "iwl-trans.hpp"

struct iwl_phy_db;

struct iwl_phy_db *iwl_phy_db_init(struct iwl_trans *trans);
void iwl_phy_db_free(struct iwl_phy_db *phy_db);

int iwl_phy_db_set_section(struct iwl_phy_db *phy_db,
                           struct iwl_rx_packet *pkt);

int iwl_send_phy_db_data(struct iwl_phy_db *phy_db);

#endif /* iwl_phy_db_hpp */
