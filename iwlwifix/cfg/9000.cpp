//
//  9000.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-03.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#include "../iwl-config.hpp"
#include "../net80211/nl80211.hpp"

/*
 * Maximum length of AMPDU that the STA can receive in high-throughput (HT).
 * Length = 2 ^ (13 + max_ampdu_length_exp) - 1 (octets)
 */
enum ieee80211_max_ampdu_length_exp {
    IEEE80211_HT_MAX_AMPDU_8K = 0,
    IEEE80211_HT_MAX_AMPDU_16K = 1,
    IEEE80211_HT_MAX_AMPDU_32K = 2,
    IEEE80211_HT_MAX_AMPDU_64K = 3
};

// -----------  End of Imported Symbols ----------------

// Highest firmware API version supported
#define IWL9000_UCODE_API_MAX   46
// Lowest firmware API version supported
#define IWL9000_UCODE_API_MIN   30

#define IWL9000_NVM_VERSION     0x0a1d

// Memory offsets and lengths
#define IWL9000_DCCM_OFFSET         0x800000
#define IWL9000_DCCM_LEN            0x18000
#define IWL9000_DCCM2_OFFSET        0x880000
#define IWL9000_DCCM2_LEN           0x8000
#define IWL9000_SMEM_OFFSET         0x400000
#define IWL9000_SMEM_LEN            0x68000

#define IWL9000_FW_PRE "iwlwifi-9000-pu-b0-jf-b0-"
#define IWL9000_MODULE_FIRMWARE(api) \
IWL9000_FW_PRE __stringify(api) ".ucode"

static const struct IwlBaseParams iwl9000_base_params = {
    .eeprom_size = OTP_LOW_IMAGE_SIZE_32K,
    .num_of_queues = 31,
    .max_tfd_queue_size = 256,
    .shadow_ram_support = true,
    .led_compensation = 57,
    .wd_timeout = IWL_LONG_WD_TIMEOUT,
    .max_event_log_size = 512,
    .shadow_reg_enable = true,
    .pcieL1_allowed = true,
};


/**
 * enum nl80211_band - Frequency band
 * @NL80211_BAND_2GHZ: 2.4 GHz ISM band
 * @NL80211_BAND_5GHZ: around 5 GHz band (4.9 - 5.7 GHz)
 * @NL80211_BAND_60GHZ: around 60 GHz band (58.32 - 69.12 GHz)
 * @NUM_NL80211_BANDS: number of bands, avoid using this in userspace
 *    since newer kernel versions may support more bands
 */
enum nl80211_band {
    NL80211_BAND_2GHZ,
    NL80211_BAND_5GHZ,
    NL80211_BAND_60GHZ,
    
    NUM_NL80211_BANDS,
};

static const struct IwlHtParams iwl9000_ht_params = {
    .stbc = true,
    .ldpc = true,
    .ht40_bands = BIT(NL80211_BAND_2GHZ) | BIT(NL80211_BAND_5GHZ),
};



static const struct IwlTTParams iwl9000_tt_params = {
    .ct_kill_entry = 115,
    .ct_kill_exit = 93,
    .ct_kill_duration = 5,
    .dynamic_smps_entry = 111,
    .dynamic_smps_exit = 107,
    .tx_protection_entry = 112,
    .tx_protection_exit = 105,
    .tx_backoff = {
        {.temperature = 110, .backoff = 200},
        {.temperature = 111, .backoff = 600},
        {.temperature = 112, .backoff = 1200},
        {.temperature = 113, .backoff = 2000},
        {.temperature = 114, .backoff = 4000},
    },
    .support_ct_kill = true,
    .support_dynamic_smps = true,
    .support_tx_protection = true,
    .support_tx_backoff = true,
};

#define IWL_DEVICE_9000                            \
.ucode_api_max = IWL9000_UCODE_API_MAX,                \
.ucode_api_min = IWL9000_UCODE_API_MIN,                \
.device_family = IWL_DEVICE_FAMILY_9000,            \
.base_params = &iwl9000_base_params,                \
.led_mode = IWL_LED_RF_STATE,                    \
.nvm_hw_section_num = 10,                    \
.non_shared_ant = ANT_B,                    \
.dccm_offset = IWL9000_DCCM_OFFSET,                \
.dccm_len = IWL9000_DCCM_LEN,                    \
.dccm2_offset = IWL9000_DCCM2_OFFSET,                \
.dccm2_len = IWL9000_DCCM2_LEN,                    \
.smem_offset = IWL9000_SMEM_OFFSET,                \
.smem_len = IWL9000_SMEM_LEN,                    \
.features = IWL_TX_CSUM_NETIF_FLAGS | NETIF_F_RXCSUM,        \
.thermal_params = &iwl9000_tt_params,                \
.apmg_not_supported = true,                    \
.mq_rx_supported = true,                    \
.vht_mu_mimo_supported = true,                    \
.mac_addr_from_csr = true,                    \
.rf_id = true,                            \
.nvm_type = IWL_NVM_EXT,                    \
.dbgc_supported = true,                        \
.min_umac_error_event_table = 0x800000,                \
.csr = &iwl_csr_v1,                        \
.d3_debug_data_base_addr = 0x401000,                \
.d3_debug_data_length = 92 * 1024,                \
.ht_params = &iwl9000_ht_params,                \
.nvm_ver = IWL9000_NVM_VERSION,                    \
.max_ht_ampdu_exponent = IEEE80211_HT_MAX_AMPDU_64K,        \
.fw_mon_smem_write_ptr_addr = 0xa0476c,                \
.fw_mon_smem_write_ptr_msk = 0xfffff,                \
.fw_mon_smem_cycle_cnt_ptr_addr = 0xa04774,            \
.fw_mon_smem_cycle_cnt_ptr_msk = 0xfffff

const struct iwl_cfg iwl9560_2ac_160_cfg_soc = {
    .name = "Intel(R) Dual Band Wireless AC 9560 160MHz",
    .fw_name_pre = IWL9000_FW_PRE,
    IWL_DEVICE_9000,
    .integrated = true,
    .soc_latency = 5000,
};

