//
//  iwl-eeprom-read.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-06.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef iwl_eeprom_read_hpp
#define iwl_eeprom_read_hpp

#include "iwl-trans.hpp"

int iwl_read_eeprom(struct iwl_trans *trans, UInt8 **eeprom, size_t *eeprom_size);

#endif /* iwl_eeprom_read_hpp */
