//
//  iwl-debug.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-06.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#include "iwl-debug.hpp"


struct va_format {
    const char *fmt;
    va_list *va;
};


#define __iwl_fn(fn)                        \
void __iwl_ ##fn(struct IOPCIDevice *dev, const char *fmt, ...)    \
{                                \
struct va_format vaf = {                \
.fmt = fmt,                    \
};                            \
va_list args;                        \
\
va_start(args, fmt);                    \
vaf.va = &args;                        \
IOLog("[iwlwifix]" #fn " %pV", &vaf);                \
va_end(args);                        \
}

__iwl_fn(warn)
__iwl_fn(info)
__iwl_fn(crit)
__iwl_fn(dbg)

void __iwl_err(struct IOPCIDevice *dev, bool rfkill_prefix, bool trace_only,
               const char *fmt, ...)
{
    struct va_format vaf = {
        .fmt = fmt,
    };
    va_list args;
    
    va_start(args, fmt);
    vaf.va = &args;
    if (!trace_only) {
        if (rfkill_prefix)
            IOLog("(RFKILL) %pV", &vaf);
        else
            IOLog("[iwlfifix][ERROR] %pV", &vaf);
    }
    va_end(args);
}

