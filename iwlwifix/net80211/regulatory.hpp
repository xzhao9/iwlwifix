//
//  regulatory.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-07.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef regulatory_h
#define regulatory_h

#include <IOKit/IOLib.h>
#include "ieee80211.hpp"
#include "nl80211.hpp"


/**
 * enum environment_cap - Environment parsed from country IE
 * @ENVIRON_ANY: indicates country IE applies to both indoor and
 *    outdoor operation.
 * @ENVIRON_INDOOR: indicates country IE applies only to indoor operation
 * @ENVIRON_OUTDOOR: indicates country IE applies only to outdoor operation
 */
enum environment_cap {
    ENVIRON_ANY,
    ENVIRON_INDOOR,
    ENVIRON_OUTDOOR,
};

struct ieee80211_freq_range {
    UInt32 start_freq_khz;
    UInt32 end_freq_khz;
    UInt32 max_bandwidth_khz;
};

struct ieee80211_power_rule {
    UInt32 max_antenna_gain;
    UInt32 max_eirp;
};

struct ieee80211_wmm_ac {
    UInt16 cw_min;
    UInt16 cw_max;
    UInt16 cot;
    UInt8 aifsn;
};

struct ieee80211_wmm_rule {
    struct ieee80211_wmm_ac client[IEEE80211_NUM_ACS];
    struct ieee80211_wmm_ac ap[IEEE80211_NUM_ACS];
};

struct ieee80211_reg_rule {
    struct ieee80211_freq_range freq_range;
    struct ieee80211_power_rule power_rule;
    struct ieee80211_wmm_rule wmm_rule;
    UInt32 flags;
    UInt32 dfs_cac_ms;
    bool has_wmm;
};

struct ieee80211_regdomain {
    // struct rcu_head rcu_head;
    UInt32 n_reg_rules;
    char alpha2[3];
    enum nl80211_dfs_regions dfs_region;
    struct ieee80211_reg_rule reg_rules[];
};

#define MHZ_TO_KHZ(freq) ((freq) * 1000)
#define KHZ_TO_MHZ(freq) ((freq) / 1000)
#define DBI_TO_MBI(gain) ((gain) * 100)
#define MBI_TO_DBI(gain) ((gain) / 100)
#define DBM_TO_MBM(gain) ((gain) * 100)
#define MBM_TO_DBM(gain) ((gain) / 100)

#endif /* regulatory_h */
