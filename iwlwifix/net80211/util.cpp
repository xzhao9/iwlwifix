//
//  util.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-07.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//
#include "cfg80211.hpp"
#include "../linux/align.hpp"
#include "../linux/stddef.h"

int ieee80211_channel_to_frequency(int chan, enum nl80211_band band)
{
    /* see 802.11 17.3.8.3.2 and Annex J
     * there are overlapping channel numbers in 5GHz and 2GHz bands */
    if (chan <= 0)
        return 0; /* not supported */
    switch (band) {
        case NL80211_BAND_2GHZ:
            if (chan == 14)
                return 2484;
            else if (chan < 14)
                return 2407 + chan * 5;
            break;
        case NL80211_BAND_5GHZ:
            if (chan >= 182 && chan <= 196)
                return 4000 + chan * 5;
            else
                return 5000 + chan * 5;
            break;
        case NL80211_BAND_60GHZ:
            if (chan < 7)
                return 56160 + chan * 2160;
            break;
        default:
            ;
    }
    return 0; /* not supported */
}


struct __attribute__((packed)) fwdb_wmm_ac {
    UInt8 ecw;
    UInt8 aifsn;
    __be16 cot;
};

struct __attribute__((packed)) fwdb_wmm_rule {
    struct fwdb_wmm_ac client[IEEE80211_NUM_ACS];
    struct fwdb_wmm_ac ap[IEEE80211_NUM_ACS];
};

struct __attribute__((packed, aligned(4))) fwdb_rule {
    UInt8 len;
    UInt8 flags;
    __be16 max_eirp;
    __be32 start, end, max_bw;
    /* start of optional data */
    __be16 cac_timeout;
    __be16 wmm_ptr;
};

#define FWDB_MAGIC 0x52474442
#define FWDB_VERSION 20


struct __attribute__((packed, aligned(4))) fwdb_country {
    UInt8 alpha2[2];
    __be16 coll_ptr;
    /* this struct cannot be extended */
};

struct __attribute__((packed, aligned(4))) fwdb_header {
    __be32 magic;
    __be32 version;
    struct fwdb_country country[];
};


struct __attribute__((packed, aligned(4))) fwdb_collection {
    UInt8 len;
    UInt8 n_rules;
    UInt8 dfs_region;
    /* no optional data yet */
    /* aligned to 2, then followed by __be16 array of rule pointers */
};

enum fwdb_flags {
    FWDB_FLAG_NO_OFDM    = BIT(0),
    FWDB_FLAG_NO_OUTDOOR    = BIT(1),
    FWDB_FLAG_DFS        = BIT(2),
    FWDB_FLAG_NO_IR        = BIT(3),
    FWDB_FLAG_AUTO_BW    = BIT(4),
};

/* code to directly load a firmware database through request_firmware */
static const struct fwdb_header *regdb;
#include "../linux/errno.hpp"
#include "regulatory.hpp"

static bool alpha2_equal(const char *alpha2_x, const char *alpha2_y)
{
    if (!alpha2_x || !alpha2_y)
        return false;
    return alpha2_x[0] == alpha2_y[0] && alpha2_x[1] == alpha2_y[1];
}

static int ecw2cw(int ecw)
{
    return (1 << ecw) - 1;
}


static bool valid_wmm(struct fwdb_wmm_rule *rule)
{
    struct fwdb_wmm_ac *ac = (struct fwdb_wmm_ac *)rule;
    int i;
    
    for (i = 0; i < IEEE80211_NUM_ACS * 2; i++) {
        UInt16 cw_min = ecw2cw((ac[i].ecw & 0xf0) >> 4);
        UInt16 cw_max = ecw2cw(ac[i].ecw & 0x0f);
        UInt8 aifsn = ac[i].aifsn;
        
        if (cw_min >= cw_max)
            return false;
        
        if (aifsn < 1)
            return false;
    }
    
    return true;
}


static void set_wmm_rule(const struct fwdb_header *db,
                         const struct fwdb_country *country,
                         const struct fwdb_rule *rule,
                         struct ieee80211_reg_rule *rrule)
{
    struct ieee80211_wmm_rule *wmm_rule = &rrule->wmm_rule;
    struct fwdb_wmm_rule *wmm;
    unsigned int i, wmm_ptr;
    
    wmm_ptr = be16_to_cpu(rule->wmm_ptr) << 2;
    wmm = (struct fwdb_wmm_rule *)((UInt8 *)db + wmm_ptr);
    
    if (!valid_wmm(wmm)) {
        /*pr_err("Invalid regulatory WMM rule %u-%u in domain %c%c\n",
               be32_to_cpu(rule->start), be32_to_cpu(rule->end),
               country->alpha2[0], country->alpha2[1]);*/
        return;
    }
    
    for (i = 0; i < IEEE80211_NUM_ACS; i++) {
        wmm_rule->client[i].cw_min =
        ecw2cw((wmm->client[i].ecw & 0xf0) >> 4);
        wmm_rule->client[i].cw_max = ecw2cw(wmm->client[i].ecw & 0x0f);
        wmm_rule->client[i].aifsn =  wmm->client[i].aifsn;
        wmm_rule->client[i].cot =
        1000 * be16_to_cpu(wmm->client[i].cot);
        wmm_rule->ap[i].cw_min = ecw2cw((wmm->ap[i].ecw & 0xf0) >> 4);
        wmm_rule->ap[i].cw_max = ecw2cw(wmm->ap[i].ecw & 0x0f);
        wmm_rule->ap[i].aifsn = wmm->ap[i].aifsn;
        wmm_rule->ap[i].cot = 1000 * be16_to_cpu(wmm->ap[i].cot);
    }
    
    rrule->has_wmm = true;
}


static int __regdb_query_wmm(const struct fwdb_header *db,
                             const struct fwdb_country *country, int freq,
                             struct ieee80211_reg_rule *rrule)
{
    unsigned int ptr = be16_to_cpu(country->coll_ptr) << 2;
    struct fwdb_collection *coll = (struct fwdb_collection *)((UInt8 *)db + ptr);
    int i;
    
    for (i = 0; i < coll->n_rules; i++) {
        __be16 *rules_ptr = (__be16 *)((UInt8 *)coll + ALIGN(coll->len, 2));
        unsigned int rule_ptr = be16_to_cpu(rules_ptr[i]) << 2;
        struct fwdb_rule *rule = (struct fwdb_rule *)((UInt8 *)db + rule_ptr);
        
        if (rule->len < offsetofend(struct fwdb_rule, wmm_ptr))
            continue;
        
        if (freq >= KHZ_TO_MHZ(be32_to_cpu(rule->start)) &&
            freq <= KHZ_TO_MHZ(be32_to_cpu(rule->end))) {
            set_wmm_rule(db, country, rule, rrule);
            return 0;
        }
    }
    
    return -ENODATA;
}


int reg_query_regdb_wmm(char *alpha2, int freq, struct ieee80211_reg_rule *rule)
{
    const struct fwdb_header *hdr = regdb;
    const struct fwdb_country *country;
    
    if (!regdb)
        return -ENODATA;
    
    if (IS_ERR(regdb))
        return PTR_ERR(regdb);
    
    country = &hdr->country[0];
    while (country->coll_ptr) {
        if (alpha2_equal(alpha2, (const char*)country->alpha2))
            return __regdb_query_wmm(regdb, country, freq, rule);
        
        country++;
    }
    
    return -ENODATA;
}
