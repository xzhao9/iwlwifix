//
//  iwl-modparams.h
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-04.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef iwl_modparams_h
#define iwl_modparams_h

#include "linux/bitops.hpp"

extern struct iwl_mod_params iwlwifix_mod_params;

enum iwl_power_level {
    IWL_POWER_INDEX_1,
    IWL_POWER_INDEX_2,
    IWL_POWER_INDEX_3,
    IWL_POWER_INDEX_4,
    IWL_POWER_INDEX_5,
    IWL_POWER_NUM
};

enum iwl_disable_11n {
    IWL_DISABLE_HT_ALL     = BIT(0),
    IWL_DISABLE_HT_TXAGG     = BIT(1),
    IWL_DISABLE_HT_RXAGG     = BIT(2),
    IWL_ENABLE_HT_TXAGG     = BIT(3),
};

enum iwl_amsdu_size {
    IWL_AMSDU_DEF = 0,
    IWL_AMSDU_4K = 1,
    IWL_AMSDU_8K = 2,
    IWL_AMSDU_12K = 3,
    /* Add 2K at the end to avoid breaking current API */
    IWL_AMSDU_2K = 4,
};

enum iwl_uapsd_disable {
    IWL_DISABLE_UAPSD_BSS        = BIT(0),
    IWL_DISABLE_UAPSD_P2P_CLIENT    = BIT(1),
};

/**
 * struct iwl_mod_params
 *
 * Holds the module parameters
 *
 * @swcrypto: using hardware encryption, default = 0
 * @disable_11n: disable 11n capabilities, default = 0,
 *    use IWL_[DIS,EN]ABLE_HT_* constants
 * @amsdu_size: See &enum iwl_amsdu_size.
 * @fw_restart: restart firmware, default = 1
 * @bt_coex_active: enable bt coex, default = true
 * @led_mode: system default, default = 0
 * @power_save: enable power save, default = false
 * @power_level: power level, default = 1
 * @debug_level: levels are IWL_DL_*
 * @antenna_coupling: antenna coupling in dB, default = 0
 * @nvm_file: specifies a external NVM file
 * @uapsd_disable: disable U-APSD, see &enum iwl_uapsd_disable, default =
 *    IWL_DISABLE_UAPSD_BSS | IWL_DISABLE_UAPSD_P2P_CLIENT
 * @d0i3_disable: disable d0i3, default = 1,
 * @d0i3_timeout: time to wait after no refs are taken before
 *    entering D0i3 (in msecs)
 * @lar_disable: disable LAR (regulatory), default = 0
 * @fw_monitor: allow to use firmware monitor
 * @disable_11ac: disable VHT capabilities, default = false.
 * @remove_when_gone: remove an inaccessible device from the PCIe bus.
 * @enable_ini: enable new FW debug infratructure (INI TLVs)
 */
struct iwl_mod_params {
    int swcrypto;
    unsigned int disable_11n;
    int amsdu_size;
    bool fw_restart;
    bool bt_coex_active;
    int led_mode;
    bool power_save;
    int power_level;
    UInt32 debug_level;
    int antenna_coupling;
    char *nvm_file;
    UInt32 uapsd_disable;
    bool d0i3_disable;
    unsigned int d0i3_timeout;
    bool lar_disable;
    bool fw_monitor;
    bool disable_11ac;
    /**
     * @disable_11ax: disable HE capabilities, default = false
     */
    bool disable_11ax;
    bool remove_when_gone;
    bool enable_ini;
};

static inline bool iwl_enable_rx_ampdu(void)
{
    if (iwlwifix_mod_params.disable_11n & IWL_DISABLE_HT_RXAGG)
        return false;
    return true;
}

static inline bool iwl_enable_tx_ampdu(void)
{
    if (iwlwifix_mod_params.disable_11n & IWL_DISABLE_HT_TXAGG)
        return false;
    if (iwlwifix_mod_params.disable_11n & IWL_ENABLE_HT_TXAGG)
        return true;
    /* enabled by default */
    return true;
}

#endif /* iwl_modparams_h */
