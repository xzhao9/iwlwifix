//
//  iwl-eepom-parse.hpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-06.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#ifndef iwl_eepom_parse_hpp
#define iwl_eepom_parse_hpp

#include <IOKit/IOLib.h>
#include "linux/types.hpp"
#include "net80211/nl80211.hpp"
#include "net80211/if_ether.hpp"
#include "net80211/cfg80211.hpp"

struct iwl_nvm_data {
    int n_hw_addrs;
    UInt8 hw_addr[ETH_ALEN];
    
    UInt8 calib_version;
    __le16 calib_voltage;
    
    __le16 raw_temperature;
    __le16 kelvin_temperature;
    __le16 kelvin_voltage;
    __le16 xtal_calib[2];
    
    bool sku_cap_band_24ghz_enable;
    bool sku_cap_band_52ghz_enable;
    bool sku_cap_11n_enable;
    bool sku_cap_11ac_enable;
    bool sku_cap_11ax_enable;
    bool sku_cap_amt_enable;
    bool sku_cap_ipan_enable;
    bool sku_cap_mimo_disabled;
    
    UInt16 radio_cfg_type;
    UInt8 radio_cfg_step;
    UInt8 radio_cfg_dash;
    UInt8 radio_cfg_pnum;
    UInt8 valid_tx_ant, valid_rx_ant;
    
    UInt32 nvm_version;
    SInt8 max_tx_pwr_half_dbm;
    
    bool lar_enabled;
    bool vht160_supported;
    struct ieee80211_supported_band bands[NUM_NL80211_BANDS];
    struct ieee80211_channel channels[];
};

/**
 * iwl_parse_eeprom_data - parse EEPROM data and return values
 *
 * @dev: device pointer we're parsing for, for debug only
 * @cfg: device configuration for parsing and    overrides
 * @eeprom: the EEPROM data
 * @eeprom_size: length of the EEPROM data
 *
 * This function parses all EEPROM values we need and then
 * returns a (newly allocated) struct containing all the
 * relevant values for driver use. The struct must be freed
 * later with iwl_free_nvm_data().
 */
struct iwl_nvm_data *
iwl_parse_eeprom_data(struct IOPCIDevice *dev, const struct iwl_cfg *cfg,
                      const UInt8 *eeprom, size_t eeprom_size);

int iwl_init_sband_channels(struct iwl_nvm_data *data,
                            struct ieee80211_supported_band *sband,
                            int n_channels, enum nl80211_band band);

void iwl_init_ht_hw_capab(const struct iwl_cfg *cfg,
                          struct iwl_nvm_data *data,
                          struct ieee80211_sta_ht_cap *ht_info,
                          enum nl80211_band band,
                          UInt8 tx_chains, UInt8 rx_chains);

#endif /* iwl_eepom_parse_hpp */
