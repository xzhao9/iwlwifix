//
//  iwl-dbg-tlv.cpp
//  iwlwifix
//
//  Created by Xu Zhao on 2019-08-05.
//  Copyright © 2019 Xu Zhao. All rights reserved.
//

#include "linux/align.hpp"
#include "linux/errno.hpp"
#include "fw/img.hpp"
#include "iwl-trans.hpp"
#include "iwl-dbg-tlv.hpp"

void iwl_fw_dbg_copy_tlv(struct iwl_trans *trans, struct iwl_ucode_tlv *tlv,
                         bool ext) {
    struct iwl_apply_point_data *data;
    struct iwl_fw_ini_header *header = (struct iwl_fw_ini_header*)&tlv->data[0];
    UInt32 apply_point = le32_to_cpu(header->apply_point);
    
    int copy_size = le32_to_cpu(tlv->length) + sizeof(*tlv);
    int offset_size = copy_size;
    
    if(le32_to_cpu(header->tlv_version) != 1) {
        return;
    }
    
    if(WARN_ON_ONCE(apply_point >= IWL_FW_INI_APPLY_NUM)) {
        DEBUGLOG("[iwlwifix] Invalid apply point id %d\n", apply_point);
        return;
    }
    
    if(ext) {
        data = &trans->apply_points_ext[apply_point];
    } else {
        data = &trans->apply_points[apply_point];
    }
    
    // add room for is_alloc field in &iwl_fw_ini_allocation_data struct
    if(le32_to_cpu(tlv->type) == IWL_UCODE_TLV_TYPE_BUFFER_ALLOCATION) {
        struct iwl_fw_ini_allocation_data *buffer_alloc =
        (struct iwl_fw_ini_allocation_data *)tlv->data;
        offset_size += sizeof(buffer_alloc->is_alloc);
    }
    
    if(WARN_ON_ONCE(data->offset + offset_size > data->size)) {
        DEBUGLOG("[iwlwifi] Not enough memory for apply point %d\n", apply_point);
        return;
    }
    memcpy((char*)data->data + data->offset, (char*)tlv, copy_size);
    data->offset += offset_size;
}

void iwl_alloc_dbg_tlv(struct iwl_trans *trans, size_t len, const UInt8 *data, bool ext) {
    struct iwl_ucode_tlv *tlv;
    UInt32 size[IWL_FW_INI_APPLY_NUM] = {0};
    int i;
    while(len >= sizeof(*tlv)) {
        UInt32 tlv_len, tlv_type, apply;
        struct iwl_fw_ini_header *hdr;
        
        len -= sizeof(*tlv);
        tlv = (struct iwl_ucode_tlv*)data;
        
        tlv_len = le32_to_cpu(tlv->length);
        tlv_type = le32_to_cpu(tlv->type);
        
        if(len < tlv_len) { return; }
        
        len -= ALIGN(tlv_type, 4);
        data += sizeof(*tlv) + ALIGN(tlv_len, 4);
        
        if(tlv_type < IWL_UCODE_TLV_DEBUG_BASE ||
           tlv_type > IWL_UCODE_TLV_DEBUG_MAX) {
            continue;
        }
        
        hdr = (struct iwl_fw_ini_header*)&tlv->data[0];
        apply = le32_to_cpu(hdr->apply_point);
        
        if(le32_to_cpu(hdr->tlv_version) != 1) {
            continue;
        }
        
        DEBUGLOG("[iwlwifix] WRT: read tlv %#06x, applypoint %d\n", le32_to_cpu(tlv->type), apply);
        
        if(WARN_ON(apply >= IWL_FW_INI_APPLY_NUM)) {
            continue;
        }
        
        if(tlv_type == IWL_UCODE_TLV_TYPE_BUFFER_ALLOCATION) {
            struct iwl_fw_ini_allocation_data *buf_alloc = (struct iwl_fw_ini_allocation_data*)tlv->data;
            size[apply] += sizeof(buf_alloc->is_alloc);
        }
        size[apply] += sizeof(*tlv) + tlv_len;
    }
    
    for(i = 0; i < ARRAY_SIZE(size); i ++) {
        void *mem;
        if(!size[i]) {continue;}
        mem = IWLIOMallocZero(size[i]);
        if(!mem) {
            DEBUGLOG("[iwlwifix] No memory for apply point %d\n", i);
            return;
        }
        if(ext) {
            trans->apply_points_ext[i].data = mem;
            trans->apply_points_ext[i].size = size[i];
        } else {
            trans->apply_points[i].data = mem;
            trans->apply_points[i].size = size[i];
        }
        trans->ini_valid = true;
    }
}

void iwl_fw_dbg_free(struct iwl_trans *trans) {
    int i;
    for(i = 0; i < ARRAY_SIZE(trans->apply_points); i ++) {
        IOFree(trans->apply_points[i].data, trans->apply_points[i].size);
        trans->apply_points[i].size = 0;
        trans->apply_points[i].offset = 0;
        
        IOFree(trans->apply_points_ext[i].data,
               trans->apply_points_ext[i].size);
        trans->apply_points_ext[i].size = 0;
        trans->apply_points_ext[i].offset = 0;
    }
}

static int iwl_parse_fw_dbg_tlv(struct iwl_trans *trans,
                          const UInt8* data, size_t len) {
    struct iwl_ucode_tlv *tlv;
    enum iwl_ucode_tlv_type tlv_type;
    UInt32 tlv_len;
    
    while (len >= sizeof(*tlv)) {
        len -= sizeof(*tlv);
        tlv = (struct iwl_ucode_tlv *)data;
        
        tlv_len = le32_to_cpu(tlv->length);
        tlv_type = (enum iwl_ucode_tlv_type) le32_to_cpu(tlv->type);
        
        if (len < tlv_len) {
            DEBUGLOG("[iwlfifix] ERROR invalid TLV len: %zd/%u\n",
                    len, tlv_len);
            return -EINVAL;
        }
        len -= ALIGN(tlv_len, 4);
        data += sizeof(*tlv) + ALIGN(tlv_len, 4);
        
        switch (tlv_type) {
            case IWL_UCODE_TLV_TYPE_BUFFER_ALLOCATION:
            case IWL_UCODE_TLV_TYPE_HCMD:
            case IWL_UCODE_TLV_TYPE_REGIONS:
            case IWL_UCODE_TLV_TYPE_TRIGGERS:
            case IWL_UCODE_TLV_TYPE_DEBUG_FLOW:
                iwl_fw_dbg_copy_tlv(trans, tlv, true);
                break;
            default:
                DEBUGLOG("[iwlwifix] Invalid TLV %x\n", tlv_type);
                break;
        }
    }
    
    return 0;
}

void iwl_load_fw_dbg_tlv(void *dev, struct iwl_trans *trans)
{
    // By default INI is disabled, so just return
    if (trans->external_ini_loaded || !iwlwifix_mod_params.enable_ini)
        return;
    
}
